<?php
return [
    'Store' => 'Магазин',
    'Cash' => 'Оплата наличными',
    'Cash payment module' => 'Модуль для печати счета на оплату наличными',
    'Payment order #{id} on "{site}" website' => 'Оплата заказа №{id} на сайте "{site}"',
    'Pay' => 'Заплатить',
    'Print invoice' => 'Распечатать счет',
];
