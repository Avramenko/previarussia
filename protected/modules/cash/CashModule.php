<?php

use yupe\components\WebModule;

class CashModule extends WebModule
{
    const VERSION = '0.1';

    public function getDependencies()
    {
        return ['payment'];
    }

    public function getNavigation()
    {
        return false;
    }

    public function getAdminPageLink()
    {
        return false;
    }

    public function getIsShowInAdminMenu()
    {
        return false;
    }

    public function getVersion()
    {
        return self::VERSION;
    }

    public function getEditableParams()
    {
        return [];
    }

    public function getCategory()
    {
        return Yii::t('CashModule.cash', 'Store');
    }

    public function getName()
    {
        return Yii::t('CashModule.cash', 'Cash');
    }

    public function getDescription()
    {
        return Yii::t('CashModule.cash', 'Cash payment module');
    }

    public function getAuthor()
    {
        return Yii::t('CashModule.cash', 'McSim');
    }

    public function getAuthorEmail()
    {
        return Yii::t('CashModule.cash', 'maxim.avramenko@gmail.com');
    }

    public function getIcon()
    {
        return 'fa fa-rub';
    }

    public function init()
    {
        parent::init();
    }
}
