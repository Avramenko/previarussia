<?php

return [
    'module' => [
        'class' => 'application.modules.cash.CashModule',
    ],
    'component' => [
        'paymentManager' => [
            'paymentSystems' => [
                'cash' => [
                    'class' => 'application.modules.cash.components.payments.CashPaymentSystem',
                ]
            ],
        ],
    ],
];