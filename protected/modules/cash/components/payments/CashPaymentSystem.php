<?php

/**
 * Class CashPaymentSystem
 * @link http://www.cbbl.ru
 */

Yii::import('application.modules.cash.CashModule');

class CashPaymentSystem extends PaymentSystem
{
    public function renderCheckoutForm(Payment $payment, Order $order, $return = false)
    {
        $settings = $payment->getPaymentSystemSettings();

        $culture = $settings['language'];

        $form = CHtml::form($settings['testmode'] ? "/payment/process/test" : "/payment/process/3");
        $form .= CHtml::hiddenField('language', $culture);
        $form .= CHtml::hiddenField('OrderId', $order->id);
        $form .= CHtml::submitButton(Yii::t('CashModule.cash','Print invoice'));
        $form .= CHtml::endForm();

        if ($return) {
            return $form;
        } else {
            echo $form;
        }
    }

    public function processCheckout(Payment $payment, CHttpRequest $request)
    {

        // Печатаем счет на оплату наличными, payment method id не меняем
        //Yii::app()->end();
        $orderId = (int)$request->getParam('OrderId');
        $token = $request->getParam('YUPE_TOKEN');
        $order = Order::model()->with('products')->findByPk($orderId);


        if (null === $order) {
            Yii::log(Yii::t('CashModule.cash', 'Order with id = {id} not found!', ['{id}' => $orderId]), CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
            return false;
        }

        if ($order->isPaid()) {
            Yii::log(Yii::t('CashModule.cash', 'Order with id = {id} already payed!', ['{id}' => $orderId]), CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
            return false;
        }
        /*Создаем из html PDF*/
        //require_once '/path/to/vendor/dompdf/dompdf/dompdf_config.inc.php';

        require_once(Yii::getPathOfAlias('vendor')."/dompdf/dompdf/dompdf_config.inc.php");
        $dompdf = new DOMPDF();
        $params = [
            'order' => $order,
        ];
        $html = Yii::app()->controller->renderFile(Yii::app()->getModulePath().'/cash/views/invoice.php', $params);
        //CVarDumper::dump($html);

        Yii::app()->end();
    }
}
