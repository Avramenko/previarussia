<?php
use Endroid\QrCode\QrCode;
Yii::import('application.components.NumToString');
// Подключаем папку с assets
$mainAssets = Yii::app()->getTheme()->getAssetsUrl();
// Расчет даты актуальности счета
$true_until = new DateTime($order->date);
$true_until->add(new DateInterval('P3D'));
$true_until = $true_until->format("Y-m-d H:m:s");
$numToString = new NumToString();
$numToString->_num = $order->getTotalPriceWithDelivery();
$totalPriceWithDelivery = $numToString->num2str();
//$numToString->num2str();
//echo CVarDumper::dump($numToString->num2str());

// render qr
$qrArray = array(
    'ST00012',
    'Name='.'ИП Глинка Елена Николаевна',
    'PersonalAcc='.'40802810300000002679',
    'BankName='.'САНКТ-ПЕТЕРБУРГСКИЙ Ф-Л ОАО "БАЛТИЙСКИЙ БАНК" Г. САНКТ-ПЕТЕРБУРГ',
    'BIC='.'044030804',
    'CorrespAcc='.'30101810100000000804',
    'Sum='.$order->getTotalPriceWithDelivery()*100,
    'Purpose='.'Оплата по заказу клиента № '.$order->id . ' previarussia.ru',
    'PayeeINN='.'781801669187'
);
$qrData = implode("|", $qrArray);

//CVarDumper::dump($qrData);
$qrCode = new QrCode();
$qrCode
    ->setText($qrData)
    ->setSize(150)
    ->setPadding(5)
    ->setErrorCorrection('high')
    ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
    ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
    //->setLabel('My label')
    ->setLabelFontSize(16)
    //->render()
;

//$data = $qrCode->render(null,'png');
//CVarDumper::dump($qrCode);
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 5.0 Transitional//EN">
<HTML>
<HEAD>
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; CHARSET=utf-8">
    <TITLE>Счет на оплату №<?=$order->id?> от <?=Yii::app()->getDateFormatter()->formatDateTime($order->date, "long", "long");?></TITLE>
    <STYLE TYPE="text/css">
        body {
            background: #ffffff;
            margin: 0;
            font-family: Arial;
            font-size: 8pt;
            font-style: normal;
        }

        tr.R0 {
            height: 75px;
        }

        tr.R0 td.R0C1 {
            text-align: center;
            vertical-align: medium;
        }

        tr.R1 {
            font-family: Arial;
            font-size: 8pt;
            font-style: normal;
            height: 15px;
        }

        tr.R1 td.R1C1 {
            font-family: Arial;
            font-size: 8pt;
            font-style: normal;
        }

        tr.R16 {
            height: 46px;
        }

        tr.R16 td.R16C1 {
            font-family: Arial;
            font-size: 14pt;
            font-style: normal;
            font-weight: bold;
            vertical-align: medium;
            border-bottom: #000000 2px solid;
        }

        tr.R18 {
            height: 33px;
        }

        tr.R18 td.R18C1 {
            font-family: Arial;
            font-size: 9pt;
            font-style: normal;
            vertical-align: medium;
        }

        tr.R18 td.R18C5 {
            font-family: Arial;
            font-size: 9pt;
            font-style: normal;
            font-weight: bold;
            vertical-align: top;
        }

        tr.R3 {
            height: 17px;
        }

        tr.R3 td.R10C22 {
            font-family: Arial;
            font-size: 7pt;
            font-style: normal;
            vertical-align: top;
            border-left: #000000 1px solid;
            border-bottom: #000000 1px solid;
            border-right: #000000 1px solid;
        }

        tr.R3 td.R11C1 {
            font-family: Arial;
            font-size: 8pt;
            font-style: normal;
            border-left: #000000 1px solid;
            border-bottom: #000000 1px solid;
            border-right: #000000 1px solid;
        }

        tr.R3 td.R11C28 {
            font-family: Arial;
            font-size: 10pt;
            font-style: normal;
            vertical-align: top;
            border-bottom: #000000 1px solid;
            border-right: #000000 1px solid;
        }

        tr.R3 td.R20C1 {
            font-family: Arial;
            font-size: 9pt;
            font-style: normal;
            vertical-align: medium;
        }

        tr.R3 td.R20C5 {
            font-family: Arial;
            font-size: 9pt;
            font-style: normal;
            font-weight: bold;
            vertical-align: top;
        }

        tr.R3 td.R32C29 {
            font-family: Arial;
            font-size: 10pt;
            font-style: normal;
            font-weight: bold;
            text-align: right;
        }

        tr.R3 td.R34C1 {
            font-family: Arial;
            font-size: 10pt;
            font-style: normal;
            text-align: left;
        }

        tr.R3 td.R35C1 {
            font-family: Arial;
            font-size: 10pt;
            font-style: normal;
            font-weight: bold;
            vertical-align: top;
        }

        tr.R3 td.R39C1 {
            font-family: Arial;
            font-size: 9pt;
            font-style: normal;
            font-weight: bold;
            vertical-align: medium;
        }

        tr.R3 td.R39C18 {
            border-bottom: #000000 1px solid;
        }

        tr.R3 td.R39C21 {
            font-family: Arial;
            font-size: 10pt;
            font-style: normal;
            font-weight: bold;
            text-align: center;
            border-bottom: #000000 1px solid;
        }

        tr.R3 td.R39C7 {
            font-family: Arial;
            font-size: 9pt;
            font-style: normal;
            border-bottom: #000000 1px solid;
        }

        tr.R3 td.R3C1 {
            font-family: Arial;
            font-size: 10pt;
            font-style: normal;
            font-weight: bold;
            text-align: center;
        }

        tr.R3 td.R41C18 {
            font-family: Arial;
            font-size: 9pt;
            font-style: normal;
            border-bottom: #ffffff 1px none;
        }

        tr.R3 td.R41C8 {
            border-bottom: #ffffff 1px none;
        }

        tr.R3 td.R44C21 {
            font-family: Arial;
            font-size: 10pt;
            font-style: normal;
            font-weight: bold;
            text-align: center;
            border-bottom: #ffffff 1px none;
        }

        tr.R3 td.R4C1 {
            font-family: Arial;
            font-size: 10pt;
            font-style: normal;
            vertical-align: top;
            border-left: #000000 1px solid;
            border-top: #000000 1px solid;
            border-right: #000000 1px solid;
        }

        tr.R3 td.R4C19 {
            font-family: Arial;
            font-size: 10pt;
            font-style: normal;
            vertical-align: medium;
            border-left: #000000 1px solid;
            border-top: #000000 1px solid;
            border-bottom: #000000 1px solid;
            border-right: #000000 1px solid;
        }

        tr.R3 td.R4C22 {
            font-family: Arial;
            font-size: 10pt;
            font-style: normal;
            vertical-align: medium;
            border-left: #000000 1px solid;
            border-top: #000000 1px solid;
            border-right: #000000 1px solid;
        }

        tr.R3 td.R7C1 {
            font-family: Arial;
            font-size: 10pt;
            font-style: normal;
            vertical-align: medium;
            border-left: #000000 1px solid;
            border-top: #000000 1px solid;
            border-bottom: #000000 1px solid;
        }

        tr.R3 td.R7C19 {
            font-family: Arial;
            font-size: 10pt;
            font-style: normal;
            vertical-align: top;
            border-left: #000000 1px solid;
            border-top: #000000 1px solid;
            border-right: #000000 1px solid;
        }

        tr.R3 td.R7C22 {
            font-family: Arial;
            font-size: 10pt;
            font-style: normal;
            vertical-align: top;
            border-left: #000000 1px solid;
            border-top: #000000 1px solid;
            border-bottom: #000000 1px solid;
            border-right: #000000 1px solid;
        }

        tr.R3 td.R7C3 {
            font-family: Arial;
            font-size: 10pt;
            font-style: normal;
            vertical-align: medium;
            border-top: #000000 1px solid;
            border-bottom: #000000 1px solid;
            border-right: #000000 1px solid;
        }

        tr.R3 td.R8C19 {
            font-family: Arial;
            font-size: 8pt;
            font-style: normal;
            vertical-align: medium;
            border-top: #000000 1px solid;
        }

        tr.R3 td.R8C22 {
            font-family: Arial;
            font-size: 9pt;
            font-style: normal;
            vertical-align: medium;
            border-left: #000000 1px solid;
            border-top: #000000 1px solid;
            border-right: #000000 1px solid;
        }

        tr.R3 td.R8C25 {
            font-family: Arial;
            font-size: 8pt;
            font-style: normal;
            vertical-align: medium;
            border-left: #000000 1px solid;
            border-top: #000000 1px solid;
            border-bottom: #000000 1px solid;
            border-right: #000000 1px solid;
        }

        tr.R3 td.R8C26 {
            font-family: Arial;
            font-size: 8pt;
            font-style: normal;
        }

        tr.R3 td.R8C28 {
            font-family: Arial;
            font-size: 10pt;
            font-style: normal;
            vertical-align: top;
            border-right: #000000 1px solid;
        }

        tr.R3 td.R9C22 {
            font-family: Arial;
            font-size: 9pt;
            font-style: normal;
            vertical-align: medium;
            border-left: #000000 1px solid;
            border-right: #000000 1px solid;
        }

        tr.R31 {
            height: 7px;
        }

        tr.R31 td.R31C1 {
            border-top: #000000 2px solid;
        }

        tr.R36 {
            height: 9px;
        }

        tr.R36 td.R37C1 {
            border-top: #000000 2px solid;
        }

        tr.R5 {
            height: 16px;
        }

        tr.R5 td.R5C19 {
            font-family: Arial;
            font-size: 10pt;
            font-style: normal;
            vertical-align: top;
            border-left: #000000 1px solid;
            border-top: #000000 1px solid;
            border-bottom: #000000 1px solid;
            border-right: #000000 1px solid;
        }

        tr.R5 td.R5C22 {
            font-family: Arial;
            font-size: 10pt;
            font-style: normal;
            vertical-align: top;
            border-left: #000000 1px solid;
            border-bottom: #000000 1px solid;
            border-right: #000000 1px solid;
        }

        tr.R6 {
            height: 15px;
        }

        tr.R6 td.R12C1 {
            font-family: Arial;
            font-size: 10pt;
            font-style: normal;
            vertical-align: medium;
            border-left: #000000 1px solid;
            border-top: #000000 1px solid;
            border-right: #000000 1px solid;
        }

        tr.R6 td.R14C32 {
            font-family: Arial;
            font-size: 6pt;
            font-style: normal;
            text-align: center;
            vertical-align: top;
        }

        tr.R6 td.R22C1 {
            font-family: Arial;
            font-size: 10pt;
            font-style: normal;
            font-weight: bold;
            text-align: center;
            vertical-align: medium;
            border-left: #000000 2px solid;
            border-top: #000000 2px solid;
            border-bottom: #000000 1px solid;
            border-right: #000000 1px solid;
        }

        tr.R6 td.R22C2 {
            font-family: Arial;
            font-size: 10pt;
            font-style: normal;
            font-weight: bold;
            text-align: center;
            vertical-align: medium;
            border-left: #000000 1px solid;
            border-top: #000000 2px solid;
            border-bottom: #000000 1px solid;
            border-right: #000000 1px solid;
        }

        tr.R6 td.R22C33 {
            font-family: Arial;
            font-size: 10pt;
            font-style: normal;
            font-weight: bold;
            text-align: center;
            vertical-align: medium;
            border-left: #000000 1px solid;
            border-top: #000000 2px solid;
            border-bottom: #000000 1px solid;
            border-right: #000000 2px solid;
        }

        tr.R6 td.R24C1 {
            text-align: right;
            border-left: #000000 2px solid;
            border-top: #000000 1px solid;
            border-bottom: #000000 1px solid;
            border-right: #000000 1px solid;
        }

        tr.R6 td.R24C2 {
            border-left: #000000 1px solid;
            border-top: #000000 1px solid;
            border-bottom: #000000 1px solid;
            border-right: #000000 1px solid;
        }

        tr.R6 td.R24C24 {
            text-align: right;
            border-left: #000000 1px solid;
            border-top: #000000 1px solid;
            border-bottom: #000000 1px solid;
            border-right: #000000 1px solid;
        }

        tr.R6 td.R24C3 {
            border-left: #000000 1px solid;
            border-top: #000000 1px solid;
            border-bottom: #000000 1px solid;
        }

        tr.R6 td.R24C33 {
            text-align: right;
            border-left: #000000 1px solid;
            border-top: #000000 1px solid;
            border-bottom: #000000 1px solid;
            border-right: #000000 2px solid;
        }

        tr.R6 td.R40C7 {
            text-align: center;
            vertical-align: top;
        }

        tr.R6 td.R6C1 {
            font-family: Arial;
            font-size: 8pt;
            font-style: normal;
            border-left: #000000 1px solid;
            border-bottom: #000000 1px solid;
            border-right: #000000 1px solid;
        }

        table {
            table-layout: fixed;
            padding: 0px;
            padding-left: 2px;
            vertical-align: bottom;
            border-collapse: collapse;
            width: 100%;
            font-family: Arial;
            font-size: 8pt;
            font-style: normal;
        }

        td {
            padding: 0px;
            padding-left: 2px;
            overflow: hidden;
        }
    </STYLE>
</HEAD>
<BODY STYLE="background: #ffffff; margin: 0; font-family: Arial; font-size: 8pt; font-style: normal; ">
<TABLE style="width:100%; height:0px; " CELLSPACING=0>
    <COL WIDTH=7>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL>
    <TR CLASS=R0>
        <TD>
            <DIV STYLE="position:relative; height:75px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R0C1">
            <DIV STYLE="position:relative; height:75px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R0C1">
            <DIV STYLE="position:relative; height:75px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R0C1">
            <DIV STYLE="position:relative; height:75px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R0C1">
            <DIV STYLE="position:relative; height:75px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R0C1">
            <DIV STYLE="position:relative; height:75px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R0C1">
            <DIV STYLE="position:relative; height:75px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R0C1">
            <DIV STYLE="position:relative; height:75px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R0C1" COLSPAN=20>
            <DIV STYLE="width:100%;height:75px;overflow:hidden;"><SPAN STYLE="white-space:nowrap;max-width:0px;"><b>Внимание! Счет действителен до <?=Yii::app()->getDateFormatter()->formatDateTime($true_until, "long",false);?> </b><BR>Оплата данного счета означает согласие с условиями поставки товара. <BR>Уведомление об оплате обязательно, в противном случае не гарантируется<BR>наличие товара на складе. Товар отпускается по факту прихода денег <BR>на р/с Поставщика, самовывозом, при наличии доверенности и паспорта.</SPAN>
        </TD>
        <TD CLASS="R0C1">
            <DIV STYLE="position:relative; height:75px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R0C1">
            <DIV STYLE="position:relative; height:75px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R0C1">
            <DIV STYLE="position:relative; height:75px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R0C1">
            <DIV STYLE="position:relative; height:75px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R0C1">
            <DIV STYLE="position:relative; height:75px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R0C1">
            <DIV STYLE="position:relative; height:75px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R0C1">
            <DIV STYLE="position:relative; height:75px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R0C1">
            <DIV STYLE="position:relative; height:75px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R0C1">
            <DIV STYLE="position:relative; height:75px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R0C1">
            <DIV STYLE="position:relative; height:75px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R0C1">
            <DIV STYLE="position:relative; height:75px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R0C1">
            <DIV STYLE="position:relative; height:75px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R0C1">
            <DIV STYLE="position:relative; height:75px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:75px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="width:100%;height:75px;overflow:hidden;"></DIV>
        </TD>
    </TR>
    <TR CLASS=R1>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R1C1"><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R1C1"><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R1C1"><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R1>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R1C1"><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R1C1"><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R1C1"><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R3>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R3C1" COLSPAN=39><SPAN STYLE="white-space:nowrap;max-width:0px;">Образец заполнения платежного поручения</SPAN>
        </TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R3>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R4C1" COLSPAN=18 ROWSPAN=2>
            <DIV STYLE="width:100%;height:32px;overflow:hidden;">САНКТ-ПЕТЕРБУРГСКИЙ Ф-Л ОАО "БАЛТИЙСКИЙ БАНК" Г.
                САНКТ-ПЕТЕРБУРГ
            </DIV>
        </TD>
        <TD CLASS="R4C19" COLSPAN=3><SPAN STYLE="white-space:nowrap;max-width:0px;">БИК</SPAN></TD>
        <TD CLASS="R4C22" COLSPAN=10><SPAN STYLE="white-space:nowrap;max-width:0px;">044030804</SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R5>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R5C19" COLSPAN=3 ROWSPAN=2>
            <DIV STYLE="width:100%;height:30px;overflow:hidden;"><SPAN
                    STYLE="white-space:nowrap;max-width:0px;">Сч. №</SPAN></DIV>
        </TD>
        <TD CLASS="R5C22" COLSPAN=10 ROWSPAN=2>
            <DIV STYLE="width:100%;height:30px;overflow:hidden;"><SPAN STYLE="white-space:nowrap;max-width:0px;">30101810100000000804</SPAN>
            </DIV>
        </TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R6>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R6C1" COLSPAN=18><SPAN STYLE="white-space:nowrap;max-width:0px;">Банк получателя</SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R3>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R7C1" COLSPAN=2><SPAN STYLE="white-space:nowrap;max-width:0px;">ИНН</SPAN></TD>
        <TD CLASS="R7C3" COLSPAN=7><SPAN STYLE="white-space:nowrap;max-width:0px;">781801669187</SPAN></TD>
        <TD CLASS="R7C1" COLSPAN=2><SPAN STYLE="white-space:nowrap;max-width:0px;">КПП</SPAN></TD>
        <TD CLASS="R7C3" COLSPAN=7><SPAN></SPAN></TD>
        <TD CLASS="R7C19" COLSPAN=3><SPAN STYLE="white-space:nowrap;max-width:0px;">Сч. №</SPAN></TD>
        <TD CLASS="R7C22" COLSPAN=10><SPAN STYLE="white-space:nowrap;max-width:0px;">40802810300000002679</SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R3>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R4C1" COLSPAN=18 ROWSPAN=3>
            <DIV STYLE="width:100%;height:50px;overflow:hidden;">ИП Глинка Елена Николаевна</DIV>
        </TD>
        <TD CLASS="R8C19" COLSPAN=3><SPAN STYLE="white-space:nowrap;max-width:0px;">Вид оп.</SPAN></TD>
        <TD CLASS="R8C22" COLSPAN=3><SPAN STYLE="white-space:nowrap;max-width:0px;">01</SPAN></TD>
        <TD CLASS="R8C25" COLSPAN=3><SPAN STYLE="white-space:nowrap;max-width:0px;">Срок плат.</SPAN></TD>
        <TD CLASS="R8C28" COLSPAN=4><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R3>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R8C19" COLSPAN=3><SPAN STYLE="white-space:nowrap;max-width:0px;">Наз. пл.</SPAN></TD>
        <TD CLASS="R9C22" COLSPAN=3><SPAN></SPAN></TD>
        <TD CLASS="R8C25" COLSPAN=3><SPAN STYLE="white-space:nowrap;max-width:0px;">Очер. плат.</SPAN></TD>
        <TD CLASS="R8C28" COLSPAN=4><SPAN STYLE="white-space:nowrap;max-width:0px;">5</SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R3>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R8C19" COLSPAN=3 ROWSPAN=2>
            <DIV STYLE="width:100%;height:33px;overflow:hidden;"><SPAN
                    STYLE="white-space:nowrap;max-width:0px;">Код</SPAN></DIV>
        </TD>
        <TD CLASS="R10C22" COLSPAN=3 ROWSPAN=2>
            <DIV STYLE="width:100%;height:33px;overflow:hidden;">ЗК1507ЕГ000001100002</DIV>
        </TD>
        <TD CLASS="R8C25" COLSPAN=3 ROWSPAN=2>
            <DIV STYLE="width:100%;height:33px;overflow:hidden;"><SPAN STYLE="white-space:nowrap;max-width:0px;">Рез. поле</SPAN>
            </DIV>
        </TD>
        <TD CLASS="R8C28" COLSPAN=4><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R3>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R11C1" COLSPAN=18><SPAN STYLE="white-space:nowrap;max-width:0px;">Получатель</SPAN></TD>
        <TD CLASS="R11C28" COLSPAN=4><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R6>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R12C1" COLSPAN=31 ROWSPAN=2>
            <DIV STYLE="width:100%;height:29px;overflow:hidden;"><SPAN STYLE="white-space:nowrap;max-width:0px;">Оплата по заказу клиента №<?=$order->id?></SPAN>
            </DIV>
        </TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R6>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R6>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R6C1" COLSPAN=31><SPAN STYLE="white-space:nowrap;max-width:0px;">Назначение платежа</SPAN></TD>
        <TD CLASS="R14C32" COLSPAN=8 ROWSPAN=2>
            <DIV STYLE="width:100%;height:29px;overflow:hidden;"><SPAN STYLE="white-space:nowrap;max-width:0px;">Оплатите, отсканировав код в платежном<BR>терминале или передав сотруднику банка</SPAN>
            </DIV>
        </TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R6>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R16>
        <TD>
            <DIV STYLE="position:relative; height:46px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R16C1" COLSPAN=39>
            <DIV STYLE="width:100%;height:46px;overflow:hidden;"><SPAN STYLE="white-space:nowrap;max-width:0px;">Счет на оплату № <?=$order->id?> от <?=Yii::app()->getDateFormatter()->formatDateTime($order->date, "long", false);?></SPAN>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:46px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="width:100%;height:46px;overflow:hidden;"></DIV>
        </TD>
    </TR>
    <TR CLASS=R6>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R18>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R18C1" COLSPAN=4><SPAN STYLE="white-space:nowrap;max-width:0px;">Поставщик:</SPAN></TD>
        <TD CLASS="R18C5" COLSPAN=35>ИП Глинка Елена Николаевна, ИНН 781801669187, 197183, Санкт-Петербург г, Липовая
            аллея, дом № 9А, офис 922, тел.: +7(921)947-19-31
        </TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R1>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R3>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R20C1" COLSPAN=4><SPAN STYLE="white-space:nowrap;max-width:0px;">Покупатель:</SPAN></TD>
        <TD CLASS="R20C5" COLSPAN=35><?=$order->name;?>, тел.: <?=$order->phone;?></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R6>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
</TABLE>
<TABLE style="width:100%; height:0px; " CELLSPACING=0>
    <COL WIDTH=7>
    <COL WIDTH=42>
    <COL WIDTH=63>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL>
    <TR CLASS=R6>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R22C1" ROWSPAN=2>
            <DIV STYLE="width:100%;height:29px;overflow:hidden;">№</DIV>
        </TD>
        <TD CLASS="R22C2" ROWSPAN=2>
            <DIV STYLE="width:100%;height:29px;overflow:hidden;">Артикул</DIV>
        </TD>
        <TD CLASS="R22C2" COLSPAN=21 ROWSPAN=2>
            <DIV STYLE="width:100%;height:29px;overflow:hidden;">Товары (работы, услуги)</DIV>
        </TD>
        <TD CLASS="R22C2" COLSPAN=5 ROWSPAN=2>
            <DIV STYLE="width:100%;height:29px;overflow:hidden;">Количество</DIV>
        </TD>
        <TD CLASS="R22C2" COLSPAN=4 ROWSPAN=2>
            <DIV STYLE="width:100%;height:29px;overflow:hidden;">Цена</DIV>
        </TD>
        <TD CLASS="R22C33" COLSPAN=4 ROWSPAN=2>
            <DIV STYLE="width:100%;height:29px;overflow:hidden;">Сумма</DIV>
        </TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R6>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <?php
    $i = 1;
    foreach ($order->products as $product) {
        ?>
        <TR CLASS=R6>
            <TD><SPAN></SPAN></TD>
            <TD CLASS="R24C1"><SPAN STYLE="white-space:nowrap;max-width:0px;"><?= $i; ?></SPAN></TD>
            <TD CLASS="R24C2"><SPAN STYLE="white-space:nowrap;max-width:0px;"><?= $product->sku ?></SPAN></TD>
            <TD CLASS="R24C3" COLSPAN=21><?= $product->product_name ?></TD>
            <TD CLASS="R24C24" COLSPAN=2><SPAN
                    STYLE="white-space:nowrap;max-width:0px;"><?= $product->quantity ?></SPAN></TD>
            <TD CLASS="R24C24" COLSPAN=3><SPAN STYLE="white-space:nowrap;max-width:0px;">шт</SPAN></TD>
            <TD CLASS="R24C24" COLSPAN=4><SPAN STYLE="white-space:nowrap;max-width:0px;"><?= $product->price; ?></SPAN>
            </TD>
            <TD CLASS="R24C33" COLSPAN=4><SPAN
                    STYLE="white-space:nowrap;max-width:0px;"><?= Yii::app()->getNumberFormatter()->format('#,##0.00',$product->price * $product->quantity);?></SPAN></TD>
            <TD><SPAN></SPAN></TD>
            <TD></TD>
        </TR>
        <?php $i++;
    } ?>
    <?php if($order->delivery->name == 'Доставка курьером'){?>
        <TR CLASS=R6>
            <TD><SPAN></SPAN></TD>
            <TD CLASS="R24C1"><SPAN STYLE="white-space:nowrap;max-width:0px;"><?=$i++?></SPAN></TD>
            <TD CLASS="R24C2"><SPAN STYLE="white-space:nowrap;max-width:0px;"></SPAN></TD>
            <TD CLASS="R24C3" COLSPAN=21><?=$order->delivery->name?><?=$order->delivery->description?></TD>
            <TD CLASS="R24C24" COLSPAN=2><SPAN
                    STYLE="white-space:nowrap;max-width:0px;"></SPAN></TD>
            <TD CLASS="R24C24" COLSPAN=3><SPAN STYLE="white-space:nowrap;max-width:0px;"></SPAN></TD>
            <TD CLASS="R24C24" COLSPAN=4><SPAN STYLE="white-space:nowrap;max-width:0px;"><?= Yii::app()->getNumberFormatter()->format('#,##0.00',$order->delivery->price);?></SPAN>
            </TD>
            <TD CLASS="R24C33" COLSPAN=4><SPAN
                    STYLE="white-space:nowrap;max-width:0px;"><?= Yii::app()->getNumberFormatter()->format('#,##0.00',$order->getDeliveryPrice());?></SPAN></TD>
            <TD><SPAN></SPAN></TD>
            <TD></TD>
        </TR>
    <?php } ?>
    <TR CLASS=R31>
        <TD>
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R31C1">
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:7px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="width:100%;height:7px;overflow:hidden;"></DIV>
        </TD>
    </TR>
    <TR CLASS=R3>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R32C29" COLSPAN=4><SPAN STYLE="white-space:nowrap;max-width:0px;">Итого:</SPAN></TD>
        <TD CLASS="R32C29" COLSPAN=4><SPAN STYLE="white-space:nowrap;max-width:0px;"><?= Yii::app()->getNumberFormatter()->format('#,##0.00',$order->getTotalPriceWithDelivery());?> руб.</SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
</TABLE>
<TABLE style="width:100%; height:0px; " CELLSPACING=0>
    <COL WIDTH=7>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL WIDTH=21>
    <COL>
    <TR CLASS=R3>
        <TD CLASS="R32C29" COLSPAN=36><SPAN STYLE="white-space:nowrap;max-width:0px;">Без налога (НДС)</SPAN></TD>
        <TD CLASS="R32C29" COLSPAN=4><SPAN STYLE="white-space:nowrap;max-width:0px;">-</SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R3>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R34C1" COLSPAN=39><SPAN
                STYLE="white-space:nowrap;max-width:0px;">Всего наименований <?= count($order->products); ?>
                , на сумму <?= Yii::app()->getNumberFormatter()->format('#,##0.00',$order->getTotalPriceWithDelivery());?> руб.</SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R3>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R35C1" COLSPAN=39><SPAN STYLE="white-space:nowrap;max-width:0px;"><?=$totalPriceWithDelivery;?></SPAN>
        </TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R36>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="width:100%;height:9px;overflow:hidden;"></DIV>
        </TD>
    </TR>
    <TR CLASS=R36>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD CLASS="R37C1">
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="position:relative; height:9px;width: 100%; overflow:hidden;"><SPAN></SPAN></DIV>
        </TD>
        <TD>
            <DIV STYLE="width:100%;height:9px;overflow:hidden;"></DIV>
        </TD>
    </TR>
    <TR CLASS=R6>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R3>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R39C1" COLSPAN=5><SPAN STYLE="white-space:nowrap;max-width:0px;">Руководитель</SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C18"><SPAN></SPAN></TD>
        <TD COLSPAN=2><SPAN></SPAN></TD>
        <TD CLASS="R39C21" COLSPAN=14>Глинка Е.Н.</TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R6>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R40C7" COLSPAN=12><SPAN STYLE="white-space:nowrap;max-width:0px;">подпись</SPAN></TD>
        <TD COLSPAN=4><SPAN></SPAN></TD>
        <TD CLASS="R40C7" COLSPAN=12><SPAN STYLE="white-space:nowrap;max-width:0px;">расшифровка подписи</SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R3>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R39C1"><SPAN></SPAN></TD>
        <TD CLASS="R39C1"><SPAN></SPAN></TD>
        <TD CLASS="R39C1"><SPAN></SPAN></TD>
        <TD CLASS="R39C1"><SPAN></SPAN></TD>
        <TD CLASS="R39C1"><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R41C8"><SPAN></SPAN></TD>
        <TD CLASS="R41C8"><SPAN></SPAN></TD>
        <TD CLASS="R41C8"><SPAN></SPAN></TD>
        <TD CLASS="R41C8"><SPAN></SPAN></TD>
        <TD CLASS="R41C8"><SPAN></SPAN></TD>
        <TD CLASS="R41C8"><SPAN></SPAN></TD>
        <TD CLASS="R41C8"><SPAN></SPAN></TD>
        <TD CLASS="R41C8"><SPAN></SPAN></TD>
        <TD CLASS="R41C8"><SPAN></SPAN></TD>
        <TD CLASS="R41C8"><SPAN></SPAN></TD>
        <TD CLASS="R41C18"><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R41C18"><SPAN></SPAN></TD>
        <TD CLASS="R41C18"><SPAN></SPAN></TD>
        <TD CLASS="R41C18"><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R3>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R39C1" COLSPAN=6><SPAN STYLE="white-space:nowrap;max-width:0px;">Бухгалтер</SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C18"><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R39C21" COLSPAN=14><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R6>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R40C7" COLSPAN=12><SPAN STYLE="white-space:nowrap;max-width:0px;">подпись</SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R40C7"><SPAN></SPAN></TD>
        <TD CLASS="R40C7"><SPAN></SPAN></TD>
        <TD CLASS="R40C7" COLSPAN=12><SPAN STYLE="white-space:nowrap;max-width:0px;">расшифровка подписи</SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R3>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R39C1"><SPAN></SPAN></TD>
        <TD CLASS="R39C1"><SPAN></SPAN></TD>
        <TD CLASS="R39C1"><SPAN></SPAN></TD>
        <TD CLASS="R39C1"><SPAN></SPAN></TD>
        <TD CLASS="R39C1"><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R41C18"><SPAN></SPAN></TD>
        <TD CLASS="R41C18"><SPAN></SPAN></TD>
        <TD CLASS="R41C18"><SPAN></SPAN></TD>
        <TD CLASS="R41C18"><SPAN></SPAN></TD>
        <TD CLASS="R41C18"><SPAN></SPAN></TD>
        <TD CLASS="R41C18"><SPAN></SPAN></TD>
        <TD CLASS="R41C18"><SPAN></SPAN></TD>
        <TD CLASS="R41C18"><SPAN></SPAN></TD>
        <TD CLASS="R41C18"><SPAN></SPAN></TD>
        <TD CLASS="R41C18"><SPAN></SPAN></TD>
        <TD CLASS="R41C18"><SPAN></SPAN></TD>
        <TD CLASS="R41C18"><SPAN></SPAN></TD>
        <TD CLASS="R41C8"><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R44C21"><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R3>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R39C1" COLSPAN=6><SPAN STYLE="white-space:nowrap;max-width:0px;">Менеджер</SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R39C7"><SPAN></SPAN></TD>
        <TD CLASS="R41C8"><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R39C21" COLSPAN=14></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
    <TR CLASS=R6>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R40C7" COLSPAN=12><SPAN STYLE="white-space:nowrap;max-width:0px;">подпись</SPAN></TD>
        <TD CLASS="R40C7"><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD CLASS="R40C7"><SPAN></SPAN></TD>
        <TD CLASS="R40C7"><SPAN></SPAN></TD>
        <TD CLASS="R40C7" COLSPAN=12><SPAN STYLE="white-space:nowrap;max-width:0px;">расшифровка подписи</SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD><SPAN></SPAN></TD>
        <TD></TD>
    </TR>
</TABLE>
<div style="position:absolute;left:7px; top:1px;">
    <IMG alt="Logo" src="<?=$mainAssets?>/images/previa-big.png">
</div>
<div style="position:absolute;left:672px; top:119px;">
    <IMG alt="Счет на оплату № <?=$order->id?> от <?=Yii::app()->getDateFormatter()->formatDateTime($order->date, "long", false);?>" src="<?=$qrCode->getDataUri();?>" />
</div>
</BODY>
</HTML>
