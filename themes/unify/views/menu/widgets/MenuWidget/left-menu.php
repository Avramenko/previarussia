<?php
Yii::import('application.modules.menu.components.YMenu');
$this->widget(
    'booster.widgets.TbMenu',
    [
        'type' => 'list',
        'items' => $this->params['items'],
        'htmlOptions' =>[
            'class' => 'list-group sidebar-nav-v1',
            'id' => 'sidebar-nav'
        ]
    ]
);