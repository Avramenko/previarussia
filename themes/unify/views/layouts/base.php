<!DOCTYPE html>
<html lang="<?= Yii::app()->language; ?>">
<head>

    <?php Yii::app()->getController()->widget(
        'vendor.chemezov.yii-seo.widgets.SeoHead',
        array(
            'httpEquivs' => array(
                'Content-Type' => 'text/html; charset=utf-8',
                'X-UA-Compatible' => 'IE=edge,chrome=1',
                'Content-Language' => Yii::app()->language
            ),
            'defaultTitle' => $this->yupe->siteName,
            'defaultDescription' => $this->yupe->siteDescription,
            'defaultKeywords' => $this->yupe->siteKeyWords,
        )
    );
    $mainAssets = Yii::app()->getTheme()->getAssetsUrl();


    /*Project CSS*/
    //Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/main.css');
    //Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/flags.css');
    //Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/yupe.css');
    Yii::app()->getClientScript()->registerCssFile('//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/shop-ui/plugins/bootstrap/css/bootstrap.min.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/shop-ui/css/shop.style.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/shop-ui/css/headers/header-v5.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/shop-ui/css/footers/footer-v4.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/shop-ui/plugins/animate.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/shop-ui/plugins/line-icons/line-icons.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/shop-ui/plugins/font-awesome/css/font-awesome.min.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/shop-ui/plugins/scrollbar/css/jquery.mCustomScrollbar.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/shop-ui/plugins/owl-carousel/owl-carousel/owl.carousel.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/shop-ui/plugins/revolution-slider/rs-plugin/css/settings.css');
    //Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/parallax-slider/css/parallax-slider.css');
    //Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/owl-carousel/owl-carousel/owl.carousel.css');
    //Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/revolution-slider/rs-plugin/css/settings.css');
    //Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/theme-colors/default.css');
    //Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/theme-colors/light.css');
    //Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/theme-skins/dark.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/shop-ui/css/custom.css');

    /*Project JS*/
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/shop-ui/plugins/jquery/jquery-migrate.min.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/shop-ui/plugins/back-to-top.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/shop-ui/plugins/smoothScroll.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/shop-ui/plugins/jquery.parallax.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/shop-ui/plugins/owl-carousel/owl-carousel/owl.carousel.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/shop-ui/plugins/scrollbar/js/jquery.mCustomScrollbar.concat.min.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/shop-ui/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/shop-ui/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js');

    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/owl-carousel/owl-carousel/owl.carousel.js');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/plugins/owl-carousel.js');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/plugins/revolution-slider.js');

    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/blog.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/bootstrap-notify.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/jquery.li-translit.js');

    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/shop-ui/js/custom.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/shop-ui/js/shop.app.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/shop-ui/js/plugins/owl-carousel.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/shop-ui/js/plugins/revolution-slider.js');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/plugins/owl-carousel.js');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/plugins/revolution-slider.js');
    //Yii::app()->getClientScript()->registerScriptFile('http://maps.google.com/maps/api/js?sensor=true');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/gmap/gmap.js');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/pages/page_contacts.js');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/plugins/parallax-slider.js');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/respond.js', 'screen','<!--[if lt IE 9]>','<![endif]-->');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/html5shiv.js', 'screen','<!--[if lt IE 9]>','<![endif]-->');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/placeholder-IE-fixes.js', 'screen','<!--[if lt IE 9]>','<![endif]-->');

    Yii::app()->getClientScript()->registerScript('AppInit',"
        jQuery(document).ready(function() {
            App.init();
            App.initScrollBar();
            App.initParallaxBg();
            OwlCarousel.initOwlCarousel();
            RevolutionSlider.initRSfullWidth();
        });
    ", CClientScript::POS_END);

    ?>





    <script type="text/javascript">
        var yupeTokenName = '<?= Yii::app()->getRequest()->csrfTokenName;?>';
        var yupeToken = '<?= Yii::app()->getRequest()->getCsrfToken();?>';
    </script>
    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body class="header-fixed">

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-68095615-1', 'auto');
    ga('send', 'pageview');

</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter32681170 = new Ya.Metrika({
                    id:32681170,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/32681170" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->




<div class="wrapper">


    <!--=== Header v5 ===-->
    <div class="header-v5 header-static">
        <!-- Topbar v3 -->
        <div class="topbar-v3">
            <div class="search-open">
                <div class="container">
                    <form action="/search" type="get">
                    <input type="text" name="q" class="form-control" placeholder="Поиск"></form>
                    <div class="search-close"><i class="icon-close"></i></div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">
                        <ul class="list-inline right-topbar pull-right">
                            <?php if(Yii::app()->getUser()->isGuest){?>


                            <li><?php
                                echo Chtml::link(
                                    'Вход',
                                    Yii::app()->createUrl('user/account/login'),
                                    [
                                        'title'=>'Вход',
                                        'alt' => 'Страница авторизации пользователей'
                                    ]);
                                ?> |
                                <?php
                                echo Chtml::link(
                                    'Регистрация',
                                    Yii::app()->createUrl('user/account/registration'),
                                    [
                                        'title'=>'Регистрация нового пользователя',
                                        'alt' => 'Регистрация на сайте'
                                    ]);
                                ?>
                            </li>
                            <?php }else{ ?>
                                <li><?php
                                    echo Chtml::link(
                                        'Аккаунт',
                                        Yii::app()->createUrl('user/people/userInfo', ['username' => Chtml::encode(Yii::app()->getUser()->nick_name)]),
                                        [
                                            'title'=>'Аккаунт',
                                            'alt' => Chtml::encode(Yii::app()->getUser()->nick_name)
                                        ]);
                                    ?> |
                                <?php
                                    echo Chtml::link(
                                        'Выход',
                                        Yii::app()->createUrl('user/account/logout'),
                                        [
                                            'title'=>'Выход',
                                            'alt' => ''
                                        ]);
                                    ?>
                                </li>
                            <?php } ?>

                            <li><i class="search fa fa-search search-button"></i></li>
                        </ul>
                    </div>
                </div>
            </div><!--/container-->
        </div>
        <!-- End Topbar v3 -->

        <!-- Navbar -->
        <div class="navbar navbar-default mega-menu" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                        <span class="sr-only">Меню</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">
                        <img id="logo-header" src="<?=$mainAssets?>/images/previa-small.png" alt="previa russia">
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-responsive-collapse">
                    <?php if (Yii::app()->hasModule('cart')): ?>
                        <?php $this->widget('application.modules.cart.widgets.ShoppingCartWidget',['view'=>'topMenuShoppingCart']); ?>
                    <?php endif; ?>
                    <!-- Shopping Cart -->
                    <!--<ul class="list-inline shop-badge badge-lists badge-icons pull-right">
                        <li>
                            <a href="#"><i class="fa fa-shopping-cart"></i></a>
                            <span class="badge badge-sea rounded-x">3</span>
                            <ul class="list-unstyled badge-open mCustomScrollbar" data-mcs-theme="minimal-dark">
                                <li>
                                    <img src="assets/img/thumb/05.jpg" alt="">
                                    <button type="button" class="close">×</button>
                                    <div class="overflow-h">
                                        <span>Black Glasses</span>
                                        <small>1 x $400.00</small>
                                    </div>
                                </li>
                                <li>
                                    <img src="assets/img/thumb/02.jpg" alt="">
                                    <button type="button" class="close">×</button>
                                    <div class="overflow-h">
                                        <span>Black Glasses</span>
                                        <small>1 x $400.00</small>
                                    </div>
                                </li>
                                <li>
                                    <img src="assets/img/thumb/03.jpg" alt="">
                                    <button type="button" class="close">×</button>
                                    <div class="overflow-h">
                                        <span>Black Glasses</span>
                                        <small>1 x $400.00</small>
                                    </div>
                                </li>
                                <li class="subtotal">
                                    <div class="overflow-h margin-bottom-10">
                                        <span>Subtotal</span>
                                        <span class="pull-right subtotal-cost">$1200.00</span>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <a href="shop-ui-inner.html" class="btn-u btn-brd btn-brd-hover btn-u-sea-shop btn-block">View Cart</a>
                                        </div>
                                        <div class="col-xs-6">
                                            <a href="shop-ui-add-to-cart.html" class="btn-u btn-u-sea-shop btn-block">Checkout</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>-->
                    <!-- End Shopping Cart -->

                    <?php if (Yii::app()->hasModule('menu')): ?>
                        <?php $this->widget(
                            'application.modules.menu.widgets.MenuWidget', [
                                'name' => 'top-menu',
                                'layout' => 'top-menu'
                            ]
                        ); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <!-- End Navbar -->
    </div>
    <!--=== End Header v5 ===-->








    <?php /*CVarDumper::dump(Yii::app()->getRequest()->requestUri);*/?>
    <?php if(Yii::app()->getRequest()->requestUri == '/'){?>
        <?php $this->widget(
            "application.modules.contentblock.widgets.ContentBlockWidget",
            array("code" => "banner-na-glavnoy"));
        ?>
    <?php }else{ ?>
        <!--=== Breadcrumbs v4 ===-->
        <div class="breadcrumbs-v4">
            <div class="container">
                <span class="page-name"></span>
                <h1><?php echo $this->pageTitle;?></h1>
                <?php $this->widget(
                    'bootstrap.widgets.TbBreadcrumbs',
                    [
                        'links' => $this->breadcrumbs,
                        'htmlOptions' => [
                            'class' => 'breadcrumb-v4-in'
                        ]
                    ]
                );?>
            </div><!--/end container-->
        </div>
        <!--=== End Breadcrumbs v4 ===-->
    <?php } ?>


    <?php //container content row margin-bottom-30 col-md-12 headline h2?>
    <?php
    /*echo Chtml::link(
        'textlink',
        Yii::app()->createUrl('store/catalog/category', ['path' => 'professional/curlfriends']),
        [
            'title'=>'newtitle',
            'alt' => 'my first link'
        ]);*/
    ?>
    <?php
    /*echo CVarDumper::dump(Yii::app()->getUser());*/
    ?>
    <?= $content; ?>

    <!-- footer -->
    <?php /*$this->widget(
        "application.modules.contentblock.widgets.ContentBlockWidget",
        array("code" => "footer-block"));
    */?>
    <?php $this->renderPartial('//layouts/_footer'); ?>
    <!-- footer end -->
    <!--[if lt IE 9]>
        <script src="<?=$mainAssets?>/shop-ui/plugins/respond.js"></script>
        <script src="<?=$mainAssets?>/shop-ui/plugins/html5shiv.js"></script>
        <script src="<?=$mainAssets?>/shop-ui/js/plugins/placeholder-IE-fixes.js"></script>
    <![endif]-->
</div>
</body>
</html>