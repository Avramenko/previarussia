<?php $this->beginContent('//layouts/base'); ?>

<div class="container content">
    <div class="row">
        <div class="col-md-12">
            <?= $content; ?>
        </div>
    </div>
</div>
<?php $this->endContent(); ?>
