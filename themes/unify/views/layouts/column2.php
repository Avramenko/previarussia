<?php $this->beginContent('//layouts/base'); ?>

    <div class="container content">
        <div class="row">
            <div class="col-md-9">
                <?= $content; ?>
            </div>
            <div class="col-md-3">
                <p>second column</p>
            </div>

        </div>
    </div>
<?php $this->endContent(); ?>