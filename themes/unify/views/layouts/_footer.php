<?php
$mainAssets = Yii::app()->getTheme()->getAssetsUrl();
?>
<!--=== Footer v4 ===-->
<div class="footer-v4">
    <div class="footer">
        <div class="container">
            <div class="row">
                <!-- About -->
                <div class="col-md-4 md-margin-bottom-40">
                    <a href="/"><img class="footer-logo" src="<?=$mainAssets?>/images/previa-big.png" alt=""></a>

                    <ul class="list-unstyled address-list margin-bottom-20">
                        <li><i class="fa fa-angle-right"></i>Россия, Санкт-Петербург, Липовая аллея, д.9, оф. 922, БЦ "Приморский"</li>
                        <li><i class="fa fa-angle-right"></i>Телефон: <a href="tel:+7 (921) 945-40-22">+7 (921) 945-40-22</a></li>
                        <li><i class="fa fa-angle-right"></i>Сайт: <a href="">www.previarussia.ru</a></li>
                        <li><i class="fa fa-angle-right"></i>E-mail: info@previarussia.ru</li>
                    </ul>

                    <ul class="list-inline shop-social">
                        <li><a href="#"><i class="fb rounded-md fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="tw rounded-md fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="gp rounded-md fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="yt rounded-md fa fa-youtube"></i></a></li>
                    </ul>
                </div>
                <!-- End About -->

                <!-- Simple List -->
                <div class="col-md-3 col-sm-4">
                    <div class="row">
                        <div class="col-sm-12 col-xs-6">
                            <h2 class="thumb-headline">Поддерживающий уход</h2>
                            <ul class="list-unstyled simple-list margin-bottom-20">
                                <li><i class="fa fa-angle-right"></i> <a href="#">Четкие и упругие локоны</a></li>
                                <li><i class="fa fa-angle-right"></i> <a href="#">Идеальное и стойкое выпрямление</a></li>
                                <li><i class="fa fa-angle-right"></i> <a href="#">Стойкий и блестящий цвет </a></li>
                                <li><i class="fa fa-angle-right"></i> <a href="#">Платиновый блонд и седые волосы</a></li>
                                <li><i class="fa fa-angle-right"></i> <a href="#">Очищение волос и кожи головы</a></li>
                                <li><i class="fa fa-angle-right"></i> <a href="#">Поврежденные и ломкие волосы</a></li>
                                <li><i class="fa fa-angle-right"></i> <a href="#">Тонкие и слабые волосы</a></li>
                                <li><i class="fa fa-angle-right"></i> <a href="#">Создание собственного стиля</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-4">
                    <div class="row">
                        <div class="col-sm-12 col-xs-6">
                            <h2 class="thumb-headline">Профессиональные средства</h2>
                            <ul class="list-unstyled simple-list margin-bottom-20">
                                <li><i class="fa fa-angle-right"></i> <a href="#">Реконструкция</a></li>
                                <li><i class="fa fa-angle-right"></i> <a href="#">Завивка</a></li>
                                <li><i class="fa fa-angle-right"></i> <a href="#">Выпрямление </a></li>
                                <li><i class="fa fa-angle-right"></i> <a href="#">Окрашивание </a></li>
                                <li><i class="fa fa-angle-right"></i> <a href="#">Осветление </a></li>
                                <li><i class="fa fa-angle-right"></i> <a href="#">Очищение </a></li>
                                <li><i class="fa fa-angle-right"></i> <a href="#">Объем </a></li>
                                <li><i class="fa fa-angle-right"></i> <a href="#">Стайлинг </a></li>
                            </ul>
                        </div>

                    </div>
                </div>


                <!-- End Simple List -->
            </div><!--/end row-->
        </div><!--/end continer-->
    </div><!--/footer-->

    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p>
                        <?=date("Y");?> &copy; PREVIA RUSSIA. ALL Rights Reserved.
                        <a href="#">Privacy Policy</a> | <a href="#">Terms of Service</a>
                    </p>
                </div>
                <div class="col-md-6">
                    <ul class="list-inline sponsors-icons pull-right">
                        <li><i class="fa fa-cc-paypal"></i></li>
                        <li><i class="fa fa-cc-visa"></i></li>
                        <li><i class="fa fa-cc-mastercard"></i></li>
                        <li><i class="fa fa-cc-discover"></i></li>
                    </ul>
                </div>
            </div>
        </div>
    </div><!--/copyright-->
</div>
<!--=== End Footer v4 ===-->