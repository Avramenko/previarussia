<?php $count = Yii::app()->cart->getCount();
$cart = Yii::app()->cart;
?>
<div class="widget shopping-cart-widget" id="shopping-cart-widget">
    <ul class="list-inline shop-badge badge-lists badge-icons pull-right" id="cart-widget" data-cart-widget-url="<?= Yii::app()->createUrl('/cart/cart/widget');?>">
        <li>
            <a href="#"><i class="fa fa-shopping-cart"></i></a>
            <span class="badge badge-sea rounded-x"><?=$count?></span>

            <ul class="list-unstyled badge-open mCustomScrollbar" data-mcs-theme="minimal-dark">

                <?php if($count > 0){

                    foreach(Yii::app()->cart->positions as $position){?>
                        <li>
                            <img src="<?= $position->getImageUrl(50, 80); ?>"/>

                            <button type="button" class="close">×</button>
                            <div class="overflow-h">
                                <span><?php echo $position->name;?></span>
                                <small><?php echo $position->getQuantity();?> x <?php echo $position->price;?></small>

                            </div>
                        </li>
                    <?php };
                    ?>
                <?php } ?>
                <?php if (Yii::app()->cart->isEmpty()){ ?>
                    <li class="subtotal">
                        <div class="overflow-h margin-bottom-10">
                            <span><?= Yii::t("CartModule.cart", "There are no products in cart"); ?></span>
                        </div>
                    </li>
                <?php }else{ ?>
                    <li class="subtotal">
                        <div class="overflow-h margin-bottom-10">
                            <span>ИТОГО:</span>

                            <span class="pull-right subtotal-cost"><?= Yii::app()->cart->getCost(); ?> <?= Yii::t("CartModule.cart", "RUB"); ?></span>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <!--<a href="shop-ui-add-to-cart.html" class="btn-u btn-u-sea-shop btn-block">Checkout</a>-->
                            </div>
                            <div class="col-xs-6">
                                <a href="<?= Yii::app()->createUrl('cart/cart/index'); ?>" class="btn-u btn-brd btn-brd-hover btn-u-sea-shop btn-block"><?= Yii::t("CartModule.cart", "Cart"); ?></a>
                            </div>

                        </div>
                    </li>
                <?php } ?>

            </ul>
        </li>
    </ul>
</div>