<?php
/* @var $this CartController */
/* @var $positions Product[] */
/* @var $order Order */
/* @var $coupons Coupon[] */
Yii::app()->getClientScript()->registerCssFile(Yii::app()->getModule('cart')->getAssetsUrl(). '/css/cart-frontend.css');

$mainAssets = Yii::app()->getTheme()->getAssetsUrl();
Yii::app()->getClientScript()->registerCssFile($mainAssets . '/shop-ui/css/custom.css');
/*Yii::app()->getClientScript()->registerCssFile($mainAssets . '/shop-ui/plugins/jquery-steps/css/custom-jquery.steps.css');
Yii::app()->getClientScript()->registerCssFile($mainAssets . '/shop-ui/plugins/scrollbar/css/jquery.mCustomScrollbar.css');*/
Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/store.js');
/*Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/shop-ui/js/forms/page_login.js');
Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/shop-ui/js/plugins/stepWizard.js');
Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/shop-ui/js/forms/product-quantity.js');*/
Yii::app()->getClientScript()->registerScript("initStepWizard", '
    jQuery(document).ready(function() {
        //StepWizard.initStepWizard();
    });
', CClientScript::POS_END);

$this->title = Yii::t('CartModule.cart', 'Cart');
$this->breadcrumbs = [
    Yii::t("CartModule.cart", 'Catalog') => ['/store/catalog/index'],
    Yii::t("CartModule.cart", 'Cart')
];
?>


<!--=== Content Medium Part ===-->
<div class="content-md margin-bottom-30">
    <div class="container">
        <form class="shopping-cart" action="#">
            <div>
                <div class="header-tags">
                    <div class="overflow-h">
                        <h2>Shopping Cart</h2>
                        <p>Review &amp; edit your product</p>
                        <i class="rounded-x fa fa-check"></i>
                    </div>
                </div>
                <section>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Product</th>
                                <th>Price</th>
                                <th>Qty</th>
                                <th>Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="product-in-table">
                                    <img class="img-responsive" src="assets/img/thumb/08.jpg" alt="">
                                    <div class="product-it-in">
                                        <h3>Double-Breasted</h3>
                                        <span>Sed aliquam tincidunt tempus</span>
                                    </div>
                                </td>
                                <td>$ 160.00</td>
                                <td>
                                    <button type='button' class="quantity-button" name='subtract' onclick='javascript: subtractQty1();' value='-'>-</button>
                                    <input type='text' class="quantity-field" name='qty1' value="5" id='qty1'/>
                                    <button type='button' class="quantity-button" name='add' onclick='javascript: document.getElementById("qty1").value++;' value='+'>+</button>
                                </td>
                                <td class="shop-red">$ 320.00</td>
                                <td>
                                    <button type="button" class="close"><span>&times;</span><span class="sr-only">Close</span></button>
                                </td>
                            </tr>
                            <tr>
                                <td class="product-in-table">
                                    <img class="img-responsive" src="assets/img/thumb/07.jpg" alt="">
                                    <div class="product-it-in">
                                        <h3>Vivamus ligula</h3>
                                        <span>Sed aliquam tincidunt tempus</span>
                                    </div>
                                </td>
                                <td>$ 160.00</td>
                                <td>
                                    <button type='button' class="quantity-button" name='subtract' onclick='javascript: subtractQty2();' value='-'>-</button>
                                    <input type='text' class="quantity-field" name='qty2' value="3" id='qty2'/>
                                    <button type='button' class="quantity-button" name='add' onclick='javascript: document.getElementById("qty2").value++;' value='+'>+</button>
                                </td>
                                <td class="shop-red">$ 320.00</td>
                                <td>
                                    <button type="button" class="close"><span>&times;</span><span class="sr-only">Close</span></button>
                                </td>
                            </tr>
                            <tr>
                                <td class="product-in-table">
                                    <img class="img-responsive" src="assets/img/thumb/06.jpg" alt="">
                                    <div class="product-it-in">
                                        <h3>Vivamus ligula</h3>
                                        <span>Sed aliquam tincidunt tempus</span>
                                    </div>
                                </td>
                                <td>$ 160.00</td>
                                <td>
                                    <button type='button' class="quantity-button" name='subtract' onclick='javascript: subtractQty3();' value='-'>-</button>
                                    <input type='text' class="quantity-field" name='qty3' value="1" id='qty3'/>
                                    <button type='button' class="quantity-button" name='add' onclick='javascript: document.getElementById("qty3").value++;' value='+'>+</button>
                                </td>
                                <td class="shop-red">$ 320.00</td>
                                <td>
                                    <button type="button" class="close"><span>&times;</span><span class="sr-only">Close</span></button>
                                </td>
                            </tr>
                            <tr>
                                <td class="product-in-table">
                                    <img class="img-responsive" src="assets/img/thumb/09.jpg" alt="">
                                    <div class="product-it-in">
                                        <h3>Vivamus ligula</h3>
                                        <span>Sed aliquam tincidunt tempus</span>
                                    </div>
                                </td>
                                <td>$ 160.00</td>
                                <td>
                                    <button type='button' class="quantity-button" name='subtract' onclick='javascript: subtractQty4();' value='-'>-</button>
                                    <input type='text' class="quantity-field" name='qty4' value="7" id='qty4'/>
                                    <button type='button' class="quantity-button" name='add' onclick='javascript: document.getElementById("qty4").value++;' value='+'>+</button>
                                </td>
                                <td class="shop-red">$ 320.00</td>
                                <td>
                                    <button type="button" class="close"><span>&times;</span><span class="sr-only">Close</span></button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </section>

                <div class="header-tags">
                    <div class="overflow-h">
                        <h2>Billing info</h2>
                        <p>Shipping and address infot</p>
                        <i class="rounded-x fa fa-home"></i>
                    </div>
                </div>
                <section class="billing-info">
                    <div class="row">
                        <div class="col-md-6 md-margin-bottom-40">
                            <h2 class="title-type">Billing Address</h2>
                            <div class="billing-info-inputs checkbox-list">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input id="name" type="text" placeholder="First Name" name="firstname" class="form-control required">
                                        <input id="email" type="text" placeholder="Email" name="email" class="form-control required email">
                                    </div>
                                    <div class="col-sm-6">
                                        <input id="surname" type="text" placeholder="Last Name" name="lastname" class="form-control required">
                                        <input id="phone" type="tel" placeholder="Phone" name="phone" class="form-control required">
                                    </div>
                                </div>
                                <input id="billingAddress" type="text" placeholder="Address Line 1" name="address1" class="form-control required">
                                <input id="billingAddress2" type="text" placeholder="Address Line 2" name="address2" class="form-control required">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input id="city" type="text" placeholder="City" name="city" class="form-control required">
                                    </div>
                                    <div class="col-sm-6">
                                        <input id="zip" type="text" placeholder="Zip/Postal Code" name="zip" class="form-control required">
                                    </div>
                                </div>

                                <label class="checkbox text-left">
                                    <input type="checkbox" name="checkbox"/>
                                    <i></i>
                                    Ship item to the above billing address
                                </label>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <h2 class="title-type">Shipping Address</h2>
                            <div class="billing-info-inputs checkbox-list">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input id="name2" type="text" placeholder="First Name" name="firstname" class="form-control">
                                        <input id="email2" type="text" placeholder="Email" name="email" class="form-control email">
                                    </div>
                                    <div class="col-sm-6">
                                        <input id="surname2" type="text" placeholder="Last Name" name="lastname" class="form-control">
                                        <input id="phone2" type="tel" placeholder="Phone" name="phone" class="form-control">
                                    </div>
                                </div>
                                <input id="shippingAddress" type="text" placeholder="Address Line 1" name="address1" class="form-control">
                                <input id="shippingAddress2" type="text" placeholder="Address Line 2" name="address2" class="form-control">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input id="city2" type="text" placeholder="City" name="city" class="form-control">
                                    </div>
                                    <div class="col-sm-6">
                                        <input id="zip2" type="text" placeholder="Zip/Postal Code" name="zip" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <div class="header-tags">
                    <div class="overflow-h">
                        <h2>Payment</h2>
                        <p>Select Payment method</p>
                        <i class="rounded-x fa fa-credit-card"></i>
                    </div>
                </div>
                <section>
                    <div class="row">
                        <div class="col-md-6 md-margin-bottom-50">
                            <h2 class="title-type">Choose a payment method</h2>
                            <!-- Accordion -->
                            <div class="accordion-v2">
                                <div class="panel-group" id="accordion">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                    <i class="fa fa-credit-card"></i>
                                                    Credit or Debit Card
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse in">
                                            <div class="panel-body cus-form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-sm-4 no-col-space control-label">Cardholder Name</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control required" name="cardholder" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-4 no-col-space control-label">Card Number</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control required" name="cardnumber" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-4 no-col-space control-label">Payment Types</label>
                                                    <div class="col-sm-8">
                                                        <ul class="list-inline payment-type">
                                                            <li><i class="fa fa-cc-paypal"></i></li>
                                                            <li><i class="fa fa-cc-visa"></i></li>
                                                            <li><i class="fa fa-cc-mastercard"></i></li>
                                                            <li><i class="fa fa-cc-discover"></i></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-4">Expiration Date</label>
                                                    <div class="col-sm-8 input-small-field">
                                                        <input type="text" name="mm" placeholder="MM" class="form-control required sm-margin-bottom-20">
                                                        <span class="slash">/</span>
                                                        <input type="text" name="yy" placeholder="YY" class="form-control required">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-4 no-col-space control-label">CSC</label>
                                                    <div class="col-sm-8 input-small-field">
                                                        <input type="text" name="number" placeholder="" class="form-control required">
                                                        <a href="#">What's this?</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                                    <i class="fa fa-paypal"></i>
                                                    Pay with PayPal
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseTwo" class="panel-collapse collapse">
                                            <div class="content margin-left-10">
                                                <a href="#"><img src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/PP_logo_h_150x38.png" alt="PayPal"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Accordion -->
                        </div>

                        <div class="col-md-6">
                            <h2 class="title-type">Frequently asked questions</h2>
                            <!-- Accordion -->
                            <div class="accordion-v2 plus-toggle">
                                <div class="panel-group" id="accordion-v2">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-v2" href="#collapseOne-v2">
                                                    What payments methods can I use?
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne-v2" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam hendrerit, felis vel tincidunt sodales, urna metus rutrum leo, sit amet finibus velit ante nec lacus. Cras erat nunc, pulvinar nec leo at, rhoncus elementum orci. Nullam ut sapien ultricies, gravida ante ut, ultrices nunc.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion-v2" href="#collapseTwo-v2">
                                                    Can I use gift card to pay for my purchase?
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseTwo-v2" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam hendrerit, felis vel tincidunt sodales, urna metus rutrum leo, sit amet finibus velit ante nec lacus. Cras erat nunc, pulvinar nec leo at, rhoncus elementum orci. Nullam ut sapien ultricies, gravida ante ut, ultrices nunc.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion-v2" href="#collapseThree-v2">
                                                    Will I be charged when I place my order?
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseThree-v2" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam hendrerit, felis vel tincidunt sodales, urna metus rutrum leo, sit amet finibus velit ante nec lacus. Cras erat nunc, pulvinar nec leo at, rhoncus elementum orci. Nullam ut sapien ultricies, gravida ante ut, ultrices nunc.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion-v2" href="#collapseFour-v2">
                                                    How long will it take to get my order?
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseFour-v2" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam hendrerit, felis vel tincidunt sodales, urna metus rutrum leo, sit amet finibus velit ante nec lacus. Cras erat nunc, pulvinar nec leo at, rhoncus elementum orci. Nullam ut sapien ultricies, gravida ante ut, ultrices nunc.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Accordion -->
                        </div>
                    </div>
                </section>

                <div class="coupon-code">
                    <div class="row">
                        <div class="col-sm-4 sm-margin-bottom-30">
                            <h3>Discount Code</h3>
                            <p>Enter your coupon code</p>
                            <input class="form-control margin-bottom-10" name="code" type="text">
                            <button type="button" class="btn-u btn-u-sea-shop">Apply Coupon</button>
                        </div>
                        <div class="col-sm-3 col-sm-offset-5">
                            <ul class="list-inline total-result">
                                <li>
                                    <h4>Subtotal:</h4>
                                    <div class="total-result-in">
                                        <span>$ 1280.00</span>
                                    </div>
                                </li>
                                <li>
                                    <h4>Shipping:</h4>
                                    <div class="total-result-in">
                                        <span class="text-right">- - - -</span>
                                    </div>
                                </li>
                                <li class="divider"></li>
                                <li class="total-price">
                                    <h4>Total:</h4>
                                    <div class="total-result-in">
                                        <span>$ 1280.00</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div><!--/end container-->
</div>
<!--=== End Content Medium Part ===-->





<script type="text/javascript">
    var yupeCartDeleteProductUrl = '<?= Yii::app()->createUrl('/cart/cart/delete/')?>';
    var yupeCartUpdateUrl = '<?= Yii::app()->createUrl('/cart/cart/update/')?>';
    var yupeCartWidgetUrl = '<?= Yii::app()->createUrl('/cart/cart/widget/')?>';
</script>

<div class="row">
    <div class="col-sm-12">
        <?php if (Yii::app()->cart->isEmpty()): ?>
            <h1><?= Yii::t("CartModule.cart", "Cart is empty"); ?></h1>
            <?= Yii::t("CartModule.cart", "There are no products in cart"); ?>
        <?php else: ?>
            <?php
            $form = $this->beginWidget(
                'bootstrap.widgets.TbActiveForm',
                [
                    'action' => ['/order/order/create'],
                    'id' => 'order-form',
                    'enableAjaxValidation' => false,
                    'enableClientValidation' => true,
                    'clientOptions' => [
                        'validateOnSubmit' => true,
                        'validateOnChange' => true,
                        'validateOnType' => false,
                        'beforeValidate' => 'js:function(form){$(form).find("button[type=\'submit\']").prop("disabled", true); return true;}',
                        'afterValidate' => 'js:function(form, data, hasError){$(form).find("button[type=\'submit\']").prop("disabled", false); return !hasError;}',
                    ],
                    'htmlOptions' => [
                        'hideErrorMessage' => false,
                        'class' => 'order-form',
                    ]
                ]
            );
            ?>
            <table class="table">
                <thead>
                    <tr>
                        <th><?= Yii::t("CartModule.cart", "Product"); ?></th>
                        <th><?= Yii::t("CartModule.cart", "Amount"); ?></th>
                        <th class="text-center"><?= Yii::t("CartModule.cart", "Price"); ?></th>
                        <th class="text-center"><?= Yii::t("CartModule.cart", "Sum"); ?></th>
                        <th> </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($positions as $position): ?>
                        <tr>
                            <td class="col-sm-5">
                                <?php $positionId = $position->getId(); ?>
                                <?= CHtml::hiddenField('OrderProduct[' . $positionId . '][product_id]', $position->id); ?>
                                <input type="hidden" class="position-id" value="<?= $positionId; ?>"/>

                                <div class="media">
                                    <?php $productUrl = Yii::app()->createUrl('store/catalog/show', ['name' => $position->slug]); ?>
                                    <a class="img-thumbnail pull-left" href="<?= $productUrl; ?>">
                                        <img class="media-object" src="<?= $position->getProductModel()->getImageUrl(72, 72); ?>">
                                    </a>

                                    <div class="media-body">
                                        <h4 class="media-heading">
                                            <a href="<?= $productUrl; ?>"><?= $position->name; ?></a>
                                        </h4>
                                        <?php foreach ($position->selectedVariants as $variant): ?>
                                            <h6><?= $variant->attribute->title; ?>: <?= $variant->getOptionValue(); ?></h6>
                                            <?= CHtml::hiddenField('OrderProduct[' . $positionId . '][variant_ids][]', $variant->id); ?>
                                        <?php endforeach; ?>
                                        <span>
                                            <?= Yii::t("CartModule.cart", "Status"); ?>:
                                        </span>
                                        <span class="text-<?= $position->in_stock ? "success" : "warning"; ?>">
                                            <strong><?= $position->in_stock ? Yii::t("CartModule.cart", "In stock") : Yii::t("CartModule.cart", "Not in stock"); ?></strong>
                                        </span>
                                    </div>
                                </div>
                            </td>
                            <td class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button class="btn btn-default cart-quantity-decrease" type="button" data-target="#cart_<?= $positionId; ?>">-</button>
                                    </div>
                                    <?= CHtml::textField(
                                        'OrderProduct[' . $positionId . '][quantity]',
                                        $position->getQuantity(),
                                        ['id' => 'cart_' . $positionId, 'class' => 'form-control text-center position-count']
                                    ); ?>
                                    <div class="input-group-btn">
                                        <button class="btn btn-default cart-quantity-increase" type="button" data-target="#cart_<?= $positionId; ?>">+</button>
                                    </div>
                                </div>
                            </td>
                            <td class="col-sm-2 text-center">
                                <strong>
                                    <span class="position-price"><?= $position->getPrice(); ?></span>
                                    <?= Yii::t("CartModule.cart", "RUB"); ?>
                                </strong>
                            </td>
                            <td class="col-sm-2 text-center">
                                <strong>
                                    <span class="position-sum-price"><?= $position->getSumPrice(); ?></span>
                                    <?= Yii::t("CartModule.cart", "RUB"); ?>
                                </strong>
                            </td>
                            <td class="col-sm-1 text-right">
                                <button type="button" class="btn btn-danger cart-delete-product" data-position-id="<?= $positionId; ?>">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </button>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <tr>
                        <td>  </td>
                        <td colspan="2">
                            <h5><?= Yii::t("CartModule.cart", "Subtotal"); ?></h5>
                        </td>
                        <td colspan="2" style="text-align: right;">
                            <h4>
                                <strong id="cart-full-cost">
                                    <?= Yii::app()->cart->getCost(); ?>
                                </strong>
                                <?= Yii::t("CartModule.cart", "RUB"); ?>
                            </h4>
                        </td>
                    </tr>
                    <?php if (Yii::app()->hasModule('coupon')): ?>
                        <tr>
                            <td colspan="5" class="coupons">
                                <p>
                                    <b><?= Yii::t("CartModule.cart", "Coupons"); ?></b>
                                </p>
                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="input-group">
                                            <input id="coupon-code" type="text" class="form-control">
                                            <div class="input-group-btn">
                                                <button class="btn btn-default" type="button" id="add-coupon-code"><?= Yii::t("CartModule.cart", "Add coupon"); ?></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <?php foreach ($coupons as $coupon): ?>
                                            <span class="label alert alert-info coupon" title="<?= $coupon->name; ?>">
                                                <?= $coupon->code; ?>
                                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                <?= CHtml::hiddenField(
                                                    "Order[couponCodes][{$coupon->code}]",
                                                    $coupon->code,
                                                    [
                                                        'class' => 'coupon-input',
                                                        'data-code' => $coupon->code,
                                                        'data-name' => $coupon->name,
                                                        'data-value' => $coupon->value,
                                                        'data-type' => $coupon->type,
                                                        'data-min-order-price' => $coupon->min_order_price,
                                                        'data-free-shipping' => $coupon->free_shipping,
                                                    ]
                                                );?>
                                            </span>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php endif; ?>
                    <tr>
                        <td colspan="5">
                        <?php if(!empty($deliveryTypes)):?>
                            <fieldset>
                                <div class="form-group">
                                    <label class="control-label" for="radios">
                                        <b><?= Yii::t("CartModule.cart", "Delivery method"); ?></b>
                                    </label>
                                    <div class="controls">
                                        <?php foreach ($deliveryTypes as $key => $delivery): ?>
                                            <label class="radio" for="delivery-<?= $delivery->id; ?>">
                                                <input type="radio" name="Order[delivery_id]" id="delivery-<?= $delivery->id; ?>"
                                                       value="<?= $delivery->id; ?>"
                                                       data-price="<?= $delivery->price; ?>"
                                                       data-free-from="<?= $delivery->free_from; ?>"
                                                       data-available-from="<?= $delivery->available_from; ?>"
                                                       data-separate-payment="<?= $delivery->separate_payment; ?>">
                                                <?= $delivery->name; ?> - <?= $delivery->price; ?>
                                                <?= Yii::t("CartModule.cart", "RUB"); ?>(
                                                <?= Yii::t("CartModule.cart", "available from"); ?>
                                                <?= $delivery->available_from; ?>
                                                <?= Yii::t("CartModule.cart", "RUB"); ?>;
                                                <?= Yii::t("CartModule.cart", "free from"); ?> <?= $delivery->free_from; ?>
                                                <?= Yii::t("CartModule.cart", "RUB"); ?>; )
                                                <?=($delivery->separate_payment ? Yii::t("CartModule.cart", "Pay separately") : ""); ?>
                                            </label>
                                            <div class="text-muted">
                                                <?= $delivery->description; ?>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </fieldset>
                        <?php else:?>
                            <div class="alert alert-danger">
                                <?= Yii::t("CartModule.cart", "Delivery method aren't selected! The ordering is impossible!") ?>
                            </div>
                        <?php endif;?>   
                        </td>
                    </tr>
                    <tr>
                        <td>  </td>
                        <td colspan="2">
                            <h5>
                                <?= Yii::t("CartModule.cart", "Delivery price"); ?>
                            </h5>
                        </td>
                        <td colspan="2" style="text-align: right;">
                            <h4><strong id="cart-shipping-cost">0</strong> <?= Yii::t("CartModule.cart", "RUB"); ?></h4>
                        </td>
                    </tr>
                    <tr>
                        <td>  </td>
                        <td colspan="2"><h4><?= Yii::t("CartModule.cart", "Total"); ?></h4></td>
                        <td colspan="2" style="text-align: right;">
                            <h4><strong id="cart-full-cost-with-shipping">0</strong> <?= Yii::t("CartModule.cart", "RUB"); ?></h4>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <table class="col-sm-6 order-receiver">
                                <thead>
                                    <tr>
                                        <th>
                                            <?= Yii::t("CartModule.cart", "Address"); ?>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div>
                                                <?= $form->errorSummary($order); ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                <?= $form->labelEx($order, 'name'); ?>
                                                <?= $form->textField($order, 'name', ['class' => 'form-control']); ?>
                                                <?= $form->error($order, 'name'); ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                <?= $form->labelEx($order, 'phone'); ?>
                                                <?= $form->textField($order, 'phone', ['class' => 'form-control']); ?>
                                                <?= $form->error($order, 'phone'); ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                <?= $form->labelEx($order, 'email'); ?>
                                                <?= $form->emailField($order, 'email', ['class' => 'form-control']); ?>
                                                <?= $form->error($order, 'email'); ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                <?= $form->labelEx($order, 'address'); ?>
                                                <?= $form->textField($order, 'address', ['class' => 'form-control']); ?>
                                                <?= $form->error($order, 'address'); ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?= $form->labelEx($order, 'comment'); ?>
                                            <?= $form->textArea($order, 'comment', ['class' => 'form-control']); ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>

                    </tr>
                    <tr>
                        <td colspan="5" style="text-align: right;">
                            <a href="<?= Yii::app()->createUrl('store/catalog/index'); ?>" class="btn btn-default">
                                <span class="glyphicon glyphicon-shopping-cart"></span>
                                <?= Yii::t("CartModule.cart", "Back to catalog"); ?>
                            </a>
                            <button type="submit" class="btn btn-success">
                                <?= Yii::t("CartModule.cart", "Create order and proceed to payment"); ?>
                                <span class="glyphicon glyphicon-play"></span>
                            </button>
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php $this->endWidget(); ?>
        <?php endif; ?>
    </div>
</div>
