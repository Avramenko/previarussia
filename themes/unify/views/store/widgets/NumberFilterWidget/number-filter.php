<?php $filter = Yii::app()->getComponent('attributesFilter');?>
<div class="filter-block-checkbox-list">
    <div class="filter-block-header">
        <strong><?= $attribute->title ?></strong>
    </div>
    <div class="filter-block-body">
        <div class="row">
            <div class="col-xs-6">
                <?= CHtml::textField($filter->getFieldName($attribute, 'from'), $filter->getFieldValue($attribute, 'from'), ['class' => 'form-control']) ?>
            </div>
            <div class="col-xs-6">
                <?= CHtml::textField($filter->getFieldName($attribute, 'to'), $filter->getFieldValue($attribute, 'to'), ['class' => 'form-control']) ?>
            </div>

        </div>
        <?php
        // range slider
        $this->widget('zii.widgets.jui.CJuiSlider', array(
            'options'=>array(
                'min'=>0,
                'max'=>15000,
                'range'=>true,
                'values'=>array(500, 10000)
            ),
            'htmlOptions'=>array(
                'class'=>'ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all',
            ),
        ));
        ?>
    </div>
</div>
<hr/>
