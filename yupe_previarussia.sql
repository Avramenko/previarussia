-- MySQL dump 10.13  Distrib 5.5.44, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: yupe_previarussia
-- ------------------------------------------------------
-- Server version	5.5.44-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `yupe_blog_blog`
--

DROP TABLE IF EXISTS `yupe_blog_blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_blog_blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `icon` varchar(250) NOT NULL DEFAULT '',
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `member_status` int(11) NOT NULL DEFAULT '1',
  `post_status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_blog_blog_slug_lang` (`slug`,`lang`),
  KEY `ix_yupe_blog_blog_create_user` (`create_user_id`),
  KEY `ix_yupe_blog_blog_update_user` (`update_user_id`),
  KEY `ix_yupe_blog_blog_status` (`status`),
  KEY `ix_yupe_blog_blog_type` (`type`),
  KEY `ix_yupe_blog_blog_create_date` (`create_time`),
  KEY `ix_yupe_blog_blog_update_date` (`update_time`),
  KEY `ix_yupe_blog_blog_lang` (`lang`),
  KEY `ix_yupe_blog_blog_slug` (`slug`),
  KEY `ix_yupe_blog_blog_category_id` (`category_id`),
  CONSTRAINT `fk_yupe_blog_blog_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_blog_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_blog_update_user` FOREIGN KEY (`update_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_blog_blog`
--

LOCK TABLES `yupe_blog_blog` WRITE;
/*!40000 ALTER TABLE `yupe_blog_blog` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_blog_blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_blog_post`
--

DROP TABLE IF EXISTS `yupe_blog_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_blog_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_id` int(11) NOT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `publish_time` int(11) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `title` varchar(250) NOT NULL,
  `quote` text,
  `content` text NOT NULL,
  `link` varchar(250) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '0',
  `comment_status` int(11) NOT NULL DEFAULT '1',
  `create_user_ip` varchar(20) NOT NULL,
  `access_type` int(11) NOT NULL DEFAULT '1',
  `keywords` varchar(250) NOT NULL DEFAULT '',
  `description` varchar(250) NOT NULL DEFAULT '',
  `image` varchar(300) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_blog_post_lang_slug` (`slug`,`lang`),
  KEY `ix_yupe_blog_post_blog_id` (`blog_id`),
  KEY `ix_yupe_blog_post_create_user_id` (`create_user_id`),
  KEY `ix_yupe_blog_post_update_user_id` (`update_user_id`),
  KEY `ix_yupe_blog_post_status` (`status`),
  KEY `ix_yupe_blog_post_access_type` (`access_type`),
  KEY `ix_yupe_blog_post_comment_status` (`comment_status`),
  KEY `ix_yupe_blog_post_lang` (`lang`),
  KEY `ix_yupe_blog_post_slug` (`slug`),
  KEY `ix_yupe_blog_post_publish_date` (`publish_time`),
  KEY `ix_yupe_blog_post_category_id` (`category_id`),
  CONSTRAINT `fk_yupe_blog_post_blog` FOREIGN KEY (`blog_id`) REFERENCES `yupe_blog_blog` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_post_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_post_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_post_update_user` FOREIGN KEY (`update_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_blog_post`
--

LOCK TABLES `yupe_blog_post` WRITE;
/*!40000 ALTER TABLE `yupe_blog_post` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_blog_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_blog_post_to_tag`
--

DROP TABLE IF EXISTS `yupe_blog_post_to_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_blog_post_to_tag` (
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`post_id`,`tag_id`),
  KEY `ix_yupe_blog_post_to_tag_post_id` (`post_id`),
  KEY `ix_yupe_blog_post_to_tag_tag_id` (`tag_id`),
  CONSTRAINT `fk_yupe_blog_post_to_tag_post_id` FOREIGN KEY (`post_id`) REFERENCES `yupe_blog_post` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_post_to_tag_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `yupe_blog_tag` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_blog_post_to_tag`
--

LOCK TABLES `yupe_blog_post_to_tag` WRITE;
/*!40000 ALTER TABLE `yupe_blog_post_to_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_blog_post_to_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_blog_tag`
--

DROP TABLE IF EXISTS `yupe_blog_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_blog_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_blog_tag_tag_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_blog_tag`
--

LOCK TABLES `yupe_blog_tag` WRITE;
/*!40000 ALTER TABLE `yupe_blog_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_blog_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_blog_user_to_blog`
--

DROP TABLE IF EXISTS `yupe_blog_user_to_blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_blog_user_to_blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `role` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `note` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_blog_user_to_blog_blog_user_to_blog_u_b` (`user_id`,`blog_id`),
  KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_user_id` (`user_id`),
  KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_id` (`blog_id`),
  KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_status` (`status`),
  KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_role` (`role`),
  CONSTRAINT `fk_yupe_blog_user_to_blog_blog_user_to_blog_blog_id` FOREIGN KEY (`blog_id`) REFERENCES `yupe_blog_blog` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_user_to_blog_blog_user_to_blog_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_blog_user_to_blog`
--

LOCK TABLES `yupe_blog_user_to_blog` WRITE;
/*!40000 ALTER TABLE `yupe_blog_user_to_blog` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_blog_user_to_blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_catalog_good`
--

DROP TABLE IF EXISTS `yupe_catalog_good`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_catalog_good` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `price` decimal(19,3) NOT NULL DEFAULT '0.000',
  `article` varchar(100) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text NOT NULL,
  `slug` varchar(150) NOT NULL,
  `data` text,
  `is_special` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `change_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_catalog_good_alias` (`slug`),
  KEY `ix_yupe_catalog_good_status` (`status`),
  KEY `ix_yupe_catalog_good_category` (`category_id`),
  KEY `ix_yupe_catalog_good_user` (`user_id`),
  KEY `ix_yupe_catalog_good_change_user` (`change_user_id`),
  KEY `ix_yupe_catalog_good_article` (`article`),
  KEY `ix_yupe_catalog_good_price` (`price`),
  CONSTRAINT `fk_yupe_catalog_good_category` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_catalog_good_change_user` FOREIGN KEY (`change_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_catalog_good_user` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_catalog_good`
--

LOCK TABLES `yupe_catalog_good` WRITE;
/*!40000 ALTER TABLE `yupe_catalog_good` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_catalog_good` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_category_category`
--

DROP TABLE IF EXISTS `yupe_category_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_category_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_category_category_alias_lang` (`slug`,`lang`),
  KEY `ix_yupe_category_category_parent_id` (`parent_id`),
  KEY `ix_yupe_category_category_status` (`status`),
  CONSTRAINT `fk_yupe_category_category_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_category_category`
--

LOCK TABLES `yupe_category_category` WRITE;
/*!40000 ALTER TABLE `yupe_category_category` DISABLE KEYS */;
INSERT INTO `yupe_category_category` VALUES (1,NULL,'product-catalog','ru','Каталог товаров',NULL,'<p>Каталог товаров</p>','<p>Каталог товаров</p>',1);
/*!40000 ALTER TABLE `yupe_category_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_comment_comment`
--

DROP TABLE IF EXISTS `yupe_comment_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_comment_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `model` varchar(100) NOT NULL,
  `model_id` int(11) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `text` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(20) DEFAULT NULL,
  `level` int(11) DEFAULT '0',
  `root` int(11) DEFAULT '0',
  `lft` int(11) DEFAULT '0',
  `rgt` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_comment_comment_status` (`status`),
  KEY `ix_yupe_comment_comment_model_model_id` (`model`,`model_id`),
  KEY `ix_yupe_comment_comment_model` (`model`),
  KEY `ix_yupe_comment_comment_model_id` (`model_id`),
  KEY `ix_yupe_comment_comment_user_id` (`user_id`),
  KEY `ix_yupe_comment_comment_parent_id` (`parent_id`),
  KEY `ix_yupe_comment_comment_level` (`level`),
  KEY `ix_yupe_comment_comment_root` (`root`),
  KEY `ix_yupe_comment_comment_lft` (`lft`),
  KEY `ix_yupe_comment_comment_rgt` (`rgt`),
  CONSTRAINT `fk_yupe_comment_comment_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `yupe_comment_comment` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_comment_comment_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_comment_comment`
--

LOCK TABLES `yupe_comment_comment` WRITE;
/*!40000 ALTER TABLE `yupe_comment_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_comment_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_contentblock_content_block`
--

DROP TABLE IF EXISTS `yupe_contentblock_content_block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_contentblock_content_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `code` varchar(100) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `content` text NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_contentblock_content_block_code` (`code`),
  KEY `ix_yupe_contentblock_content_block_type` (`type`),
  KEY `ix_yupe_contentblock_content_block_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_contentblock_content_block`
--

LOCK TABLES `yupe_contentblock_content_block` WRITE;
/*!40000 ALTER TABLE `yupe_contentblock_content_block` DISABLE KEYS */;
INSERT INTO `yupe_contentblock_content_block` VALUES (1,'Баннер на главной','banner-na-glavnoy',3,'<div class=\"tp-banner-container\">\r\n<div class=\"tp-banner\">\r\n	<ul>\r\n		<!-- SLIDE -->\r\n		<li class=\"revolution-mch-1\" data-transition=\"fade\" data-slotamount=\"5\" data-masterspeed=\"1000\" data-title=\"Slide 1\">\r\n		<!-- MAIN IMAGE -->\r\n		<img src=\"/uploads/image/ac92b39e2d2dc9c554e79271133f6505.jpg\" alt=\"darkblurbg\" data-bgfit=\"cover\" data-bgposition=\"left top\" data-bgrepeat=\"no-repeat\">\r\n		<div class=\"tp-caption revolution-ch1 sft start\" data-x=\"center\" data-hoffset=\"0\" data-y=\"100\" data-speed=\"1500\" data-start=\"500\" data-easing=\"Back.easeInOut\" data-endeasing=\"Power1.easeIn\" data-endspeed=\"300\">\r\n			Introducing Unify Template\r\n		</div>\r\n		<!-- LAYER -->\r\n		<div class=\"tp-caption revolution-ch2 sft\" data-x=\"center\" data-hoffset=\"0\" data-y=\"190\" data-speed=\"1400\" data-start=\"2000\" data-easing=\"Power4.easeOut\" data-endspeed=\"300\" data-endeasing=\"Power1.easeIn\" data-captionhidden=\"off\" style=\"z-index: 6\">\r\n			We are creative technology company providing <br>\r\n			key digital services on web and mobile.\r\n		</div>\r\n		<!-- LAYER -->\r\n		<div class=\"tp-caption sft\" data-x=\"center\" data-hoffset=\"0\" data-y=\"310\" data-speed=\"1600\" data-start=\"2800\" data-easing=\"Power4.easeOut\" data-endspeed=\"300\" data-endeasing=\"Power1.easeIn\" data-captionhidden=\"off\" style=\"z-index: 6\">\r\n			<a href=\"#\" class=\"btn-u btn-brd btn-brd-hover btn-u-light\">Learn More</a>\r\n			<a href=\"#\" class=\"btn-u\">Our Work</a>\r\n		</div>\r\n		</li>\r\n		<!-- END SLIDE -->\r\n		<!-- SLIDE -->\r\n		<li class=\"revolution-mch-1\" data-transition=\"fade\" data-slotamount=\"5\" data-masterspeed=\"1000\" data-title=\"Slide 2\">\r\n		<!-- MAIN IMAGE -->\r\n		<img src=\"/uploads/image/87f6bbd668eeb0dcc698407982733b36.jpg\" alt=\"darkblurbg\" data-bgfit=\"cover\" data-bgposition=\"left top\" data-bgrepeat=\"no-repeat\">\r\n		<div class=\"tp-caption revolution-ch1 sft start\" data-x=\"center\" data-hoffset=\"0\" data-y=\"100\" data-speed=\"1500\" data-start=\"500\" data-easing=\"Back.easeInOut\" data-endeasing=\"Power1.easeIn\" data-endspeed=\"300\">\r\n			Includes 140+ Template Pages\r\n		</div>\r\n		<!-- LAYER -->\r\n		<div class=\"tp-caption revolution-ch2 sft\" data-x=\"center\" data-hoffset=\"0\" data-y=\"190\" data-speed=\"1400\" data-start=\"2000\" data-easing=\"Power4.easeOut\" data-endspeed=\"300\" data-endeasing=\"Power1.easeIn\" data-captionhidden=\"off\" style=\"z-index: 6\">\r\n			We are creative technology company providing <br>\r\n			key digital services on web and mobile.\r\n		</div>\r\n		<!-- LAYER -->\r\n		<div class=\"tp-caption sft\" data-x=\"center\" data-hoffset=\"0\" data-y=\"310\" data-speed=\"1600\" data-start=\"2800\" data-easing=\"Power4.easeOut\" data-endspeed=\"300\" data-endeasing=\"Power1.easeIn\" data-captionhidden=\"off\" style=\"z-index: 6\">\r\n			<a href=\"#\" class=\"btn-u btn-brd btn-brd-hover btn-u-light\">Learn More</a>\r\n			<a href=\"#\" class=\"btn-u btn-brd btn-brd-hover btn-u-light\">Our Work</a>\r\n		</div>\r\n		</li>\r\n		<!-- END SLIDE -->\r\n		<!-- SLIDE -->\r\n		<li class=\"revolution-mch-1\" data-transition=\"fade\" data-slotamount=\"5\" data-masterspeed=\"1000\" data-title=\"Slide 3\">\r\n		<!-- MAIN IMAGE -->\r\n		<img src=\"/uploads/image/92005cb04797c87cc6def0f355544b4d.jpg\" alt=\"darkblurbg\" data-bgfit=\"cover\" data-bgposition=\"left top\" data-bgrepeat=\"no-repeat\">\r\n		<div class=\"tp-caption revolution-ch1 sft start\" data-x=\"center\" data-hoffset=\"0\" data-y=\"100\" data-speed=\"1500\" data-start=\"500\" data-easing=\"Back.easeInOut\" data-endeasing=\"Power1.easeIn\" data-endspeed=\"300\">\r\n			Over 12000+ Satisfied Users\r\n		</div>\r\n		<!-- LAYER -->\r\n		<div class=\"tp-caption revolution-ch2 sft\" data-x=\"center\" data-hoffset=\"0\" data-y=\"190\" data-speed=\"1400\" data-start=\"2000\" data-easing=\"Power4.easeOut\" data-endspeed=\"300\" data-endeasing=\"Power1.easeIn\" data-captionhidden=\"off\" style=\"z-index: 6\">\r\n			We are creative technology company providing <br>\r\n			key digital services on web and mobile.\r\n		</div>\r\n		<!-- LAYER -->\r\n		<div class=\"tp-caption sft\" data-x=\"center\" data-hoffset=\"0\" data-y=\"310\" data-speed=\"1600\" data-start=\"2800\" data-easing=\"Power4.easeOut\" data-endspeed=\"300\" data-endeasing=\"Power1.easeIn\" data-captionhidden=\"off\" style=\"z-index: 6\">\r\n			<a href=\"#\" class=\"btn-u btn-brd btn-brd-hover btn-u-light\">Learn More</a>\r\n			<a href=\"#\" class=\"btn-u btn-brd btn-brd-hover btn-u-light\">Our Work</a>\r\n		</div>\r\n		</li>\r\n		<!-- END SLIDE -->\r\n		<!-- SLIDE -->\r\n		<li class=\"revolution-mch-1\" data-transition=\"fade\" data-slotamount=\"5\" data-masterspeed=\"1000\" data-title=\"Slide 3\">\r\n		<!-- MAIN IMAGE -->\r\n		<img src=\"/uploads/image/b38b8728555a86b5f6aaa2f155ece6ac.jpg\" alt=\"darkblurbg\" data-bgfit=\"cover\" data-bgposition=\"left top\" data-bgrepeat=\"no-repeat\">\r\n		<div class=\"tp-caption revolution-ch1 sft start\" data-x=\"center\" data-hoffset=\"0\" data-y=\"100\" data-speed=\"1500\" data-start=\"500\" data-easing=\"Back.easeInOut\" data-endeasing=\"Power1.easeIn\" data-endspeed=\"300\">\r\n			Over 12000+ Satisfied Users\r\n		</div>\r\n		<!-- LAYER -->\r\n		<div class=\"tp-caption revolution-ch2 sft\" data-x=\"center\" data-hoffset=\"0\" data-y=\"190\" data-speed=\"1400\" data-start=\"2000\" data-easing=\"Power4.easeOut\" data-endspeed=\"300\" data-endeasing=\"Power1.easeIn\" data-captionhidden=\"off\" style=\"z-index: 6\">\r\n			We are creative technology company providing <br>\r\n			key digital services on web and mobile.\r\n		</div>\r\n		<!-- LAYER -->\r\n		<div class=\"tp-caption sft\" data-x=\"center\" data-hoffset=\"0\" data-y=\"310\" data-speed=\"1600\" data-start=\"2800\" data-easing=\"Power4.easeOut\" data-endspeed=\"300\" data-endeasing=\"Power1.easeIn\" data-captionhidden=\"off\" style=\"z-index: 6\">\r\n			<a href=\"#\" class=\"btn-u btn-brd btn-brd-hover btn-u-light\">Learn More</a>\r\n			<a href=\"#\" class=\"btn-u btn-brd btn-brd-hover btn-u-light\">Our Work</a>\r\n		</div>\r\n		</li>\r\n		<!-- END SLIDE -->\r\n		<!-- SLIDE -->\r\n		<li class=\"revolution-mch-1\" data-transition=\"fade\" data-slotamount=\"5\" data-masterspeed=\"1000\" data-title=\"Slide 3\">\r\n		<!-- MAIN IMAGE -->\r\n		<img src=\"/uploads/image/fdccf0befd914978db751028058cadc1.jpg\" alt=\"darkblurbg\" data-bgfit=\"cover\" data-bgposition=\"left top\" data-bgrepeat=\"no-repeat\">\r\n		<div class=\"tp-caption revolution-ch1 sft start\" data-x=\"center\" data-hoffset=\"0\" data-y=\"100\" data-speed=\"1500\" data-start=\"500\" data-easing=\"Back.easeInOut\" data-endeasing=\"Power1.easeIn\" data-endspeed=\"300\">\r\n			Over 12000+ Satisfied Users\r\n		</div>\r\n		<!-- LAYER -->\r\n		<div class=\"tp-caption revolution-ch2 sft\" data-x=\"center\" data-hoffset=\"0\" data-y=\"190\" data-speed=\"1400\" data-start=\"2000\" data-easing=\"Power4.easeOut\" data-endspeed=\"300\" data-endeasing=\"Power1.easeIn\" data-captionhidden=\"off\" style=\"z-index: 6\">\r\n			We are creative technology company providing <br>\r\n			key digital services on web and mobile.\r\n		</div>\r\n		<!-- LAYER -->\r\n		<div class=\"tp-caption sft\" data-x=\"center\" data-hoffset=\"0\" data-y=\"310\" data-speed=\"1600\" data-start=\"2800\" data-easing=\"Power4.easeOut\" data-endspeed=\"300\" data-endeasing=\"Power1.easeIn\" data-captionhidden=\"off\" style=\"z-index: 6\">\r\n			<a href=\"#\" class=\"btn-u btn-brd btn-brd-hover btn-u-light\">Learn More</a>\r\n			<a href=\"#\" class=\"btn-u btn-brd btn-brd-hover btn-u-light\">Our Work</a>\r\n		</div>\r\n		</li>\r\n		<!-- END SLIDE -->\r\n	</ul>\r\n	<div class=\"tp-bannertimer tp-bottom\">\r\n	</div>\r\n</div>\r\n<!--=== End Slider ===--></div>','<p>Баннер на главной страничке</p>',NULL,1),(2,'Блок в подвале сайта','footer-block',3,'<!--=== Footer v2 ===--><div id=\"footer-v2\" class=\"footer-v2\">\r\n	<div class=\"footer\">\r\n		<div class=\"container\">\r\n			<div class=\"row\">\r\n				<!-- About -->\r\n				<div class=\"col-md-3 md-margin-bottom-40\">\r\n					<a href=\"index.html\"><img id=\"logo-footer\" class=\"footer-logo\" src=\"/assets/1315b4b9/images/previa-big.png\" alt=\"\"></a>\r\n					<p class=\"margin-bottom-20\">Unify is an incredibly beautiful responsive Bootstrap Template for corporate and creative professionals.\r\n					</p>\r\n					<form class=\"footer-subsribe\">\r\n						<div class=\"input-group\">\r\n							<input type=\"text\" class=\"form-control\" placeholder=\"Email Address\">\r\n							<span class=\"input-group-btn\">\r\n							<button class=\"btn-u\" type=\"button\">Go</button>\r\n							</span>\r\n						</div>\r\n					</form>\r\n				</div>\r\n				<!-- End About -->\r\n				<!-- Link List -->\r\n				<div class=\"col-md-3 md-margin-bottom-40\">\r\n					<div class=\"headline\">\r\n						<h2 class=\"heading-sm\">Быстрые ссылки</h2>\r\n					</div>\r\n					<ul class=\"list-unstyled link-list\">\r\n						<li><a href=\"#\">About us</a><i class=\"fa fa-angle-right\"></i></li>\r\n						<li><a href=\"#\">Portfolio</a><i class=\"fa fa-angle-right\"></i></li>\r\n						<li><a href=\"#\">Latest jobs</a><i class=\"fa fa-angle-right\"></i></li>\r\n						<li><a href=\"#\">Community</a><i class=\"fa fa-angle-right\"></i></li>\r\n						<li><a href=\"#\">Contact us</a><i class=\"fa fa-angle-right\"></i></li>\r\n					</ul>\r\n				</div>\r\n				<!-- End Link List -->\r\n				<!-- Latest Tweets -->\r\n				<div class=\"col-md-3 md-margin-bottom-40\">\r\n					<div class=\"latest-tweets\">\r\n						<div class=\"headline\">\r\n							<h2 class=\"heading-sm\">Хит продаж</h2>\r\n						</div>\r\n						<div class=\"latest-tweets-inner\">\r\n							<i class=\"fa fa-twitter\"></i>\r\n							<p>\r\n								<a href=\"#\">@htmlstream</a> \r\n                                    At vero seos etodela ccusamus et \r\n								<a href=\"#\">http://t.co/sBav7dm</a>\r\n								<small class=\"twitter-time\">2 hours ago</small>\r\n							</p>\r\n						</div>\r\n						<div class=\"latest-tweets-inner\">\r\n							<i class=\"fa fa-twitter\"></i>\r\n							<p>\r\n								<a href=\"#\">@htmlstream</a> \r\n                                    At vero seos etodela ccusamus et \r\n								<a href=\"#\">http://t.co/sBav7dm</a>\r\n								<small class=\"twitter-time\">4 hours ago</small>\r\n							</p>\r\n						</div>\r\n					</div>\r\n				</div>\r\n				<!-- End Latest Tweets -->\r\n				<!-- Address -->\r\n				<div class=\"col-md-3 md-margin-bottom-40\">\r\n					<div class=\"headline\">\r\n						<h2 class=\"heading-sm\">Обратная связь</h2>\r\n					</div>\r\n					<address class=\"md-margin-bottom-40\">\r\n					<i class=\"fa fa-home\"></i>25, Lorem Lis Street, California, US <br>\r\n					<i class=\"fa fa-phone\"></i>Phone: 800 123 3456 <br>\r\n					<i class=\"fa fa-globe\"></i>Website: <a href=\"#\">www.htmlstream.com</a> <br>\r\n					<i class=\"fa fa-envelope\"></i>Email: <a href=\"mailto:info@anybiz.com\">info@anybiz.com</a>\r\n					</address>\r\n					<!-- Social Links -->\r\n					<ul class=\"social-icons\">\r\n						<li><a href=\"#\" data-original-title=\"Facebook\" class=\"rounded-x social_facebook\"></a></li>\r\n						<li><a href=\"#\" data-original-title=\"Twitter\" class=\"rounded-x social_twitter\"></a></li>\r\n						<li><a href=\"#\" data-original-title=\"Goole Plus\" class=\"rounded-x social_googleplus\"></a></li>\r\n						<li><a href=\"#\" data-original-title=\"Linkedin\" class=\"rounded-x social_linkedin\"></a></li>\r\n					</ul>\r\n					<!-- End Social Links -->\r\n				</div>\r\n				<!-- End Address -->\r\n			</div>\r\n		</div>\r\n	</div>\r\n	<!--/footer-->\r\n	<div class=\"copyright\">\r\n		<div class=\"container\">\r\n			<p class=\"text-center\">2015 © All Rights Reserved. Unify Theme by <a target=\"_blank\" href=\"https://twitter.com/htmlstream\">Htmlstream</a>\r\n			</p>\r\n		</div>\r\n	</div>\r\n	<!--/copyright-->\r\n</div><!--=== End Footer v2 ===-->','<p>Футер сайта</p>',NULL,1),(3,'carusel','carusel',3,'<div class=\"asasasas\">\r\n</div>','<p><span id=\"selection-marker-1\" class=\"redactor-selection-marker\">Карусель</span></p>',NULL,1);
/*!40000 ALTER TABLE `yupe_contentblock_content_block` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_dictionary_dictionary_data`
--

DROP TABLE IF EXISTS `yupe_dictionary_dictionary_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_dictionary_dictionary_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `code` varchar(100) NOT NULL,
  `name` varchar(250) NOT NULL,
  `value` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL DEFAULT '',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_dictionary_dictionary_data_code_unique` (`code`),
  KEY `ix_yupe_dictionary_dictionary_data_group_id` (`group_id`),
  KEY `ix_yupe_dictionary_dictionary_data_create_user_id` (`create_user_id`),
  KEY `ix_yupe_dictionary_dictionary_data_update_user_id` (`update_user_id`),
  KEY `ix_yupe_dictionary_dictionary_data_status` (`status`),
  CONSTRAINT `fk_yupe_dictionary_dictionary_data_create_user_id` FOREIGN KEY (`create_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_dictionary_dictionary_data_data_group_id` FOREIGN KEY (`group_id`) REFERENCES `yupe_dictionary_dictionary_group` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_dictionary_dictionary_data_update_user_id` FOREIGN KEY (`update_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_dictionary_dictionary_data`
--

LOCK TABLES `yupe_dictionary_dictionary_data` WRITE;
/*!40000 ALTER TABLE `yupe_dictionary_dictionary_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_dictionary_dictionary_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_dictionary_dictionary_group`
--

DROP TABLE IF EXISTS `yupe_dictionary_dictionary_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_dictionary_dictionary_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) NOT NULL,
  `name` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL DEFAULT '',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_dictionary_dictionary_group_code` (`code`),
  KEY `ix_yupe_dictionary_dictionary_group_create_user_id` (`create_user_id`),
  KEY `ix_yupe_dictionary_dictionary_group_update_user_id` (`update_user_id`),
  CONSTRAINT `fk_yupe_dictionary_dictionary_group_create_user_id` FOREIGN KEY (`create_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_dictionary_dictionary_group_update_user_id` FOREIGN KEY (`update_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_dictionary_dictionary_group`
--

LOCK TABLES `yupe_dictionary_dictionary_group` WRITE;
/*!40000 ALTER TABLE `yupe_dictionary_dictionary_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_dictionary_dictionary_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_feedback_feedback`
--

DROP TABLE IF EXISTS `yupe_feedback_feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_feedback_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `answer_user` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `phone` varchar(150) DEFAULT NULL,
  `theme` varchar(250) NOT NULL,
  `text` text NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `answer` text NOT NULL,
  `answer_time` datetime DEFAULT NULL,
  `is_faq` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_yupe_feedback_feedback_category` (`category_id`),
  KEY `ix_yupe_feedback_feedback_type` (`type`),
  KEY `ix_yupe_feedback_feedback_status` (`status`),
  KEY `ix_yupe_feedback_feedback_isfaq` (`is_faq`),
  KEY `ix_yupe_feedback_feedback_answer_user` (`answer_user`),
  CONSTRAINT `fk_yupe_feedback_feedback_answer_user` FOREIGN KEY (`answer_user`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_feedback_feedback_category` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_feedback_feedback`
--

LOCK TABLES `yupe_feedback_feedback` WRITE;
/*!40000 ALTER TABLE `yupe_feedback_feedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_feedback_feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_gallery_gallery`
--

DROP TABLE IF EXISTS `yupe_gallery_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_gallery_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `description` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `owner` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_yupe_gallery_gallery_status` (`status`),
  KEY `ix_yupe_gallery_gallery_owner` (`owner`),
  CONSTRAINT `fk_yupe_gallery_gallery_owner` FOREIGN KEY (`owner`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_gallery_gallery`
--

LOCK TABLES `yupe_gallery_gallery` WRITE;
/*!40000 ALTER TABLE `yupe_gallery_gallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_gallery_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_gallery_image_to_gallery`
--

DROP TABLE IF EXISTS `yupe_gallery_image_to_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_gallery_image_to_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_id` int(11) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_gallery_image_to_gallery_gallery_to_image` (`image_id`,`gallery_id`),
  KEY `ix_yupe_gallery_image_to_gallery_gallery_to_image_image` (`image_id`),
  KEY `ix_yupe_gallery_image_to_gallery_gallery_to_image_gallery` (`gallery_id`),
  CONSTRAINT `fk_yupe_gallery_image_to_gallery_gallery_to_image_gallery` FOREIGN KEY (`gallery_id`) REFERENCES `yupe_gallery_gallery` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_gallery_image_to_gallery_gallery_to_image_image` FOREIGN KEY (`image_id`) REFERENCES `yupe_image_image` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_gallery_image_to_gallery`
--

LOCK TABLES `yupe_gallery_image_to_gallery` WRITE;
/*!40000 ALTER TABLE `yupe_gallery_image_to_gallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_gallery_image_to_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_image_image`
--

DROP TABLE IF EXISTS `yupe_image_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_image_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `file` varchar(250) NOT NULL,
  `create_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `alt` varchar(250) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_image_image_status` (`status`),
  KEY `ix_yupe_image_image_user` (`user_id`),
  KEY `ix_yupe_image_image_type` (`type`),
  KEY `ix_yupe_image_image_category_id` (`category_id`),
  KEY `fk_yupe_image_image_parent_id` (`parent_id`),
  CONSTRAINT `fk_yupe_image_image_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_image_image_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `yupe_image_image` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_image_image_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_image_image`
--

LOCK TABLES `yupe_image_image` WRITE;
/*!40000 ALTER TABLE `yupe_image_image` DISABLE KEYS */;
INSERT INTO `yupe_image_image` VALUES (1,NULL,NULL,'smoothing-previa.jpg','','913d03e7214179e383fcc358a2872356.jpg','2015-08-07 14:31:32',2,'smoothing-previa.jpg',0,1,1),(2,NULL,NULL,'reconstruct-previa.jpg','','1b9fd5f1f7393a02aecdd185ebdfb8d0.jpg','2015-08-07 15:22:00',2,'reconstruct-previa.jpg',0,1,2),(3,NULL,NULL,'curlfriends-previa.jpg','','80520ace374e49301991a553a7ffd956.jpg','2015-08-07 15:48:52',2,'curlfriends-previa.jpg',0,1,3),(4,NULL,NULL,'10250071_666926130009525_1559146887_n.jpg','','873618244eb6eaf3f23a8dab9f215e51.jpg','2015-08-16 01:13:58',1,'10250071_666926130009525_1559146887_n.jpg',0,1,4),(5,NULL,NULL,'10338327_680797971955674_5766897655043880432_n.jpg','','0e844146f8e1866e459e3f80072b2be6.jpg','2015-08-16 01:14:06',1,'10338327_680797971955674_5766897655043880432_n.jpg',0,1,5),(6,NULL,NULL,'10356693_750215955013875_333958975425081758_n.jpg','','5fadffeb53d0a4eb6f35883aa2bc3701.jpg','2015-08-16 01:14:15',1,'10356693_750215955013875_333958975425081758_n.jpg',0,1,6),(7,NULL,NULL,'10357264_684677614901043_1035464071201835232_n.jpg','','cfc430afb84f8ec8c1ae8ef99a0094a9.jpg','2015-08-16 01:14:20',1,'10357264_684677614901043_1035464071201835232_n.jpg',0,1,7),(8,NULL,NULL,'10419999_709335672435237_7075535187815579283_n.jpg','','1236380980b41507158f65be0ecd9137.jpg','2015-08-16 01:14:26',1,'10419999_709335672435237_7075535187815579283_n.jpg',0,1,8),(9,NULL,NULL,'10922610_809320892436714_8875287397085773219_n.jpg','','f8e9989dbb838cc855b718905d9196e4.jpg','2015-08-16 01:14:47',1,'10922610_809320892436714_8875287397085773219_n.jpg',0,1,9),(10,NULL,NULL,'11196315_859689174066552_6193897890749072104_n.jpg','','5e9731bdd17d7d9804e2edc93c65222b.jpg','2015-08-16 01:14:58',1,'11196315_859689174066552_6193897890749072104_n.jpg',0,1,10),(11,NULL,NULL,'10250071_666926130009525_1559146887_n.jpg','','d3e9ec1be01b995f3c5ef7e5e08f9617.jpg','2015-09-18 05:12:18',1,'10250071_666926130009525_1559146887_n.jpg',0,1,11),(12,NULL,NULL,'10338327_680797971955674_5766897655043880432_n.jpg','','5c5abc9d0be42a69fb1ad758ca8d5ead.jpg','2015-09-18 05:13:02',1,'10338327_680797971955674_5766897655043880432_n.jpg',0,1,12),(13,NULL,NULL,'10356693_750215955013875_333958975425081758_n.jpg','','fdccf0befd914978db751028058cadc1.jpg','2015-09-18 05:13:08',1,'10356693_750215955013875_333958975425081758_n.jpg',0,1,13),(14,NULL,NULL,'10357264_684677614901043_1035464071201835232_n.jpg','','b501b79550337c5d0e3c872382aa120a.jpg','2015-09-18 05:13:12',1,'10357264_684677614901043_1035464071201835232_n.jpg',0,1,14),(15,NULL,NULL,'10419999_709335672435237_7075535187815579283_n.jpg','','92005cb04797c87cc6def0f355544b4d.jpg','2015-09-18 05:13:18',1,'10419999_709335672435237_7075535187815579283_n.jpg',0,1,15),(16,NULL,NULL,'10639601_753800944655376_1167245397883325588_n.jpg','','b38b8728555a86b5f6aaa2f155ece6ac.jpg','2015-09-18 05:13:24',1,'10639601_753800944655376_1167245397883325588_n.jpg',0,1,16),(17,NULL,NULL,'10922610_809320892436714_8875287397085773219_n.jpg','','ac92b39e2d2dc9c554e79271133f6505.jpg','2015-09-18 05:13:27',1,'10922610_809320892436714_8875287397085773219_n.jpg',0,1,17),(18,NULL,NULL,'11196315_859689174066552_6193897890749072104_n.jpg','','87f6bbd668eeb0dcc698407982733b36.jpg','2015-09-18 05:13:31',1,'11196315_859689174066552_6193897890749072104_n.jpg',0,1,18),(19,NULL,NULL,'RECONSTRUCT_system.jpg','','58815643d69eade7209bbfa28176d02c.jpg','2015-09-18 05:43:16',1,'RECONSTRUCT_system.jpg',0,1,19),(20,NULL,NULL,'CURLFRIENDS_family.jpg','','12f4b7534bbf04a53d38054769cc959e.jpg','2015-09-18 05:47:51',1,'CURLFRIENDS_family.jpg',0,1,20),(21,NULL,NULL,'SMOOTHING_system.jpg','','bd8e848534f88cdc2798fdd66132ca11.jpg','2015-09-18 05:50:00',1,'SMOOTHING_system.jpg',0,1,21),(22,NULL,NULL,'4 - Окрашивание.jpg','','6f30a5ac9e6b55623b95325be1d5f993.jpg','2015-09-18 05:57:33',1,'4 - Окрашивание.jpg',0,1,22),(23,NULL,NULL,'5 - Осветление.jpg','','5b665513f5124d303bcb832fca24683c.jpg','2015-09-18 05:58:48',1,'5 - Осветление.jpg',0,1,23),(24,NULL,NULL,'6 очищение.jpg','','ce57e2fed0fe2d340d1fca83d22cbe64.jpg','2015-09-18 05:59:28',1,'6 очищение.jpg',0,1,24),(25,NULL,NULL,'7 - объем.jpg','','b87fbeba993547cc77bdc933a7ef8fc0.jpg','2015-09-18 06:00:31',1,'7 - объем.jpg',0,1,25),(26,NULL,NULL,'8 - стайлинг.jpg','','bbc67b7de8a756629db2203b06f3e9d5.jpg','2015-09-18 06:01:16',1,'8 - стайлинг.jpg',0,1,26);
/*!40000 ALTER TABLE `yupe_image_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_mail_mail_event`
--

DROP TABLE IF EXISTS `yupe_mail_mail_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_mail_mail_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(150) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_mail_mail_event_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_mail_mail_event`
--

LOCK TABLES `yupe_mail_mail_event` WRITE;
/*!40000 ALTER TABLE `yupe_mail_mail_event` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_mail_mail_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_mail_mail_template`
--

DROP TABLE IF EXISTS `yupe_mail_mail_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_mail_mail_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(150) NOT NULL,
  `event_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text,
  `from` varchar(150) NOT NULL,
  `to` varchar(150) NOT NULL,
  `theme` text NOT NULL,
  `body` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_mail_mail_template_code` (`code`),
  KEY `ix_yupe_mail_mail_template_status` (`status`),
  KEY `ix_yupe_mail_mail_template_event_id` (`event_id`),
  CONSTRAINT `fk_yupe_mail_mail_template_event_id` FOREIGN KEY (`event_id`) REFERENCES `yupe_mail_mail_event` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_mail_mail_template`
--

LOCK TABLES `yupe_mail_mail_template` WRITE;
/*!40000 ALTER TABLE `yupe_mail_mail_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_mail_mail_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_menu_menu`
--

DROP TABLE IF EXISTS `yupe_menu_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_menu_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_menu_menu_code` (`code`),
  KEY `ix_yupe_menu_menu_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_menu_menu`
--

LOCK TABLES `yupe_menu_menu` WRITE;
/*!40000 ALTER TABLE `yupe_menu_menu` DISABLE KEYS */;
INSERT INTO `yupe_menu_menu` VALUES (1,'Верхнее меню','top-menu','Основное меню сайта, расположенное сверху в блоке mainmenu.',1);
/*!40000 ALTER TABLE `yupe_menu_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_menu_menu_item`
--

DROP TABLE IF EXISTS `yupe_menu_menu_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_menu_menu_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `regular_link` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(150) NOT NULL,
  `href` varchar(150) NOT NULL,
  `class` varchar(150) NOT NULL,
  `title_attr` varchar(150) NOT NULL,
  `before_link` varchar(150) NOT NULL,
  `after_link` varchar(150) NOT NULL,
  `target` varchar(150) NOT NULL,
  `rel` varchar(150) NOT NULL,
  `condition_name` varchar(150) DEFAULT '0',
  `condition_denial` int(11) DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_menu_menu_item_menu_id` (`menu_id`),
  KEY `ix_yupe_menu_menu_item_sort` (`sort`),
  KEY `ix_yupe_menu_menu_item_status` (`status`),
  CONSTRAINT `fk_yupe_menu_menu_item_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `yupe_menu_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_menu_menu_item`
--

LOCK TABLES `yupe_menu_menu_item` WRITE;
/*!40000 ALTER TABLE `yupe_menu_menu_item` DISABLE KEYS */;
INSERT INTO `yupe_menu_menu_item` VALUES (1,0,1,1,'Главная','/pages/index','','Главная страница сайта','','','','','',0,1,1),(2,0,1,0,'Блоги','/blog/blog/index','','Блоги','','','','','',0,2,0),(3,0,1,0,'Пользователи','/user/people/index','','Пользователи','','','','','',0,3,0),(4,0,1,0,'Wiki','/wiki/default/index','','Wiki','','','','','',0,7,0),(5,0,1,0,'Войти','/user/account/login','login-text','Войти на сайт','','','','','isAuthenticated',1,10,0),(6,0,1,0,'Выйти','/user/account/logout','login-text','Выйти','','','','','isAuthenticated',0,11,0),(7,0,1,0,'Регистрация','/user/account/registration','login-text','Регистрация на сайте','','','','','isAuthenticated',1,9,0),(8,0,1,0,'Панель управления','/yupe/backend/index','login-text','Панель управления сайтом','','','','','isSuperUser',0,12,0),(9,0,1,0,'FAQ','/feedback/contact/faq','','FAQ','','','','','',0,7,0),(10,0,1,1,'Контакты','#','','Контакты','','','','','',0,15,1),(11,0,1,0,'Магазин','/store/catalog/index','','Магазин','','','','','',0,1,0),(12,0,1,0,'Поддерживающий уход','/store/products','','','','','','','',0,13,1),(13,0,1,1,'Профессиональные средства','/store/professional','','','','','','','',0,14,1),(14,12,1,0,'Четкие и упругие локоны','/store/products/defined-curls','','','','','','','',0,16,1),(15,12,1,0,'Идеальное и стойкое выпрямление','/store/products/perfect-straightness','','','','','','','',0,17,1),(16,12,1,1,'Стойкий и блестящий цвет','/store/products/long-lasting-shiny-colour','','','','','','','',0,18,1),(17,12,1,1,'Платиновый блонд и седые волосы','/store/products/for-platinum-blonde-and-grey-hair','','','','','','','',0,19,1),(18,12,1,1,'Очищение волос и кожи головы','/store/products/well-being-of-hair-and-scalp','','','','','','','',0,20,1),(19,12,1,1,'Поврежденные и ломкие волосы','/store/products/damaged-and-brittle-hair','','','','','','','',0,21,1),(20,12,1,1,'Тонкие и слабые волосы','/store/products/fine-and-thin-hair','','','','','','','',0,22,1),(21,12,1,1,'Создание собственного стиля','/store/products/create-your-style','','','','','','','',0,23,1),(22,13,1,1,'Завивка','/store/professional/curlfriends','','','','','','','',0,25,1),(23,13,1,1,'Выпрямление','/store/professional/smoothing','','','','','','','',0,26,1),(24,13,1,1,'Окрашивание','/store/professional/keeping','','','','','','','',0,27,1),(25,13,1,1,'Осветление','/store/professional/lightening','','','','','','','',0,28,1),(26,13,1,1,'Очищение','/store/professional/extralife','','','','','','','',0,29,1),(27,13,1,1,'Реконструкция','/store/professional/reconstruct','','','','','','','',0,24,1),(28,13,1,1,'Объем','/store/professional/volumizing','','','','','','','',0,30,1),(29,13,1,1,'Стайлинг','/store/professional/finish','','','','','','','',0,31,1),(30,10,1,1,'Напишите нам','/contacts','','','','','','','',0,32,1),(31,10,1,1,'Стать дистрибьютором','/become-a-distributor','','','','','','','',0,33,1);
/*!40000 ALTER TABLE `yupe_menu_menu_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_migrations`
--

DROP TABLE IF EXISTS `yupe_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_migrations_module` (`module`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_migrations`
--

LOCK TABLES `yupe_migrations` WRITE;
/*!40000 ALTER TABLE `yupe_migrations` DISABLE KEYS */;
INSERT INTO `yupe_migrations` VALUES (1,'user','m000000_000000_user_base',1436399071),(2,'user','m131019_212911_user_tokens',1436399071),(3,'user','m131025_152911_clean_user_table',1436399072),(4,'user','m131026_002234_prepare_hash_user_password',1436399072),(5,'user','m131106_111552_user_restore_fields',1436399072),(6,'user','m131121_190850_modify_tokes_table',1436399073),(7,'user','m140812_100348_add_expire_to_token_table',1436399073),(8,'user','m150416_113652_rename_fields',1436399073),(9,'yupe','m000000_000000_yupe_base',1436399074),(10,'yupe','m130527_154455_yupe_change_unique_index',1436399075),(11,'yupe','m150416_125517_rename_fields',1436399075),(12,'category','m000000_000000_category_base',1436399075),(13,'category','m150415_150436_rename_fields',1436399075),(14,'image','m000000_000000_image_base',1436399076),(15,'image','m150226_121100_image_order',1436399076),(16,'image','m150416_080008_rename_fields',1436399077),(17,'comment','m000000_000000_comment_base',1436399077),(18,'comment','m130704_095200_comment_nestedsets',1436399078),(19,'comment','m150415_151804_rename_fields',1436399078),(20,'store','m140812_160000_store_attribute_group_base',1436399078),(21,'store','m140812_170000_store_attribute_base',1436399078),(22,'store','m140812_180000_store_attribute_option_base',1436399079),(23,'store','m140813_200000_store_category_base',1436399079),(24,'store','m140813_210000_store_type_base',1436399079),(25,'store','m140813_220000_store_type_attribute_base',1436399079),(26,'store','m140813_230000_store_producer_base',1436399079),(27,'store','m140814_000000_store_product_base',1436399082),(28,'store','m140814_000010_store_product_category_base',1436399083),(29,'store','m140814_000013_store_product_attribute_eav_base',1436399083),(30,'store','m140814_000018_store_product_image_base',1436399083),(31,'store','m140814_000020_store_product_variant_base',1436399083),(32,'store','m141014_210000_store_product_category_column',1436399083),(33,'store','m141015_170000_store_product_image_column',1436399084),(34,'store','m141218_091834_default_null',1436399084),(35,'store','m150210_063409_add_store_menu_item',1436399084),(36,'store','m150210_105811_add_price_column',1436399084),(37,'store','m150210_131238_order_category',1436399084),(38,'store','m150211_105453_add_position_for_product_variant',1436399084),(39,'store','m150226_065935_add_product_position',1436399084),(40,'store','m150416_112008_rename_fields',1436399084),(41,'store','m150417_180000_store_product_link_base',1436399084),(42,'mail','m000000_000000_mail_base',1436399086),(43,'payment','m140815_170000_store_payment_base',1436399087),(44,'blog','m000000_000000_blog_base',1436399092),(45,'blog','m130503_091124_BlogPostImage',1436399092),(46,'blog','m130529_151602_add_post_category',1436399092),(47,'blog','m140226_052326_add_community_fields',1436399092),(48,'blog','m140714_110238_blog_post_quote_type',1436399092),(49,'blog','m150406_094809_blog_post_quote_type',1436399092),(50,'blog','m150414_180119_rename_date_fields',1436399092),(51,'delivery','m140815_190000_store_delivery_base',1436399093),(52,'delivery','m140815_200000_store_delivery_payment_base',1436399093),(53,'order','m140814_200000_store_order_base',1436399094),(54,'order','m150324_105949_order_status_table',1436399094),(55,'order','m150416_100212_rename_fields',1436399094),(56,'order','m150514_065554_change_order_price',1436399094),(57,'page','m000000_000000_page_base',1436399096),(58,'page','m130115_155600_columns_rename',1436399096),(59,'page','m140115_083618_add_layout',1436399096),(60,'page','m140620_072543_add_view',1436399096),(61,'page','m150312_151049_change_body_type',1436399096),(62,'page','m150416_101038_rename_fields',1436399096),(63,'contentblock','m000000_000000_contentblock_base',1436399099),(64,'contentblock','m140715_130737_add_category_id',1436399099),(65,'contentblock','m150127_130425_add_status_column',1436399099),(66,'rbac','m140115_131455_auth_item',1436399101),(67,'rbac','m140115_132045_auth_item_child',1436399101),(68,'rbac','m140115_132319_auth_item_assign',1436399101),(69,'rbac','m140702_230000_initial_role_data',1436399101),(70,'yandexmarket','m141110_090000_yandex_market_export_base',1436399103),(71,'sitemap','m141004_130000_sitemap_page',1436399104),(72,'sitemap','m141004_140000_sitemap_page_data',1436399105),(73,'news','m000000_000000_news_base',1436399107),(74,'news','m150416_081251_rename_fields',1436399107),(75,'social','m000000_000000_social_profile',1436399108),(76,'coupon','m140816_200000_store_coupon_base',1436399109),(77,'coupon','m150414_124659_add_order_coupon_table',1436399109),(78,'coupon','m150415_153218_rename_fields',1436399109),(79,'menu','m000000_000000_menu_base',1436399113),(80,'menu','m121220_001126_menu_test_data',1436399113),(81,'gallery','m000000_000000_gallery_base',1436399114),(82,'gallery','m130427_120500_gallery_creation_user',1436399114),(83,'gallery','m150416_074146_rename_fields',1436399114),(84,'catalog','m000000_000000_good_base',1436399115),(85,'catalog','m150415_121606_rename_fields',1436399116),(86,'notify','m141031_091039_add_notify_table',1436399118),(87,'queue','m000000_000000_queue_base',1436399118),(88,'queue','m131007_031000_queue_fix_index',1436399119),(89,'dictionary','m000000_000000_dictionary_base',1436399119),(90,'dictionary','m150415_183050_rename_fields',1436399120),(91,'feedback','m000000_000000_feedback_base',1436399122),(92,'feedback','m150415_184108_rename_fields',1436399122);
/*!40000 ALTER TABLE `yupe_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_news_news`
--

DROP TABLE IF EXISTS `yupe_news_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_news_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `lang` char(2) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `date` date NOT NULL,
  `title` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `short_text` text,
  `full_text` text NOT NULL,
  `image` varchar(300) DEFAULT NULL,
  `link` varchar(300) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `keywords` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_news_news_alias_lang` (`slug`,`lang`),
  KEY `ix_yupe_news_news_status` (`status`),
  KEY `ix_yupe_news_news_user_id` (`user_id`),
  KEY `ix_yupe_news_news_category_id` (`category_id`),
  KEY `ix_yupe_news_news_date` (`date`),
  CONSTRAINT `fk_yupe_news_news_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_news_news_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_news_news`
--

LOCK TABLES `yupe_news_news` WRITE;
/*!40000 ALTER TABLE `yupe_news_news` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_news_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_notify_settings`
--

DROP TABLE IF EXISTS `yupe_notify_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_notify_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `my_post` tinyint(1) NOT NULL DEFAULT '1',
  `my_comment` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_notify_settings_user_id` (`user_id`),
  CONSTRAINT `fk_yupe_notify_settings_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_notify_settings`
--

LOCK TABLES `yupe_notify_settings` WRITE;
/*!40000 ALTER TABLE `yupe_notify_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_notify_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_page_page`
--

DROP TABLE IF EXISTS `yupe_page_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_page_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `lang` char(2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `change_user_id` int(11) DEFAULT NULL,
  `title_short` varchar(150) NOT NULL,
  `title` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `body` mediumtext NOT NULL,
  `keywords` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  `status` int(11) NOT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `layout` varchar(250) DEFAULT NULL,
  `view` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_page_page_slug_lang` (`slug`,`lang`),
  KEY `ix_yupe_page_page_status` (`status`),
  KEY `ix_yupe_page_page_is_protected` (`is_protected`),
  KEY `ix_yupe_page_page_user_id` (`user_id`),
  KEY `ix_yupe_page_page_change_user_id` (`change_user_id`),
  KEY `ix_yupe_page_page_menu_order` (`order`),
  KEY `ix_yupe_page_page_category_id` (`category_id`),
  CONSTRAINT `fk_yupe_page_page_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_page_page_change_user_id` FOREIGN KEY (`change_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_page_page_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_page_page`
--

LOCK TABLES `yupe_page_page` WRITE;
/*!40000 ALTER TABLE `yupe_page_page` DISABLE KEYS */;
INSERT INTO `yupe_page_page` VALUES (1,NULL,'ru',NULL,'2015-07-09 05:10:18','2015-07-09 05:17:39',1,1,'Доставка','Доставка','shipping-and-delivery','<h2>Доставка\r\n</h2>\r\n <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ornare magna neque, quis sodales ex luctus at. In feugiat finibus sem. Ut nisi est, cursus vitae risus non, sagittis auctor nisl. Aenean sit amet luctus ante. Phasellus pellentesque tempor libero nec rhoncus. Nam sed diam eget ante rhoncus volutpat vitae ac dui. Vestibulum massa velit, pretium pellentesque vestibulum et, pharetra at magna. Praesent sit amet arcu malesuada, malesuada dui et, fringilla neque. Integer sapien elit, gravida in malesuada consectetur, sagittis ac erat. Praesent vel vestibulum eros, ac interdum diam. Cras vitae massa neque. Duis dapibus auctor dui, eu pulvinar odio efficitur non. Sed quis nibh in risus gravida hendrerit.</p>\r\n\r\n        <p>In volutpat tristique rutrum. Vestibulum sollicitudin at nulla sit amet pulvinar. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam dictum at diam eu commodo. Mauris vehicula sapien lectus, ac molestie mauris commodo sit amet. Aliquam non est leo. Mauris sagittis sagittis urna, in lacinia lectus efficitur in. Proin sollicitudin mattis sodales. Cras et faucibus neque, non ornare metus. Vestibulum id semper est. Maecenas vestibulum varius sapien, id elementum tellus euismod at. Vestibulum auctor ante a efficitur sagittis. Proin tempus lacus risus, quis dapibus nunc eleifend sit amet.</p>\r\n\r\n        <p>Aliquam quis varius nisl. Praesent egestas magna efficitur enim posuere rutrum. Vivamus nulla velit, tempus id tempus nec, lacinia et leo. Nullam fringilla commodo facilisis. Maecenas mi eros, feugiat eget arcu sit amet, posuere efficitur ex. Nulla vel felis facilisis turpis porta euismod. Aenean vel enim nec nisl varius feugiat vitae nec urna. Duis feugiat molestie ex, non tincidunt tortor finibus in. Pellentesque quis ultricies augue. Nulla et lacus semper quam ornare cursus sit amet a sem. Vivamus semper lectus dui, faucibus dignissim enim feugiat vitae. Morbi fermentum odio neque, a molestie nunc volutpat ac. In ut aliquam est. Pellentesque est elit, pulvinar a magna sit amet, vestibulum fermentum neque. Aliquam vel lorem ut tortor tristique pretium eget vitae dolor.</p>\r\n\r\n        <p>Vestibulum dictum nec libero ut euismod. Fusce posuere sit amet libero vitae ornare. Curabitur posuere semper ipsum in efficitur. Etiam pellentesque felis sodales, blandit eros vel, dignissim turpis. Phasellus ullamcorper rhoncus enim, a consequat libero sagittis at. Mauris in pharetra elit. Suspendisse in urna dictum, tincidunt odio eget, vehicula turpis. Sed eget quam consequat, aliquam magna non, aliquam ipsum.</p>','Доставка','Доставка',1,0,0,'column2',''),(2,NULL,'ru',NULL,'2015-07-09 05:11:58','2015-07-09 11:12:50',1,2,'Политика конфиденциальности','Политика конфиденциальности','privacy-policy','<p class=\"lead\"><em>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident.</em></p><p>Et harum quidem rerum facilis est et expedita distinctio lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero consectetur adipiscing elit magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. Pellentesque viverra vehicula sem ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.</p><p><br></p>\r\n<h4>Lorem ipsum dolor integer sed arcu</h4><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. Pellentesque viverra vehicula sem ut volutpat. Integer sed arcu. At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non <a href=\"#\">libero consectetur adipiscing</a> elit magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. Pellentesque viverra vehicula sem ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.</p><p>Iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non sit amet, consectetur adipiscing elit. Ut adipiscing elit magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. Pellentesque viverra vehicula sem ut volutpat. Lorem ipsum dolor sit amet. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat <a href=\"#\">consectetur adipiscing elit</a>.</p><p><br></p>\r\n<h4>Molestias excepturi sint</h4><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. Pellentesque viverra lacus. Fusce condimentum eleifend enim a feugiat. Pellentesque viverra vehicula sem ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.</p><p><strong><em>Praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non ipsum dolor sit amet. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat <a href=\"#\">consectetur adipiscing elit</a>.</em></strong></p><p><br></p>\r\n<h4>Vehicula sem ut volutpat</h4><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. Pellentesque viverra vehicula sem ut volutpat. Integer sed arcu. At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non sit amet, consectetur adipiscing elit. Ut adipiscing elit magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. Pellentesque viverra vehicula sem ut volutpat. Lorem ipsum dolor sit amet. Et harum quidem rerum facilis est et expedita distinctio lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non <a href=\"#\">libero consectetur adipiscing</a> elit magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. Pellentesque viverra vehicula sem ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.</p><p>Iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non ipsum dolor sit amet. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat <a href=\"#\">consectetur adipiscing elit</a>.</p><p><br></p>\r\n<h4><em>Vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis</em></h4><ul>\r\n            	<li>Ut adipiscing elit magna sed et quam lacus. Fusce condimentum eleifend enim a feugiat. Pellentesque viverra vehicula sem ut volutpat. Lorem ipsum dolor sit amet. Et harum quidem rerum facilis fusce condimentum eleifend enim a feugiat.</li>\r\n            	<li>Lusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati.</li>\r\n            	<li>Praesentium voluptatum deleniti atque corrupti quos</li>\r\n            	<li>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi.</li>\r\n            	<li>Mentum eleifend enim a feugiat distinctio lor</li>\r\n            </ul><p>Iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non ipsum dolor sit amet. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat <a href=\"#\">consectetur adipiscing elit</a>.</p>','Политика конфиденциальности','Политика конфиденциальности',1,0,0,'column2',''),(3,NULL,'ru',NULL,'2015-07-09 05:37:07','2015-09-18 06:41:58',1,1,'Главаня','Главная','index','<!-- Info Blokcs --><div class=\"row margin-bottom-40\">\r\n	<!-- Welcome Block -->\r\n	<div class=\"col-md-6 md-margin-bottom-40\">\r\n		<img src=\"/uploads/image/58815643d69eade7209bbfa28176d02c.jpg\" class=\"img-responsive\">\r\n	</div>\r\n	<!--/col-md-8-->\r\n	<!-- Posts -->\r\n	<div class=\"col-md-6\">\r\n		<div class=\"headline\">\r\n			<h2>Реконструкция</h2>\r\n		</div>\r\n		<p>\r\n			Система ухода за волосами на основе кератина и коллагена Содержит натуральный экстракт женьшеня \r\nВосстанавливающий филлер-уход для поврежденных и ломких волос - это интенсивная программа для реконструкции, созданная в соответствии с инновационной технологией. Филлер-уход способствует заполнению волос изнутри, восстанавливает поврежденную структуру безжизненных волос, обогащает протеинами и увлажняет. Система ухода за волосами на основе кератина и коллагена улучшает и сохраняет результаты окрашивания, зививки, выпрямления и стайлинга. Волосы выглядят ухоженными после первого применения.\r\n		</p>\r\n		<a href=\"/store/professional/reconstruct\" class=\"btn-u btn-u-green\">Подробнее</a>\r\n	</div>\r\n</div><div class=\"row margin-bottom-40\">\r\n	<!-- Welcome Block -->\r\n	<div class=\"col-md-6 md-margin-bottom-40\">\r\n		<div class=\"headline\">\r\n			<h2>Завивка<br></h2>\r\n		</div>\r\n		<p>\r\n			Система завивки волос на основе кератина Содержит натуральный экстракт алоэ вера \r\nСистема завивки волос на основе кератина - это новое средство, гарантирующее стойкие локоны. Максимально бережное воздействие. Формула без аммиака и формальдегида. Продукты этой серии содержат кератин и натуральный экстракт алоэ вера, которые питают и увлажняют волосы от корней до самых кончиков, делая локоны эластичными, естественными, мягкими, блестящими и здоровыми, предотвращают спутывание волос.\r\n		</p>\r\n		<a href=\"/store/professional/curlfriends\" class=\"btn-u btn-u-green\">Подробнее</a>\r\n	</div>\r\n	<!--/col-md-8-->\r\n	<!-- Posts -->\r\n	<div class=\"col-md-6\">\r\n		<img src=\"/uploads/image/12f4b7534bbf04a53d38054769cc959e.jpg\" class=\"img-responsive\">\r\n	</div>\r\n</div><div class=\"row margin-bottom-40\">\r\n	<!-- Welcome Block -->\r\n	<div class=\"col-md-6 md-margin-bottom-40\">\r\n		<img src=\"/uploads/image/bd8e848534f88cdc2798fdd66132ca11.jpg\" class=\"img-responsive\">\r\n	</div>\r\n	<!--/col-md-8-->\r\n	<!-- Posts -->\r\n	<div class=\"col-md-6\">\r\n		<div class=\"headline\">\r\n			<h2>Выпрямление</h2>\r\n		</div>\r\n		<p>\r\n			Система выпрямления волос на основе кератина Содержит натуральное льняное масло \r\nКератиновая система для идеального стойкого выпрямления специально предназначена для непослушных волос. Максимально бережное воздействие. Формула без аммиака и формальдегида. Продукты этой серии содержат кератин и натуральное льняное масло, которые питают и увлажняют волосы от корней до самых кончиков, делая их гладкими, мягкими и блестящими.\r\n		</p>\r\n		<a href=\"/store/professional/smoothing\" class=\"btn-u btn-u-green\">Подробнее</a>\r\n	</div>\r\n</div><div class=\"row margin-bottom-40\">\r\n	<!-- Welcome Block -->\r\n	<div class=\"col-md-6 md-margin-bottom-40\">\r\n		<div class=\"headline\">\r\n			<h2>Окрашивание<br></h2>\r\n		</div>\r\n		<a href=\"/store/professional/keeping\" class=\"btn-u btn-u-green\">Подробнее</a>\r\n	</div>\r\n	<!--/col-md-8-->\r\n	<!-- Posts -->\r\n	<div class=\"col-md-6\">\r\n		<img src=\"/uploads/image/6f30a5ac9e6b55623b95325be1d5f993.jpg\" class=\"img-responsive\">\r\n	</div>\r\n</div><div class=\"row margin-bottom-40\">\r\n	<!-- Welcome Block -->\r\n	<div class=\"col-md-6 md-margin-bottom-40\">\r\n		<img src=\"/uploads/image/5b665513f5124d303bcb832fca24683c.jpg\" class=\"img-responsive\">\r\n	</div>\r\n	<!--/col-md-8-->\r\n	<!-- Posts -->\r\n	<div class=\"col-md-6\">\r\n		<div class=\"headline\">\r\n			<h2>Осветление</h2>\r\n		</div>\r\n		<a href=\"/store/professional/lightening\" class=\"btn-u btn-u-green\">Подробнее</a>\r\n	</div>\r\n</div><div class=\"row margin-bottom-40\">\r\n	<!-- Welcome Block -->\r\n	<div class=\"col-md-6 md-margin-bottom-40\">\r\n		<div class=\"headline\">\r\n			<h2>Очищение</h2>\r\n		</div>\r\n		<a href=\"/store/professional/extralife\" class=\"btn-u btn-u-green\">Подробнее</a>\r\n	</div>\r\n	<!--/col-md-8-->\r\n	<!-- Posts -->\r\n	<div class=\"col-md-6\">\r\n		<img src=\"/uploads/image/ce57e2fed0fe2d340d1fca83d22cbe64.jpg\" class=\"img-responsive\">\r\n	</div>\r\n</div><div class=\"row margin-bottom-40\">\r\n	<!-- Welcome Block -->\r\n	<div class=\"col-md-6 md-margin-bottom-40\">\r\n		<img src=\"/uploads/image/b87fbeba993547cc77bdc933a7ef8fc0.jpg\" class=\"img-responsive\">\r\n	</div>\r\n	<!--/col-md-8-->\r\n	<!-- Posts -->\r\n	<div class=\"col-md-6\">\r\n		<div class=\"headline\">\r\n			<h2>Объем</h2>\r\n		</div>\r\n		<a href=\"/store/professional/volumizing\" class=\"btn-u btn-u-green\">Подробнее</a>\r\n	</div>\r\n</div><div class=\"row margin-bottom-40\">\r\n	<!-- Welcome Block -->\r\n	<div class=\"col-md-6 md-margin-bottom-40\">\r\n		<div class=\"headline\">\r\n			<h2>Стайлинг</h2>\r\n		</div>\r\n		<a href=\"/store/professional/finish\" class=\"btn-u btn-u-green\">Подробнее</a>\r\n	</div>\r\n	<!--/col-md-8-->\r\n	<!-- Posts -->\r\n	<div class=\"col-md-6\">\r\n		<img src=\"/uploads/image/bbc67b7de8a756629db2203b06f3e9d5.jpg\" class=\"img-responsive\">\r\n	</div>\r\n</div><!--/row--><!-- End Info Blokcs -->','Главаня','Главаня',1,0,0,'column1','');
/*!40000 ALTER TABLE `yupe_page_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_queue_queue`
--

DROP TABLE IF EXISTS `yupe_queue_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_queue_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `worker` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `task` text NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `complete_time` datetime DEFAULT NULL,
  `priority` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '0',
  `notice` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ux_yupe_queue_queue_worker` (`worker`),
  KEY `ux_yupe_queue_queue_priority` (`priority`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_queue_queue`
--

LOCK TABLES `yupe_queue_queue` WRITE;
/*!40000 ALTER TABLE `yupe_queue_queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_queue_queue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_sitemap_page`
--

DROP TABLE IF EXISTS `yupe_sitemap_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_sitemap_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(250) NOT NULL,
  `changefreq` varchar(20) NOT NULL,
  `priority` float NOT NULL DEFAULT '0.5',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_sitemap_page_url` (`url`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_sitemap_page`
--

LOCK TABLES `yupe_sitemap_page` WRITE;
/*!40000 ALTER TABLE `yupe_sitemap_page` DISABLE KEYS */;
INSERT INTO `yupe_sitemap_page` VALUES (1,'/','daily',0.5,1);
/*!40000 ALTER TABLE `yupe_sitemap_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_social_user`
--

DROP TABLE IF EXISTS `yupe_social_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_social_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `provider` varchar(250) NOT NULL,
  `uid` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_social_user_uid` (`uid`),
  KEY `ix_yupe_social_user_provider` (`provider`),
  KEY `fk_yupe_social_user_user_id` (`user_id`),
  CONSTRAINT `fk_yupe_social_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_social_user`
--

LOCK TABLES `yupe_social_user` WRITE;
/*!40000 ALTER TABLE `yupe_social_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_social_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_attribute`
--

DROP TABLE IF EXISTS `yupe_store_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `unit` varchar(30) DEFAULT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_store_attribute_name_group` (`name`,`group_id`),
  KEY `ix_yupe_store_attribute_title` (`title`),
  KEY `fk_yupe_store_attribute_group` (`group_id`),
  CONSTRAINT `fk_yupe_store_attribute_group` FOREIGN KEY (`group_id`) REFERENCES `yupe_store_attribute_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_attribute`
--

LOCK TABLES `yupe_store_attribute` WRITE;
/*!40000 ALTER TABLE `yupe_store_attribute` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_store_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_attribute_group`
--

DROP TABLE IF EXISTS `yupe_store_attribute_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_attribute_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_attribute_group`
--

LOCK TABLES `yupe_store_attribute_group` WRITE;
/*!40000 ALTER TABLE `yupe_store_attribute_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_store_attribute_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_attribute_option`
--

DROP TABLE IF EXISTS `yupe_store_attribute_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_attribute_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) DEFAULT NULL,
  `position` tinyint(4) DEFAULT NULL,
  `value` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_store_attribute_option_attribute_id` (`attribute_id`),
  KEY `ix_yupe_store_attribute_option_position` (`position`),
  CONSTRAINT `fk_yupe_store_attribute_option_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `yupe_store_attribute` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_attribute_option`
--

LOCK TABLES `yupe_store_attribute_option` WRITE;
/*!40000 ALTER TABLE `yupe_store_attribute_option` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_store_attribute_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_category`
--

DROP TABLE IF EXISTS `yupe_store_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(150) NOT NULL,
  `name` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text,
  `meta_title` varchar(250) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `meta_keywords` varchar(250) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_store_category_alias` (`slug`),
  KEY `ix_yupe_store_category_parent_id` (`parent_id`),
  KEY `ix_yupe_store_category_status` (`status`),
  CONSTRAINT `fk_yupe_store_category_parent` FOREIGN KEY (`parent_id`) REFERENCES `yupe_store_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_category`
--

LOCK TABLES `yupe_store_category` WRITE;
/*!40000 ALTER TABLE `yupe_store_category` DISABLE KEYS */;
INSERT INTO `yupe_store_category` VALUES (1,NULL,'products','Поддерживающий уход',NULL,'<p>короткое описание для категории Поддерживающий уход</p>','<p>Описание для категории Поддерживающий уход</p>','Поддерживающий уход​ Previa','Поддерживающий уход​ Previa','Поддерживающий уход​ Previa',1,1),(2,NULL,'professional','Профессиональные средства',NULL,'<p>Короткое описание для Профессиональные средства</p>','<p>Подробное описание для Профессиональные средства</p>','Профессиональные средства Previa','Профессиональные средства Previa','Профессиональные средства Previa',1,2),(3,1,'defined-curls','Четкие и упругие локоны',NULL,'<p>Короткое описание Четкие и упругие локоны</p>','<p>Полное описание Четкие и упругие локоны</p>','Четкие и упругие локоны Previa','Четкие и упругие локоны Previa','Четкие и упругие локоны Previa',1,3),(4,1,'perfect-straightness','Идеальное и стойкое выпрямление',NULL,'<p>Короткое описание Идеальное и стойкое выпрямление</p>','<p>Подробное описание Идеальное и стойкое выпрямление</p>','Идеальное и стойкое выпрямление Previa','Идеальное и стойкое выпрямление Previa','Идеальное и стойкое выпрямление Previa',1,4),(5,1,'long-lasting-shiny-colour','Стойкий и блестящий цвет',NULL,'<p>Короткое описание Стойкий и блестящий цвет</p>','<p>Подробное описание Стойкий и блестящий цвет</p>','Стойкий и блестящий цвет Previa','Стойкий и блестящий цвет Previa','Стойкий и блестящий цвет Previa',1,5),(6,1,'for-platinum-blonde-and-grey-hair','Платиновый блонд и седые волосы',NULL,'<p>Короткое описание Платиновый блонд и седые волосы</p>','<p>Подробное описание Платиновый блонд и седые волосы</p>','Платиновый блонд и седые волосы Previa','Платиновый блонд и седые волосы Previa','Платиновый блонд и седые волосы Previa',1,6),(7,1,'well-being-of-hair-and-scalp','Очищение волос и кожи головы',NULL,'<p>Короткое описание Очищение волос и кожи головы Previa</p>','<p>Подробное описание Очищение волос и кожи головы Previa</p>','Очищение волос и кожи головы Previa','Очищение волос и кожи головы Previa','Очищение волос и кожи головы Previa',1,7),(8,1,'damaged-and-brittle-hair','Поврежденные и ломкие волосы',NULL,'<p>Короткое описание Поврежденные и ломкие волосы</p>','<p>Подробное описание Поврежденные и ломкие волосы</p>','Поврежденные и ломкие волосы','Поврежденные и ломкие волосы','Поврежденные и ломкие волосы',1,8),(9,1,'fine-and-thin-hair','Тонкие и слабые волосы',NULL,'<p>Короткое описание Тонкие и слабые волосы</p>','<p>Подробное описание Тонкие и слабые волосы</p>','Тонкие и слабые волосы','Тонкие и слабые волосы','Тонкие и слабые волосы',1,9),(10,1,'create-your-style','Создание собственного стиля',NULL,'<p>Короткое описание Создание собственного стиля</p>','<p>Подробное описание Создание собственного стиля</p>','Создание собственного стиля','Создание собственного стиля','Создание собственного стиля',1,10),(11,2,'curlfriends','Завивка','337e1e40cef8738c4d6dfedc521a95a2.jpg','','<h1>CURLFRIENDS</h1><p><strong>Система завивки волос на основе кератина</strong>\r\n</p><p><strong>Содержит натуральный экстракт алоэ вера</strong>\r\n</p><p>Система завивки волос на основе кератина - это новое средство, гарантирующее стойкие локоны. Максимально бережное воздействие. Формула без аммиака и формальдегида. Продукты этой серии содержат кератин и натуральный экстракт алоэ вера, которые питают и увлажняют волосы от корней до самых кончиков, делая локоны эластичными, естественными, мягкими, блестящими и здоровыми, предотвращают спутывание волос.\r\n</p><p><img src=\"http://previarussia.local/uploads/image/80520ace374e49301991a553a7ffd956.jpg\">\r\n</p><h2>Система завивки волос</h2><p>Система завивки волос на основе кератина позволяет добиться превосходного стойкого результата в два простых шага.\r\n</p><p><strong>Шаг 1: средства для стойкой завивки.</strong>\r\n</p><p>Кератиновый лосьон и кератиновый нейтрализатор для стойкой завивки волос. Не содержат аммиак, формальдегид, тиогликолевую кислоту и ее производные. Содержат кератин, натуральный экстракт алоэ вера, масло макадамии, соевые и пшеничные протеины. Созданы для максимально мягкого воздействия на волосы и кожу головы.\r\n</p><p><strong>Шаг 2: средства для последующего ухода.</strong>\r\n</p><p>Интенсивно увлажняющий шампунь, интенсивно увлажняющий кондиционер, разглаживающее несмываемое средство для волос. Средства для работы в салоне красоты и последующего ухода закрепляют эффект завивки, интенсивно питают и увлажняют волосы, делая их мягкими и блестящими.\r\n</p><p>Система завивки волос на основе кератина содержит активные ингредиенты, обеспечивающие питательное и кондиционирующее воздействие. Позволяет создать эластичные локоны, оставляя волосы яркими, мягкими и крепкими, в то же время не утяжеляет их, чтобы локоны не потеряли легкость. Максимально облегчает расчесывание.\r\n</p><p>Эластичные и естественные локоны.\r\n</p><p>Гарантированный объем волос.\r\n</p><p>Максимально облегчает расчесывание.\r\n</p><p>Устраняет спутывание волос, обладает антистатическими свойствами.\r\n</p><p>Обеспечивает мягкость, здоровье и блеск волос.\r\n</p><p><strong>Шаг 1: средства для стойкой завивки</strong>\r\n</p><ul><li>Кератиновый лосьон для завивки волос: «0» - для завивки трудноподдающихся волос; «1» - для завивки натуральных волос; «2» - для завивки поврежденных волос.</li><li>Кератиновый нейтрализатор</li></ul><p>Благодаря использованию отборных натуральных ингредиентов, Кератиновая система для завивки гарантирует превосходные результаты и максимально щадящее действие на волосы и кожу головы. Не содержит аммиак, тиогликолевую кислоту и ее производные. Кератиновая система для завивки обеспечивает формирование новых химических связей в структуре волоса для мягкого и здорового завитка, а также увлажняет и кондиционирует волосы, делая их исключительно мягкими и блестящими.\r\n</p><p>Стойкая завивка на основе кератина.\r\n</p><p>Эластичные, естественные, мягкие, блестящие и здоровые локоны.\r\n</p><p>Простота использования гарантирует максимальное применение в салоне.\r\n</p><p>Не содержит аммиак, формальдегид, тиогликолевую кислоту и ее производные.\r\n</p><p>Максимально бережное воздействие.\r\n</p><p><strong>Применение:</strong>\r\n</p><ol><li>Нанести Интенсивно увлажняющий шампунь на влажные волосы. Смыть и подсушить волосы полотенцем.</li><li>Накрутить волосы на бигуди. Рекомендуется использовать бигуди из натурального пробкового дерева для получения более натуральных локонов.</li><li>Тщательно смочить Кератиновым лосьоном для завивки волосы, накрученные на бигуди, начиная с затылка и заканчивая передней частью головы. В зависимости от структуры и сопротивления волос время выдержки составляет от 5 до 30 минут без применения тепла. Графа «Тест» в таблице – это время, после которого необходимо проверить некоторые бигуди на разных участках головы, чтобы оценить степень завитка. Рекомендуется применение сетки для волос для достижения максимального результата увлажнения и эластичности.</li><li>После окончания времени выдержки тщательно промыть бигуди теплой (не горячей) водой (не менее 3-х минут). Подсушить волосы полотенцем. Система завивки волос на основе кератина содержит активный ингредиент Цистиамин (аминокислоту, составляющую кератин волоса). Уровень рН щелочной. Высокое содержание серы, что обеспечивает полностью совместимость со структурой волос. В отдельных случаях может выделять неприятный запах.</li><li>Тщательно смочить Кератиновым нейтрализатором волосы, накрученные на бигуди. Время выдержки Кертинового нейтрализатора 5 минут. Раскрутить бигуди. Используя нейтрализатор, тщательно пропитать длину и кончики волос. Время выдержки 5 минут. Тщательно промыть волосы теплой (не горячей) водой (не менее 3-х минут).</li><li>Нанести Интенсивно увлажняющий шампунь на влажные волосы. Смыть и подсушить волосы полотенцем.</li><li>Нанести Интенсивно питающий кондиционер на вымытые и подсушенные полотенцем волосы. Оставить на 3-5 минут. Тщательно смыть теплой водой.</li><li>Нанести Разглаживающее несмываемое средство для волос. Сделать укладку.</li></ol><p><strong>Применение для процедуры «Буст Ап!»:</strong>\r\n</p><ol><li>Нанести Интенсивно увлажняющий шампунь на влажные волосы. Смыть и подсушить волосы полотенцем.</li><li>Накрутить волосы на бигуди с применением технологии «Буст Ап!».</li><li>Для дополнительной защиты нанести Интенсивно питающий кондиционер на оставшуюся часть волос.</li><li>Тщательно смочить Кератиновым лосьоном для завивки волосы, накрученные на бигуди, с применением технологии «Буст Ап!». В зависимости от структуры и сопротивления волос время выдержки составляет от 5 до 30 минут без применения тепла.</li><li>После окончания времени выдержки тщательно промыть бигуди теплой (не горячей) водой (не менее 3-х минут). Подсушить волосы полотенцем. Система завивки волос на основе кератина содержит активный ингредиент Цистиамин (аминокислоту, составляющую кератин волоса). Уровень рН щелочной. Высокое содержание серы, что обеспечивает полностью совместимость со структурой волос. В отдельных случаях может выделять неприятный запах.</li><li>Тщательно смочить Кератиновым нейтрализатором волосы, накрученные на бигуди. Время выдержки Кертинового нейтрализатора 5 минут. Тщательно промыть волосы теплой (не горячей) водой (не менее 3 х минут). Раскрутить бигуди.</li><li>Нанести Интенсивно увлажняющий шампунь на влажные волосы. Смыть и подсушить волосы полотенцем.</li><li>Нанести Интенсивно питающий кондиционер на вымытые и подсушенные полотенцем волосы. Оставить на 3-5 минут. Тщательно смыть теплой водой.</li><li>Нанести Разглаживающее несмываемое средство для волос на зону «Буст Ап!». Сделать укладку.</li></ol><p><strong>Шаг 2: средства для последующего ухода*</strong>\r\n</p><ul>\r\n	<li>Интенсивно увлажняющий шампунь</li>\r\n	<li>Интенсивно питающий кондиционер</li>\r\n	<li>Разглаживающее несмываемое средство для волос</li>\r\n</ul><p style=\"margin-left: 20px;\"><span style=\"font-size: 12px;\">*Рекомендовано как поддерживающий уход в домашних условиях.</span>\r\n</p><p>Дополните стойкую завивку в салоне последующим уходом, средства которого закрепляют эффект завивки, питают, увлажняют, делают локоны эластичными, естественными, мягкими, блестящими и здоровыми, предотвращают спутывание волос.\r\n</p><p>Интенсивно увлажняющий шампунь\r\n</p><p>Шампунь с эффектом кондиционера. Бережно очищает и увлажняет волосы, делает их мягкими и блестящими. Восстанавливает баланс pH. Устраняет эффект статического электричества. Смягчает эффект завивки.\r\n</p><p>Применение: нанести на влажные волосы, помассировать. Тщательно смыть. При необходимости повторить.\r\n</p><p>Интенсивно питающий кондиционер\r\n</p><p>Бережно питает и увлажняет волосы, делает их мягкими и блестящими. Восстанавливает баланс pH. Устраняет эффект статического электричества. Смягчает эффект завивки.\r\n</p><p>Применение: нанести на вымытые и высушенные полотенцем волосы. Оставить на 3-5 минут. Тщательно смыть.\r\n</p><p>Разглаживающее несмываемое средство для волос\r\n</p><p>Облегчает укладку, защищает от влаги и устраняет эффект статического электричества. Придает силу и блеск, делает волосы эластичными. Устраняет спутывание волос. Смягчает эффект завивки. Идеально подходит для естественной сушки. Сильная фиксация.\r\n</p><p>Применение: нанести на влажные волосы. Сделать укладку. Не смывать.\r\n</p><p><br>\r\n</p><h3>Активные ингредиенты\r\n</h3><p><strong>Кератин </strong>- один из главных натуральных компонентов, составляющих волос. Это протеин, регулирующий структуру волоса и придающий ему форму. Маленький размер молекул позволяет кератину проникать в ткани волоса, укрепляя и реструктурируя их. Проникая в кутикулу, он питает и увлажняет волос, облегчает расчесывание, придает мягкость и блеск.\r\n</p><p><strong>Натуральный экстракт алоэ вера</strong> богат витаминами, минералами и аминокислотами. Он питает и увлажняет волосы, делая их блестящими, мягкими и эластичными. Увлажняет и способствует образованию защитной пленки, которая удерживает влагу. Обладает заживляющим и регенерирующим действием, ферментативно растворяет и разрушает мертвые или поврежденные клетки, стимулирует процессы восстановления. Интенсивно увлажняет упругие и здоровые локоны.\r\n</p><p><strong>Масло макадамии</strong> питает и придает мягкость, делает волосы легкими для расчесывания и полностью подготовленными к стайлингу, не жирнит волосы. Благодаря своим свойствам, схожим со свойствами кожного жира, создает защитную гидролипидную пленку, противодействует сухости волос, придает блеск, мягкость и шелковистость. Масло макадамии не дает волосам запутываться и защищает от влажности.\r\n</p><p><strong>Соевые протеины</strong> имеют высокое содержание аргинина и разнообразные аминокислоты. Аргинин имеет заметное увлажняющее и антивозрастное действие. Подходит для сухих и безжизненных волос, также для волос, обработанных средствами для химической завивки.\r\n</p><p><strong>Пшеничные протеины</strong> - это уникальный увлажняющий комплекс, способный проникать в структуру кутикулы волоса. Благодаря свойствам образовывать пленку, протеины пшеницы способствуют силе, объему и блеску волос, восстанавливая их структуру. Подходят для питания завитых, спутывающихся или поврежденных волос.\r\n</p>','Завивка','Завивка','Завивка',1,11),(12,2,'smoothing','Выпрямление','8f6ba7782111289bb68ced2f7ef4081a.jpg','<h1>SMOOTHING\r\n</h1><p><strong>Система выпрямления волос на основе кератина\r\n</strong></p><p><strong>Содержит натуральное льняное масло</strong>\r\n</p><p>Кератиновая система для идеального стойкого выпрямления специально предназначена для непослушных волос. Максимально бережное воздействие. Формула без аммиака и формальдегида. Продукты этой серии содержат кератин и натуральное льняное масло, которые питают и увлажняют волосы от корней до самых кончиков, делая их гладкими, мягкими и блестящими.</p>','<h1>SMOOTHING\r\n</h1><p><strong>Система выпрямления волос на основе кератина\r\n	</strong>\r\n</p><p><strong>Содержит натуральное льняное масло</strong>\r\n</p><p>Кератиновая система для идеального стойкого выпрямления специально предназначена для непослушных волос. Максимально бережное воздействие. Формула без аммиака и формальдегида. Продукты этой серии содержат кератин и натуральное льняное масло, которые питают и увлажняют волосы от корней до самых кончиков, делая их гладкими, мягкими и блестящими.\r\n</p><p><img src=\"http://previarussia.local/uploads/image/913d03e7214179e383fcc358a2872356.jpg\" class=\"img-responsive\">\r\n</p><h2>Система выпрямления волос\r\n</h2><p>Система выпрямления волос на основе кератина позволяет добиться превосходного стойкого результата в два простых шага.\r\n</p><p><strong>Шаг 1: средства для стойкого выпрямления.</strong>\r\n</p><p>Кератиновый выпрямляющий гель и Кератиновый выпрямляющий нейтрализатор для стойкого выпрямления волос. Не содержат аммиак, формальдегид, тиогликолевую кислоту и ее производные.\r\n</p><p>Содержат кератин, натуральное льняное масло, гидролизованные протеины шелка, алое вера. Созданы для максимально мягкого воздействия на волосы и кожу головы.\r\n</p><p><strong>Шаг 2: средства для последующего ухода.</strong>\r\n</p><p>Кератиновый разглаживающий шампунь, Кератиновый разглаживающий кондиционер и Кератиновое средство для блеска волос с разглаживающим эффектом. Средства для работы в салоне красоты и последующего ухода закрепляют эффект выпрямления, питают, увлажняют и распутывают волосы, делая их гладкими, здоровыми и блестящими.\r\n</p><p>Система выпрямления волос на основе кератина заметно сокращает объем. Восстанавливает структуру волос, защищает их от влаги, предотвращает эффект начеса. Помогает справиться с непослушными волосами, делая их мягкими и шелковистыми, также максимально облегчает расчесывание.\r\n</p><p>Идеально для длительного выпрямления.\r\n</p><p>Уменьшает объем.\r\n</p><p>Идеально для утреннего ухода за волосами.\r\n</p><p>Максимально облегчает расчесывание.\r\n</p><p>Убирает эффект начеса, обладает антистатическими свойствами.\r\n</p><p>Обеспечивает мягкость, здоровье и блеск волос.\r\n</p><p><strong>Шаг 1: средства для стойкого выпрямления</strong>\r\n</p><ul>\r\n	<li><strong>Кератиновый выпрямляющий гель</strong></li>\r\n	<li><strong>Кератиновый выпрямляющий нейтрализатор</strong></li>\r\n</ul><p>Обеспечивают идеальное и длительное выпрямление. Волосы становятся мягкими, блестящими и шелковистыми.\r\n</p><p>Стойкий эффект идеального выпрямления.\r\n</p><p>Простота использования гарантирует максимальное применение в салоне.\r\n</p><p>Не содержит аммиак, формальдегид, тиогликолевую кислоту и ее производные.\r\n</p><p>Максимально бережное воздействие.\r\n</p><p><strong>Применение:</strong>\r\n</p><ol>\r\n	<li>Нанести Кератиновый разглаживающий шампунь на влажные волосы. Смыть и подсушить волосы полотенцем.</li>\r\n	<li>Разделить волосы по линии от уха до уха на две зоны: переднюю и заднюю.</li>\r\n	<li>Начать нанесение геля с задней части снизу наверх до линии разделения зон горизонтальными секциями (макс. 1 см). Переднюю зону необходимо разделить на три части: центральную прямоугольную секцию и две боковые секции. Нанести гель на боковые секции, убирая волосы на заднюю зону, обработанную ранее. Затем перейти к центральной секции (последняя обрабатываемая секция): нанести гель, убирая волосы на заднюю зону, обработанную ранее.</li>\r\n	<li>В зависимости от типа волос и желаемой степени выпрямления время выдержки составляет от 10 до 30 минут.</li>\r\n	<li>Смыть Кератиновый выпрямляющий гель большим количеством теплой (не горячей) воды (не менее 3-х минут). Подсушить волосы полотенцем.</li>\r\n	<li>Нанести Кератиновый выпрямляющий нейтрализатор, распределяя его плоским гребнем по всей длине обработанных волос.</li>\r\n	<li>Время выдержки Кератинового выпрямляющего нейтрализатора составляет от 10 до 30 минут и совпадает со временем выдержки Кератинового выпрямляющего геля. Перед окончанием времени выдержки уложить волосы в желаемом направлении и оставить на несколько минут.</li>\r\n	<li>Смыть Кератиновый выпрямляющий нейтрализатор большим количеством теплой (не горячей) воды (не менее 3-х минут).</li>\r\n	<li>Нанести Кератиновый разглаживающий шампунь на влажные волосы. Смыть и подсушить волосы полотенцем.</li>\r\n	<li>Нанести Кератиновый разглаживающий кондиционер на вымытые и подсушенные полотенцем волосы. Оставить на 3-5 минут. Тщательно смыть теплой водой.</li>\r\n	<li>Нанести Кератиновое средство для блеска волос с разглаживающим эффектом. Сделать укладку.</li>\r\n</ol><p><span></span>\r\n</p><p><strong>Шаг 2: средства для последующего ухода*</strong>\r\n</p><ul>\r\n	<li><strong>Кератиновый разглаживающий шампунь</strong></li>\r\n	<li><strong>Кератиновый разглаживающий кондиционер</strong></li>\r\n	<li><strong>Кератиновое средство для блеска волос с разглаживающим эффектом</strong></li>\r\n</ul><p style=\"margin-left: 20px;\"><span style=\"font-size: 12px;\">*Рекомендовано как поддерживающий уход в домашних условиях.</span>\r\n</p><p>Дополните стойкое выпрямление в салоне последующим уходом, средства которого закрепляют эффект выпрямления, питают, увлажняют и распутывают волосы, делая их гладкими, здоровыми и блестящими.\r\n</p><p><br>\r\n</p><p><strong>Кератиновый разглаживающий шампунь</strong>\r\n</p><p>Шампунь с содержанием кератина для разглаживания волос. Мягко очищает, увлажняет и разглаживает волосы, делает их мягкими, шелковистыми и блестящими. Восстанавливает баланс pH. Устраняет эффект статического электричества и спутывание волос. Применение: нанести на влажные волосы, помассировать. Тщательно смыть. При необходимости повторить.\r\n</p><p><strong>Кератиновый разглаживающий кондиционер</strong>\r\n</p><p>Кондиционер с содержанием кератина для разглаживания волос. Бережно питает, увлажняет и разглаживает волосы, делает их мягкими, шелковистыми и блестящими. Восстанавливает баланс pH. Устраняет эффект статического электричества и спутывание волос.\r\n</p><p>Применение: нанести на вымытые и высушенные полотенцем волосы. Оставить на 3-5 минут.\r\n</p><p>Тщательно смыть.\r\n</p><p><strong>Кератиновое средство для блеска волос с разглаживающим эффектом</strong>\r\n</p><p>Облегчает укладку, защищает от влаги, питает и увлажняет волосы, делает их шелковистыми и блестящими. Устраняет эффект статического электричества. Защищает от воздействия тепла. Не оставляет следов.\r\n</p><p>Применение: нанести на влажные волосы. Сделать укладку. Не смывать.\r\n</p><p><br>\r\n</p><p><br>\r\n</p><h4>Активные ингредиенты\r\n</h4><p><strong>Кератин</strong> - один из главных натуральных компонентов, составляющих волос. Это протеин, регулирующий структуру волоса и придающий ему форму. Маленький размер молекул позволяет кератину проникать в ткани волоса, укрепляя и реструктурируя их. Проникая в кутикулу, он питает и увлажняет волос, облегчает расчесывание, придает мягкость и блеск.\r\n</p><p><strong>Натуральное льняное масло</strong> идеально подходит для ломких, хрупких, сухих и путающихся волос. Создает невидимую пленку, покрывающую волос. Придает блеск, распутывает и оживляет волосы благодаря омега-кислотам 3-6-9.\r\n</p><p><strong>Гидролизованные протеины шелка</strong> питают волос и кутикулу, заполняют кератином, помогают восстановить и усилить защитный барьер от атмосферных факторов, механических и тепловых воздействий. Фиброин шелка придает яркость и гибкость, делая волосы приятными на вид и ощупь.\r\n</p><p><strong>Алоэ вера </strong>в косметической трихологии используется, чтобы компенсировать агрессивные атмосферные воздействия такие, как снег, холод и солнце. Увлажняет и защищает, образует пленку, которая сохраняет влагу в тканях. Заживляет и регенерирует, растворяет и разрушает мертвые и поврежденные клетки на молекулярном уровне, стимулирует возрождающие процессы.\r\n</p>','Выпрямление','Выпрямление','Выпрямление',1,12),(13,2,'keeping','Окрашивание',NULL,'<p>Короткое описание Окрашивание</p>','<p>Подробное описание Окрашивание</p>','Окрашивание','Окрашивание','Окрашивание',1,13),(14,2,'lightening','Осветление',NULL,'<p>Короткое описание Осветление</p>','<p>Подробное описание Осветление</p>','Осветление','Осветление','Осветление',1,14),(15,2,'extralife','Очищение',NULL,'<p>Короткое описание Очищение</p>','<p>Подробное описание Очищение</p>','Очищение','Очищение','Очищение',1,15),(16,2,'reconstruct','Реконструкция','afcd4c56c5f48c8de4d8307638a8c38c.jpg','','<h1>ECONSTRUCT</h1><p><strong>Система ухода за волосами на основе кератина и коллагена</strong>\r\n</p><p><strong>Содержит натуральный экстракт женьшеня</strong>\r\n</p><p>Восстанавливающий филлер-уход для поврежденных и ломких волос - это интенсивная программа для реконструкции, созданная в соответствии с инновационной технологией. Филлер-уход способствует заполнению волос изнутри, восстанавливает поврежденную структуру безжизненных волос, обогащает протеинами и увлажняет. Система ухода за волосами на основе кератина и коллагена улучшает и сохраняет результаты окрашивания, зививки, выпрямления и стайлинга. Волосы выглядят ухоженными после первого применения.\r\n</p><p><img src=\"/uploads/image/1b9fd5f1f7393a02aecdd185ebdfb8d0.jpg\" alt=\"/uploads/image/reconstruct-previa.jpg\" class=\"img-responsive\"></p><h2>Система ухода за волосами</h2><p>Окрашивание, осветление, завивка, чрезмерное использование фена и утюжка, действие атмосферных факторов оказывают вредное воздействие на волосы, делая их сухими, поврежденными, слабыми и безжизненными. Система ухода за волосами на основе кератина и коллагена действует на волокна волос, заполняя их и восстанавливая оптимальную структуру. Волосы становятся плотнее, крепче и лучше противостоят внешним воздействиям. Результат гарантирован инновационной формулой, содержащей высококачественные активные ингредиенты с мощными восстанавливающими свойствами. Формула, обогащенная кератином и коллагеном, действует как на поверхности волоса, так и проникая глубоко в его структуру.\r\n</p><p>Восстанавливает волосы изнутри.\r\n</p><p>Обеспечивает блеск, силу, эластичность и мягкость.\r\n</p><p>Улучшает результат и продлевает действие окрашивания, завивки, выпрямления и стайлинга.\r\n</p><p>Защищает от вредных атмосферных факторов, влажности и имеет антистатический эффект.\r\n</p><p><span style=\"font-size: 18px;\"><strong>Восстанавливает волосы всего за 15 минут</strong></span>\r\n</p><p><strong>Восстанавливающий филлер-уход</strong> – это многоступенчатая обновляющая программа, которая начинается в салоне и продолжается дома. Результат, полученный при интенсивном уходе, закрепляется и продлевается благодаря поддерживающей программе.\r\n</p><p>МГНОВЕННЫЙ ЭФФЕКТ: заметный результат после первого применения.\r\n</p><p>ЛЕГКОЕ ИСПОЛЬЗОВАНИЕ: гарантированная максимальная простота применения.\r\n</p><p>НЕ ЗАНИМАЕТ МНОГО ВРЕМЕНИ: необходимо всего 15 минут.\r\n</p><p><strong>Интенсивный восстанавливающий филлер-уход:</strong>\r\n</p><ul>\r\n	<li>Восстанавливающий шампунь</li>\r\n	<li>Средство для интенсивного питания волос</li>\r\n	<li>Увлажняющий кондиционер</li>\r\n</ul><p>РЕКОМЕНДОВАННО:\r\n</p><ul>\r\n	<li>для слабых, ломких, сухих, пористых и тусклых волос;</li>\r\n	<li>для подготовки волос к окрашиванию, осветлению, завивке, выпрямлению и т.д.</li>\r\n</ul><p><strong>Поддерживающий филлер-уход:</strong>\r\n</p><ul>\r\n	<li>Восстанавливающий шампунь</li>\r\n	<li>Увлажняющий кондиционер</li>\r\n</ul><p>РЕКОМЕНДОВАННО:\r\n</p><ul>\r\n	<li>для ухода за поврежденными и слабыми волосами;</li>\r\n	<li>для поддержки волос после окрашивания, осветления, завивки, выпрямления и т.д.</li>\r\n</ul><p><strong>Восстанавливающий шампунь</strong>\r\n</p><p>Шампунь с содержанием кератина и коллагена глубоко очищает, увлажняет поврежденные и истощенные от воздействия окружающей среды, а также после химического воздействия волосы. Оптимальным образом подготавливает волосы к восприятию активных компонентов для восстановления.\r\n</p><p>Применение: нанести массажными движениями на влажные волосы. Смыть. При необходимости повторить.\r\n</p><p><strong>Средство для интенсивного питания волос</strong>\r\n</p><p>Средство с содержанием кератина и коллагена интенсивно питает и восстанавливает поврежденные и истощенные после химического воздействия волосы. Придает волосам здоровый блеск.\r\n</p><p>Применение: нанести массажными движениями на вымытые и подсушенные полотенцем волосы. Оставить на 3-5 минут. Смыть.\r\n</p><p><strong>Увлажняющий кондиционер</strong>\r\n</p><p>Кондиционер с содержанием кератина и коллагена улучшает упругость, придает блеск поврежденным и истощенным от воздействия окружающей среды, а также после химического воздействия волосам. Способствует улучшению восприятия активных компонентов. Придает волосам здоровый блеск.\r\n</p><p>Применение: нанести массажными движениями на вымытые и подсушенные полотенцем волосы. Оставить на 3-5 минут. Смыть.\r\n</p><p><strong>Двухфазный несмываемый кондиционер</strong>\r\n</p><p>Средство с содержанием кератина и коллагена увлажняет и питает, делает волосы шелковистыми, блестящими и легкими. Устраняет спутывание волос. Защищает от вредного воздействия солнечных лучей и источников тепла. Устраняет эффект статического электричества.\r\n</p><p>Применение: нанести на влажные волосы. Сделать укладку. Не смывать.\r\n</p><p><strong>Сыворотка для волос</strong>\r\n</p><p>Регенерирующая сыворотка для волос с содержанием кератина и коллагена, запечатывающая кутикулу. Восстанавливает поврежденные и лишенные блеска волосы. Защищает от вредного воздействия солнечных лучей и источников тепла.\r\n</p><p>Применение: нанести на влажные или сухие волосы. Сделать укладку. Не смывать.\r\n</p><p><span style=\"font-size: 12px;\">*Для максимального результата:</span>\r\n</p><p>Продукция проникает более легко во влажные, высушенные полотенцем волосы. Капли влаги на волосах отталкивают активные ингредиенты, содержащиеся в средствах ухода и уменьшают эффект. Для увеличения эффективности можно использовать массажные движения во время выдержки средств на волосах. Это помогает более глубокому проникновению восстанавливающих и увлажняющих ингредиентов. Важно соблюдать время выдержки.\r\n</p><ul>\r\n	<li>Восстановленная структура изнутри и снаружи. Волосы снова здоровые, сильные, объемные и блестящие.</li>\r\n	<li>Волосы наполнены питательными веществами, увлажненные, эластичные, мягкие и легкие, легко расчесываются.</li>\r\n	<li>Поверхность волос выглядит сплошной и бархатистой. Волосы защищены от агрессивного воздействия окружающей среды.</li>\r\n</ul><h2>Активные ингредиенты</h2><p><strong>Кератин</strong> - один из главных натуральных компонентов, составляющих волос. Это протеин, регулирующий структуру волоса и придающий ему форму. Маленький размер молекул позволяет кератину проникать в ткани волоса, укрепляя и реструктурируя их. Проникая в кутикулу, он питает и увлажняет волос, облегчает расчесывание, придает мягкость и блеск.\r\n</p><p><strong>Коллаген </strong>- ключевой протеин, обеспечивающий эластичность и защиту тканей от механических повреждений, сохраняя их гибкими и увлажненными. Коллаген используется, чтобы улучшить ткани, составляющие кутикулу, делая волосы эластичными и мягкими. Волосы становятся плотными и сияющими. Действие коллагена способствует сохранению упругости и предотвращению дегидратации волос. Благодаря его свойству формировать пленку, он действует на поверхность волос с «запечатывающим эффектом».\r\n</p><p><strong>Женьшень </strong>имеет заметный противовозрастной эффект, насыщает энергией и оживляет. Благодаря его стимулирующим свойствам, волосы восстанавливаются по всей длине. Женьшень также способствует увеличению прочности волос и их сопротивляемость против самых различных повреждающих агентов химического, физического, механического и биологического характера.\r\n</p>','Реконструкция','Реконструкция','Реконструкция',1,16),(17,2,'volumizing','Объем',NULL,'<p>Короткое описание Объем</p>','<p>Подробное описание Объем</p>','Объем','Объем','Объем',1,17),(18,2,'finish','Стайлинг',NULL,'<p>Короткое описание Стайлинг</p>','<p>Подробное описание Стайлинг</p>','Стайлинг','Стайлинг','Стайлинг',1,18);
/*!40000 ALTER TABLE `yupe_store_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_coupon`
--

DROP TABLE IF EXISTS `yupe_store_coupon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `code` varchar(255) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `value` decimal(10,2) NOT NULL DEFAULT '0.00',
  `min_order_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `registered_user` tinyint(4) NOT NULL DEFAULT '0',
  `free_shipping` tinyint(4) NOT NULL DEFAULT '0',
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `quantity_per_user` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_coupon`
--

LOCK TABLES `yupe_store_coupon` WRITE;
/*!40000 ALTER TABLE `yupe_store_coupon` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_store_coupon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_delivery`
--

DROP TABLE IF EXISTS `yupe_store_delivery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_delivery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `price` float(10,2) NOT NULL DEFAULT '0.00',
  `free_from` float(10,2) DEFAULT NULL,
  `available_from` float(10,2) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `separate_payment` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_yupe_store_delivery_position` (`position`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_delivery`
--

LOCK TABLES `yupe_store_delivery` WRITE;
/*!40000 ALTER TABLE `yupe_store_delivery` DISABLE KEYS */;
INSERT INTO `yupe_store_delivery` VALUES (1,'Заберу самостоятельно','',0.00,NULL,NULL,1,1,0),(2,'Доставка курьером','<p>по Санкт-Петербургу в пределах КАД</p>',300.00,5000.00,1000.00,2,1,0);
/*!40000 ALTER TABLE `yupe_store_delivery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_delivery_payment`
--

DROP TABLE IF EXISTS `yupe_store_delivery_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_delivery_payment` (
  `delivery_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  PRIMARY KEY (`delivery_id`,`payment_id`),
  KEY `fk_yupe_store_delivery_payment_payment` (`payment_id`),
  CONSTRAINT `fk_yupe_store_delivery_payment_delivery` FOREIGN KEY (`delivery_id`) REFERENCES `yupe_store_delivery` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_delivery_payment_payment` FOREIGN KEY (`payment_id`) REFERENCES `yupe_store_payment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_delivery_payment`
--

LOCK TABLES `yupe_store_delivery_payment` WRITE;
/*!40000 ALTER TABLE `yupe_store_delivery_payment` DISABLE KEYS */;
INSERT INTO `yupe_store_delivery_payment` VALUES (1,2),(2,2),(1,3),(2,3);
/*!40000 ALTER TABLE `yupe_store_delivery_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_order`
--

DROP TABLE IF EXISTS `yupe_store_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `delivery_id` int(11) DEFAULT NULL,
  `delivery_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `payment_method_id` int(11) DEFAULT NULL,
  `paid` tinyint(4) DEFAULT '0',
  `payment_time` datetime DEFAULT NULL,
  `payment_details` text,
  `total_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `discount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `coupon_discount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `separate_delivery` tinyint(4) DEFAULT '0',
  `status_id` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '',
  `phone` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `comment` varchar(1024) NOT NULL DEFAULT '',
  `ip` varchar(15) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `note` varchar(1024) NOT NULL DEFAULT '',
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `udx_yupe_store_order_url` (`url`),
  KEY `idx_yupe_store_order_user_id` (`user_id`),
  KEY `idx_yupe_store_order_date` (`date`),
  KEY `idx_yupe_store_order_status` (`status_id`),
  KEY `idx_yupe_store_order_paid` (`paid`),
  KEY `fk_yupe_store_order_delivery` (`delivery_id`),
  KEY `fk_yupe_store_order_payment` (`payment_method_id`),
  CONSTRAINT `fk_yupe_store_order_delivery` FOREIGN KEY (`delivery_id`) REFERENCES `yupe_store_delivery` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_order_payment` FOREIGN KEY (`payment_method_id`) REFERENCES `yupe_store_payment` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_order_status` FOREIGN KEY (`status_id`) REFERENCES `yupe_store_order_status` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_order_user` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_order`
--

LOCK TABLES `yupe_store_order` WRITE;
/*!40000 ALTER TABLE `yupe_store_order` DISABLE KEYS */;
INSERT INTO `yupe_store_order` VALUES (1,NULL,1200.00,NULL,1,NULL,NULL,4000.00,0.00,0.00,0,1,'2015-07-09 07:46:13',1,'admin','Петропавловск Камчатский','+79313549521','info@previarussia.ru','Привезите поскорее','192.168.56.1','d50630ef9aaf9b81ee5f3e25f74c7550','','2015-07-09 08:01:49'),(2,1,0.00,NULL,0,NULL,NULL,1000.00,0.00,0.00,0,1,'2015-07-09 07:54:41',1,'admin','Санкт-Петербург Щербакова 6 205','+79313549521','info@previarussia.ru','Привезите поскорее','192.168.56.1','c27b2897dee652da8ddd3c021d60f965','','2015-07-09 07:54:41'),(3,NULL,1200.00,NULL,0,NULL,NULL,3750.00,0.00,0.00,0,1,'2015-07-09 08:35:22',1,'admin','Щербакова 6 205','+79313549521','info@previarussia.ru','','192.168.56.1','008d4f7f54de6ad862be59be7e3ded42','','2015-07-09 08:35:22'),(4,1,0.00,NULL,0,NULL,NULL,3000.00,0.00,0.00,0,1,'2015-07-09 08:36:12',1,'admin','Щербакова 6 205','+79313549521','info@previarussia.ru','','192.168.56.1','a8bd87ca1eadc8064bab8c62f32d136c','','2015-07-09 16:18:11'),(5,1,0.00,NULL,0,NULL,NULL,1000.00,0.00,0.00,0,1,'2015-07-09 17:19:00',NULL,'vazgen','pr.Elizarova, 10, kv.269','+79313549521','madgrob@mail.ru','Нижний Тагил','192.168.56.1','d7fad3d6d8620544fc0152a54468080d','','2015-07-09 17:19:00'),(6,1,0.00,NULL,0,NULL,NULL,3750.00,0.00,0.00,0,1,'2015-07-09 18:03:54',2,'Барчугов Сергей','Щербакова 6 205','+79313549521','bs7@pisem.net','test','192.168.56.1','60756a50ecd989fd12f77fc1f90e3199','','2015-07-09 18:03:54'),(7,2,0.00,NULL,0,NULL,NULL,1000.00,0.00,0.00,0,1,'2015-07-09 19:57:06',2,'Барчугов Сергей','Щербакова 6 205','+79313549521','bs7@pisem.net','bs7@pisem.net','192.168.56.1','d9b701c835be955dc78ff0b4b2dae2dd','','2015-07-09 19:57:06'),(8,1,0.00,NULL,0,NULL,NULL,1000.00,0.00,0.00,0,1,'2015-07-10 00:49:26',2,'Барчугов Сергей','Щербакова 6 205','+79313549521','bs7@pisem.net','test','192.168.56.1','b3ac0dc0a44b0e870bdc14fa00667ad7','','2015-07-10 00:49:26'),(9,NULL,0.00,NULL,0,NULL,NULL,2000.00,0.00,0.00,0,1,'2015-07-10 02:00:19',NULL,'test','Щербакова 6 205','+79313549521','test@test.ru','test','192.168.56.1','31fdb3340efc20d38546013d9f702156','','2015-07-10 02:00:19'),(10,1,0.00,NULL,0,NULL,NULL,1000.00,0.00,0.00,0,1,'2015-07-11 03:35:56',NULL,'test','Щербакова 6 205','+79313549521','maxim.avramenko@gmail.com','test','192.168.56.1','7d2e2d443d4d63e53590d7b572303d7d','','2015-07-11 03:35:56'),(11,NULL,500.00,NULL,0,NULL,NULL,2000.00,0.00,0.00,0,1,'2015-07-11 14:29:13',1,'admin','Щербакова 6 205','+79313549521','info@previarussia.ru','','192.168.56.1','bfe7c9f6ede3f6c55888b029142969a4','','2015-07-11 14:29:13'),(12,1,0.00,NULL,0,NULL,NULL,2250.00,0.00,0.00,0,1,'2015-07-11 15:01:26',NULL,'тетя Соня','','','maxim.avramenko@gmail.com','test','192.168.56.1','3efb395b08e8fc352f4718ccc89e987e','','2015-07-11 15:01:26'),(13,NULL,500.00,NULL,0,NULL,NULL,2250.00,0.00,0.00,0,1,'2015-07-11 15:15:06',1,'Иван','','','maxim.avramenko@gmail.com','','192.168.56.1','bd462876d0fa885b8609401f77540b4a','','2015-07-11 15:15:06'),(14,1,0.00,NULL,0,NULL,NULL,1000.00,0.00,0.00,0,1,'2015-07-16 06:51:24',1,'admin','','','info@previarussia.ru','','192.168.56.1','c328f021d31cb72d768988ef47f45de7','','2015-07-16 06:51:24'),(15,1,0.00,NULL,0,NULL,NULL,2250.00,0.00,0.00,0,1,'2015-07-16 07:12:00',1,'admin','ул. Щербакова 6 205','+79313549521','info@previarussia.ru','','192.168.56.1','ce854d44a73e0c3c5ea8b5e3855b67a0','','2015-07-16 07:12:00'),(16,NULL,500.00,NULL,0,NULL,NULL,6250.00,0.00,0.00,0,1,'2015-07-16 10:59:10',1,'admin','ул. Щербакова 6 205','+79313549521','info@previarussia.ru','тестовый заказ','192.168.56.1','b92481394e371df10bd489aea6a0b588','','2015-07-16 10:59:10'),(17,2,0.00,NULL,0,NULL,NULL,1250.00,0.00,0.00,0,1,'2015-07-18 23:01:32',NULL,'Test','ул. Щербакова 6 205','+79313549521','maxim.avramenko@gmail.com','test','192.168.56.1','edc3bfba709ec3c47c56f9d877c46c08','','2015-07-18 23:01:32'),(18,1,0.00,NULL,0,NULL,NULL,1250.00,0.00,0.00,0,1,'2015-07-24 23:41:28',NULL,'test','Щербакова 6 205','+79313549521','maxim.avramenko@gmail.com','test','192.168.56.1','840a381e7bd7fd9370387a9bf7827ca1','','2015-07-24 23:41:28'),(19,1,0.00,NULL,0,NULL,NULL,5500.00,0.00,0.00,0,1,'2015-07-26 20:19:36',NULL,'test','Щербакова 6 205','+79313549521','maxim.avramenko@gmail.com','test','192.168.56.1','b16517814fbaa080fe21b84a8534bddf','','2015-07-26 20:19:36'),(20,1,0.00,NULL,0,NULL,NULL,6250.00,0.00,0.00,0,1,'2015-08-16 12:40:12',NULL,'Maxim','SPb Sherbakova 6 205','+79313549521','maxim.avramenko@gmail.com','test','192.168.56.1','5bf5aa3ddc2d11eb4a415a18a73eff49','','2015-08-16 12:40:12'),(21,2,300.00,NULL,0,NULL,NULL,1250.00,0.00,0.00,0,1,'2015-08-17 14:01:53',1,'admin','Щербакова 6 205','+79313549521','info@previarussia.ru','test','192.168.56.1','e618dbc2e5b83f6ed80024386d962866','','2015-08-17 14:01:53'),(22,2,300.00,NULL,0,NULL,NULL,2250.00,0.00,0.00,0,1,'2015-08-17 14:28:16',1,'admin','','','info@previarussia.ru','','192.168.56.1','7de1009267314f7e18f2f66b7ec1e477','','2015-08-17 14:28:16'),(23,2,300.00,NULL,0,NULL,NULL,5000.00,0.00,0.00,0,1,'2015-08-17 14:41:07',1,'admin','Щербакова 6 205','+79313549521','info@previarussia.ru','test','192.168.56.1','0b0d7d0d148af98c61b60b0325e630f7','','2015-08-17 14:41:07'),(24,2,0.00,NULL,0,NULL,NULL,6250.00,0.00,0.00,0,1,'2015-08-17 14:42:57',1,'admin','Щербакова 6 205','+79313549521','info@previarussia.ru','test','192.168.56.1','09616aa5f4f9479555969e9e547305bc','','2015-08-17 14:42:57'),(25,1,0.00,NULL,0,NULL,NULL,1000.00,0.00,0.00,0,1,'2015-08-17 14:48:54',1,'admin','','','info@previarussia.ru','','192.168.56.1','fc5d85ceaeed1488474f2f37cff0cc09','','2015-08-17 14:48:54'),(26,2,300.00,NULL,0,NULL,NULL,2250.00,0.00,0.00,0,1,'2015-08-17 18:05:29',NULL,'test','Щербакова 6 205','+79313549521','maxim.avramenko@gmail.com','test','188.242.41.29','49b6b09bf1c07f7236163dee6a7b1727','','2015-08-17 18:05:29'),(27,2,300.00,NULL,0,NULL,NULL,3500.00,0.00,0.00,0,1,'2015-08-18 17:53:22',NULL,'Test','Щербакова 6 205','+79313549521','test@test.ru','Test','5.18.144.126','5b69dc29889b6c4fd11584df3b47816d','','2015-08-18 17:53:22'),(28,2,0.00,NULL,0,NULL,NULL,6250.00,0.00,0.00,0,1,'2015-08-26 22:41:48',1,'admin','Щербакова 6 205','+79313549521','info@previarussia.ru','test','188.242.41.29','bdc0a18010f071c758aab4f74f2e7004','','2015-08-26 22:41:48'),(29,2,300.00,NULL,0,NULL,NULL,3500.00,0.00,0.00,0,1,'2015-09-13 02:41:08',NULL,'Test','','','test@test.ru','','78.25.122.50','3307a8d808d8dd80cd8c3e1d5d213ca1','','2015-09-13 02:41:08'),(30,1,0.00,NULL,0,NULL,NULL,2450.00,0.00,0.00,0,1,'2015-09-14 12:27:21',NULL,'Fkf','','','df@ddc.ru','','66.102.9.57','689e81e7cd1c96e43beae9ce90e17c14','','2015-09-14 12:27:21'),(31,2,300.00,NULL,0,NULL,NULL,4050.00,0.00,0.00,0,1,'2015-09-15 10:00:13',1,'admin','','','info@previarussia.ru','','192.168.56.1','069ba21fb2f6636a66d1f4945220c140','','2015-09-15 10:00:13');
/*!40000 ALTER TABLE `yupe_store_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_order_coupon`
--

DROP TABLE IF EXISTS `yupe_store_order_coupon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_order_coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_yupe_store_order_coupon_order` (`order_id`),
  KEY `fk_yupe_store_order_coupon_coupon` (`coupon_id`),
  CONSTRAINT `fk_yupe_store_order_coupon_coupon` FOREIGN KEY (`coupon_id`) REFERENCES `yupe_store_coupon` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_yupe_store_order_coupon_order` FOREIGN KEY (`order_id`) REFERENCES `yupe_store_order` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_order_coupon`
--

LOCK TABLES `yupe_store_order_coupon` WRITE;
/*!40000 ALTER TABLE `yupe_store_order_coupon` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_store_order_coupon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_order_product`
--

DROP TABLE IF EXISTS `yupe_store_order_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_order_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_name` varchar(255) NOT NULL,
  `variants` text,
  `variants_text` varchar(1024) DEFAULT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `quantity` int(11) NOT NULL DEFAULT '0',
  `sku` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_yupe_store_order_product_order_id` (`order_id`),
  KEY `idx_yupe_store_order_product_product_id` (`product_id`),
  CONSTRAINT `fk_yupe_store_order_product_order` FOREIGN KEY (`order_id`) REFERENCES `yupe_store_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_order_product_product` FOREIGN KEY (`product_id`) REFERENCES `yupe_store_product` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_order_product`
--

LOCK TABLES `yupe_store_order_product` WRITE;
/*!40000 ALTER TABLE `yupe_store_order_product` DISABLE KEYS */;
INSERT INTO `yupe_store_order_product` VALUES (1,1,NULL,'RECONSTRUCT. REVITALISING FILLER SHAMPOO - 250 ML','a:0:{}',NULL,1000.00,4,'0000000001'),(2,2,NULL,'RECONSTRUCT. REVITALISING FILLER SHAMPOO - 250 ML','a:0:{}',NULL,1000.00,1,'0000000001'),(3,3,NULL,'RECONSTRUCT. TRUE MOIST. FILLER CONDIT. - 200 ML','a:0:{}',NULL,1250.00,3,'0000000003'),(4,4,NULL,'RECONSTRUCT. REVITALISING FILLER SHAMPOO - 250 ML','a:0:{}',NULL,1000.00,3,'0000000001'),(5,5,NULL,'RECONSTRUCT. REVITALISING FILLER SHAMPOO - 250 ML','a:0:{}',NULL,1000.00,1,'0000000001'),(6,6,NULL,'RECONSTRUCT. TRUE MOIST. FILLER CONDIT. - 200 ML','a:0:{}',NULL,1250.00,3,'0000000003'),(7,7,NULL,'RECONSTRUCT. REVITALISING FILLER SHAMPOO - 250 ML','a:0:{}',NULL,1000.00,1,'0000000001'),(8,8,NULL,'RECONSTRUCT. REVITALISING FILLER SHAMPOO - 250 ML','a:0:{}',NULL,1000.00,1,'0000000001'),(9,9,NULL,'RECONSTRUCT. REVITALISING FILLER SHAMPOO - 250 ML','a:0:{}',NULL,1000.00,2,'0000000001'),(10,10,NULL,'RECONSTRUCT. REVITALISING FILLER SHAMPOO - 250 ML','a:0:{}',NULL,1000.00,1,'0000000001'),(11,11,NULL,'RECONSTRUCT. REVITALISING FILLER SHAMPOO - 250 ML','a:0:{}',NULL,1000.00,2,'0000000001'),(12,12,NULL,'RECONSTRUCT. TRUE MOIST. FILLER CONDIT. - 200 ML','a:0:{}',NULL,1250.00,1,'0000000003'),(13,12,NULL,'RECONSTRUCT. REVITALISING FILLER SHAMPOO - 250 ML','a:0:{}',NULL,1000.00,1,'0000000001'),(14,13,NULL,'RECONSTRUCT. REVITALISING FILLER SHAMPOO - 250 ML','a:0:{}',NULL,1000.00,1,'0000000001'),(15,13,NULL,'RECONSTRUCT. TRUE MOIST. FILLER CONDIT. - 200 ML','a:0:{}',NULL,1250.00,1,'0000000003'),(16,14,3,'RECONSTRUCT. REVITALISING FILLER SHAMPOO - 250 ML','a:0:{}',NULL,1000.00,1,''),(17,15,4,'RECONSTRUCT. TRUE MOIST. FILLER CONDIT. - 200 ML','a:0:{}',NULL,1250.00,1,''),(18,15,3,'RECONSTRUCT. REVITALISING FILLER SHAMPOO - 250 ML','a:0:{}',NULL,1000.00,1,''),(19,16,4,'RECONSTRUCT. TRUE MOIST. FILLER CONDIT. - 200 ML','a:0:{}',NULL,1250.00,1,''),(20,16,3,'RECONSTRUCT. REVITALISING FILLER SHAMPOO - 250 ML','a:0:{}',NULL,1000.00,1,''),(21,16,5,'RECONSTRUCT. DEEP NOURIS. FILLER TREAT. - 150 ML','a:0:{}',NULL,1250.00,1,''),(22,16,6,'RECONSTRUCT. BIPHASIC LEAVE-IN FILLER - CONDITIONER. 200 ML','a:0:{}',NULL,1250.00,1,''),(23,16,7,'RECONSTRUCT. FILLER SERUM - 50 ML','a:0:{}',NULL,1500.00,1,''),(24,17,5,'RECONSTRUCT. DEEP NOURIS. FILLER TREAT. - 150 ML','a:0:{}',NULL,1250.00,1,''),(25,18,5,'RECONSTRUCT. DEEP NOURIS. FILLER TREAT. - 150 ML','a:0:{}',NULL,1250.00,1,''),(26,19,4,'RECONSTRUCT. TRUE MOIST. FILLER CONDIT. - 200 ML','a:0:{}',NULL,1250.00,1,''),(27,19,5,'RECONSTRUCT. DEEP NOURIS. FILLER TREAT. - 150 ML','a:0:{}',NULL,1250.00,1,''),(28,19,3,'RECONSTRUCT. REVITALISING FILLER SHAMPOO - 250 ML','a:0:{}',NULL,1000.00,3,''),(29,20,3,'RECONSTRUCT. REVITALISING FILLER SHAMPOO - 250 ML','a:0:{}',NULL,1000.00,1,''),(30,20,4,'RECONSTRUCT. TRUE MOIST. FILLER CONDIT. - 200 ML','a:0:{}',NULL,1250.00,1,''),(31,20,5,'RECONSTRUCT. DEEP NOURIS. FILLER TREAT. - 150 ML','a:0:{}',NULL,1250.00,1,''),(32,20,6,'RECONSTRUCT. BIPHASIC LEAVE-IN FILLER - CONDITIONER. 200 ML','a:0:{}',NULL,1250.00,1,''),(33,20,7,'RECONSTRUCT. FILLER SERUM - 50 ML','a:0:{}',NULL,1500.00,1,''),(34,21,4,'RECONSTRUCT. TRUE MOIST. FILLER CONDIT. - 200 ML','a:0:{}',NULL,1250.00,1,''),(35,22,3,'RECONSTRUCT. REVITALISING FILLER SHAMPOO - 250 ML','a:0:{}',NULL,1000.00,1,''),(36,22,4,'RECONSTRUCT. TRUE MOIST. FILLER CONDIT. - 200 ML','a:0:{}',NULL,1250.00,1,''),(37,23,4,'RECONSTRUCT. TRUE MOIST. FILLER CONDIT. - 200 ML','a:0:{}',NULL,1250.00,2,''),(38,23,5,'RECONSTRUCT. DEEP NOURIS. FILLER TREAT. - 150 ML','a:0:{}',NULL,1250.00,2,''),(39,24,3,'RECONSTRUCT. REVITALISING FILLER SHAMPOO - 250 ML','a:0:{}',NULL,1000.00,1,''),(40,24,4,'RECONSTRUCT. TRUE MOIST. FILLER CONDIT. - 200 ML','a:0:{}',NULL,1250.00,1,''),(41,24,5,'RECONSTRUCT. DEEP NOURIS. FILLER TREAT. - 150 ML','a:0:{}',NULL,1250.00,1,''),(42,24,6,'RECONSTRUCT. BIPHASIC LEAVE-IN FILLER - CONDITIONER. 200 ML','a:0:{}',NULL,1250.00,1,''),(43,24,7,'RECONSTRUCT. FILLER SERUM - 50 ML','a:0:{}',NULL,1500.00,1,''),(44,25,3,'RECONSTRUCT. REVITALISING FILLER SHAMPOO - 250 ML','a:0:{}',NULL,1000.00,1,''),(45,26,3,'RECONSTRUCT. REVITALISING FILLER SHAMPOO - 250 ML','a:0:{}',NULL,1000.00,1,''),(46,26,4,'RECONSTRUCT. TRUE MOIST. FILLER CONDIT. - 200 ML','a:0:{}',NULL,1250.00,1,''),(47,27,3,'RECONSTRUCT. REVITALISING FILLER SHAMPOO - 250 ML','a:0:{}',NULL,1000.00,1,''),(48,27,4,'RECONSTRUCT. TRUE MOIST. FILLER CONDIT. - 200 ML','a:0:{}',NULL,1250.00,1,''),(49,27,5,'RECONSTRUCT. DEEP NOURIS. FILLER TREAT. - 150 ML','a:0:{}',NULL,1250.00,1,''),(50,28,3,'RECONSTRUCT. REVITALISING FILLER SHAMPOO - 250 ML','a:0:{}',NULL,1000.00,1,''),(51,28,4,'RECONSTRUCT. TRUE MOIST. FILLER CONDIT. - 200 ML','a:0:{}',NULL,1250.00,1,''),(52,28,5,'RECONSTRUCT. DEEP NOURIS. FILLER TREAT. - 150 ML','a:0:{}',NULL,1250.00,1,''),(53,28,6,'RECONSTRUCT. BIPHASIC LEAVE-IN FILLER - CONDITIONER. 200 ML','a:0:{}',NULL,1250.00,1,''),(54,28,7,'RECONSTRUCT. FILLER SERUM - 50 ML','a:0:{}',NULL,1500.00,1,''),(55,29,15,'CURLFRIENDS. Интенсивно увлажняющий шампунь - 250 мл','a:0:{}',NULL,1050.00,1,''),(56,29,16,'CURLFRIENDS. Интенсивно питающий кондиционер - 200 мл','a:0:{}',NULL,1200.00,1,''),(57,29,3,'RECONSTRUCT. Восстанавливающий шампунь - 250 мл','a:0:{}',NULL,1250.00,1,''),(58,30,21,'EXTRALIFE. Тонизирующий шампунь - 250 мл','a:0:{}',NULL,1250.00,1,''),(59,30,22,'EXTRALIFE. Тонизирующий несмываемый лосьон для кожи головы - 150 мл','a:0:{}',NULL,1200.00,1,''),(60,31,4,'RECONSTRUCT. Увлажняющий кондиционер - 200 мл','a:0:{}',NULL,1400.00,1,''),(61,31,6,'RECONSTRUCT. Двухфазный несмываемый кондиционер 200 мл','a:0:{}',NULL,1400.00,1,''),(62,31,3,'RECONSTRUCT. Восстанавливающий шампунь - 250 мл','a:0:{}',NULL,1250.00,1,'');
/*!40000 ALTER TABLE `yupe_store_order_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_order_status`
--

DROP TABLE IF EXISTS `yupe_store_order_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_order_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `is_system` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_order_status`
--

LOCK TABLES `yupe_store_order_status` WRITE;
/*!40000 ALTER TABLE `yupe_store_order_status` DISABLE KEYS */;
INSERT INTO `yupe_store_order_status` VALUES (1,'Новый',1),(2,'Принят',1),(3,'Выполнен',1),(4,'Удален',1);
/*!40000 ALTER TABLE `yupe_store_order_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_payment`
--

DROP TABLE IF EXISTS `yupe_store_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(100) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `settings` text,
  `currency_id` int(11) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_yupe_store_payment_position` (`position`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_payment`
--

LOCK TABLES `yupe_store_payment` WRITE;
/*!40000 ALTER TABLE `yupe_store_payment` DISABLE KEYS */;
INSERT INTO `yupe_store_payment` VALUES (2,'robokassa','ROBOKASSA','<p>Visa, MasterCard, Yandex money, Qiwi</p>','a:5:{s:5:\"login\";s:9:\"123123123\";s:9:\"password1\";s:12:\"123123123123\";s:9:\"password2\";s:15:\"123123123123123\";s:8:\"language\";s:2:\"ru\";s:8:\"testmode\";s:1:\"1\";}',NULL,3,1),(3,'cash','Наличными','<p>Распечатать счет на оплату наличными</p>','a:2:{s:8:\"language\";s:2:\"ru\";s:8:\"testmode\";s:1:\"0\";}',NULL,2,1);
/*!40000 ALTER TABLE `yupe_store_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_producer`
--

DROP TABLE IF EXISTS `yupe_store_producer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_producer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_short` varchar(150) NOT NULL,
  `name` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text,
  `meta_title` varchar(250) DEFAULT NULL,
  `meta_keywords` varchar(250) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_store_producer_slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_producer`
--

LOCK TABLES `yupe_store_producer` WRITE;
/*!40000 ALTER TABLE `yupe_store_producer` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_store_producer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_product`
--

DROP TABLE IF EXISTS `yupe_store_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) DEFAULT NULL,
  `producer_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sku` varchar(100) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `price` decimal(19,3) NOT NULL DEFAULT '0.000',
  `discount_price` decimal(19,3) DEFAULT NULL,
  `discount` decimal(19,3) DEFAULT NULL,
  `description` text,
  `short_description` text,
  `data` text,
  `is_special` tinyint(1) NOT NULL DEFAULT '0',
  `length` decimal(19,3) DEFAULT NULL,
  `width` decimal(19,3) DEFAULT NULL,
  `height` decimal(19,3) DEFAULT NULL,
  `weight` decimal(19,3) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `in_stock` tinyint(4) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `meta_title` varchar(250) DEFAULT NULL,
  `meta_keywords` varchar(250) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `average_price` decimal(19,3) DEFAULT NULL,
  `purchase_price` decimal(19,3) DEFAULT NULL,
  `recommended_price` decimal(19,3) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_store_product_alias` (`slug`),
  KEY `ix_yupe_store_product_status` (`status`),
  KEY `ix_yupe_store_product_type_id` (`type_id`),
  KEY `ix_yupe_store_product_producer_id` (`producer_id`),
  KEY `ix_yupe_store_product_price` (`price`),
  KEY `ix_yupe_store_product_discount_price` (`discount_price`),
  KEY `ix_yupe_store_product_create_time` (`create_time`),
  KEY `ix_yupe_store_product_update_time` (`update_time`),
  KEY `fk_yupe_store_product_category` (`category_id`),
  CONSTRAINT `fk_yupe_store_product_category` FOREIGN KEY (`category_id`) REFERENCES `yupe_store_category` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_product_producer` FOREIGN KEY (`producer_id`) REFERENCES `yupe_store_producer` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_product_type` FOREIGN KEY (`type_id`) REFERENCES `yupe_store_type` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_product`
--

LOCK TABLES `yupe_store_product` WRITE;
/*!40000 ALTER TABLE `yupe_store_product` DISABLE KEYS */;
INSERT INTO `yupe_store_product` VALUES (3,7,NULL,1,'','RECONSTRUCT. Восстанавливающий шампунь - 250 мл','reconstruct-vosstanavlivayushchiy-shampun-250-ml',1250.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-07-16 05:15:45','2015-08-27 14:08:05','','','','06ba2c8fe67718a4cda389066a6d0eb7.jpg',0.000,0.000,0.000,1),(4,9,NULL,1,'','RECONSTRUCT. Увлажняющий кондиционер - 200 мл','reconstruct-uvlazhnyayushchiy-kondicioner-200-ml',1400.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-07-16 06:36:28','2015-08-27 14:08:05','','','','da904605135410112fa986947def1516.jpg',0.000,0.000,0.000,3),(5,11,NULL,1,'','RECONSTRUCT. Средство для интенсивного питания волос - 150 мл','reconstruct-sredstvo-dlya-intensivnogo-pitaniya-volos-150-ml',1400.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-07-16 10:35:02','2015-08-27 14:08:05','','','','a98b7cc3b98b05e279a52bd46ae69023.jpg',0.000,0.000,0.000,2),(6,9,NULL,1,'','RECONSTRUCT. Двухфазный несмываемый кондиционер 200 мл','reconstruct-dvuhfaznyy-nesmyvaemyy-kondicioner-200-ml',1400.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-07-16 10:46:36','2015-08-27 14:08:05','','','','f4953748ecbc949bf2060b869c6e92a7.jpg',0.000,0.000,0.000,4),(7,12,NULL,1,'','RECONSTRUCT. Сыворотка для волос - 50 мл','reconstruct-syvorotka-dlya-volos-50-ml',1650.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-07-16 10:53:35','2015-08-27 14:08:05','','','','68d8a53b65f77896bdac0fe8771dc4b8.jpg',0.000,0.000,0.000,5),(8,7,NULL,1,'','LIGHTENING. Шампунь-кондиционер, нейтрализующий желтизну - 300 мл','lightening-shampun-kondicioner-neytralizuyushchiy-zheltiznu-300-ml',1300.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-08-27 12:31:32','2015-08-27 14:08:05','','','','fe6842e7d0e140fb93ddeb6cb5f0e57f.jpg',0.000,0.000,0.000,6),(9,9,NULL,1,'','LIGHTENING. Кондиционирующая маска, нейтрализующая желтизну - 250 мл','lightening-kondicioniruyushchaya-maska-neytralizuyushchaya-zheltiznu-250-ml',1400.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-08-27 12:35:17','2015-08-27 14:08:05','','','','f1735cef4d07a9e89a24a80be789d0fc.jpg',0.000,0.000,0.000,7),(10,7,NULL,1,'','VOLUMIZING. Шампунь для придания объема - 250 мл','volumizing-shampun-dlya-pridaniya-obema-250-ml',1050.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-08-27 12:59:39','2015-08-27 14:08:05','','','','7706798e58ff749f287ee241eb6444d3.jpg',0.000,0.000,0.000,8),(11,9,NULL,1,'','VOLUMIZING. Кондиционер для придания объема - 200 мл','volumizing-kondicioner-dlya-pridaniya-obema-200-ml',1200.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-08-27 13:01:18','2015-08-27 14:08:05','','','','9c04bf5803f207111954f459b00eca12.jpg',0.000,0.000,0.000,9),(12,7,NULL,1,'','SMOOTHING. Кератиновый разглаживающий шампунь - 250 мл','smoothing-keratinovyy-razglazhivayushchiy-shampun-250-ml',1050.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-08-27 13:16:29','2015-08-27 14:08:05','','','','f7e0e266f540450d048791fa8bbc782d.jpg',0.000,0.000,0.000,10),(13,9,NULL,1,'','SMOOTHING. Кератиновый разглаживающий кондиционер - 200 мл','smoothing-keratinovyy-razglazhivayushchiy-kondicioner-200-ml',1200.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-08-27 13:17:27','2015-08-27 14:08:05','','','','cda7be41960448c2b8ecd1474f985281.jpg',0.000,0.000,0.000,11),(14,NULL,NULL,1,'','SMOOTHING. Кератиновое средство для блеска волос с разглаживающим эффектом - 150 мл','smoothing-keratinovoe-sredstvo-dlya-bleska-volos-s-razglazhivayushchim-effektom-150-ml',1900.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-08-27 13:18:20','2015-08-27 14:08:05','','','','48587d7ff99efec7ea1cd40bb95c4791.jpg',0.000,0.000,0.000,12),(15,7,NULL,1,'','CURLFRIENDS. Интенсивно увлажняющий шампунь - 250 мл','curlfriends-intensivno-uvlazhnyayushchiy-shampun-250-ml',1050.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-08-27 13:21:32','2015-08-27 14:08:05','','','','b120a3065f187824b35204f6cb2fc629.jpg',0.000,0.000,0.000,13),(16,9,NULL,1,'','CURLFRIENDS. Интенсивно питающий кондиционер - 200 мл','curlfriends-intensivno-pitayushchiy-kondicioner-200-ml',1200.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-08-27 13:23:52','2015-08-27 14:08:05','','','','3c2b36907886b2be08da4b02842e5447.jpg',0.000,0.000,0.000,14),(17,NULL,NULL,1,'','CURLFRIENDS. Разглаживающее несмываемое средство для волос - 200 мл','curlfriends-razglazhivayushchee-nesmyvaemoe-sredstvo-dlya-volos-200-ml',950.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-08-27 13:25:17','2015-08-27 14:08:05','','','','fa8b362655b34753c0830b64ac5af136.jpg',0.000,0.000,0.000,15),(18,7,NULL,1,'','KEEPING. Шампунь для окрашенных волос - 250 мл','keeping-shampun-dlya-okrashennyh-volos-250-ml',1050.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-08-27 13:29:45','2015-08-27 14:08:05','','','','22d6b6a08a68f3f1a16b90e3325aca1a.jpg',0.000,0.000,0.000,16),(19,9,NULL,1,'','KEEPING. Кондиционер для окрашенных волос - 200 мл','keeping-kondicioner-dlya-okrashennyh-volos-200-ml',1200.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-08-27 13:30:36','2015-08-27 14:08:05','','','','46b46b6c1c430a593edd4c3f5188ba46.jpg',0.000,0.000,0.000,17),(20,NULL,NULL,1,'','KEEPING. Несмываемое молочко для защиты цвета - 200 мл','keeping-nesmyvaemoe-molochko-dlya-zashchity-cveta-200-ml',1500.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-08-27 13:31:56','2015-08-27 14:08:05','','','','35f387cc3f7a3778188ea993f1927adb.jpg',0.000,0.000,0.000,18),(21,7,NULL,1,'','EXTRALIFE. Тонизирующий шампунь - 250 мл','extralife-toniziruyushchiy-shampun-250-ml',1250.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-08-27 13:35:42','2015-08-27 14:08:05','','','','49500a0f4a7ff218ead9171b732e389b.jpg',0.000,0.000,0.000,19),(22,6,NULL,1,'','EXTRALIFE. Тонизирующий несмываемый лосьон для кожи головы - 150 мл','extralife-toniziruyushchiy-nesmyvaemyy-loson-dlya-kozhi-golovy-150-ml',1200.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-08-27 13:37:46','2015-08-27 14:08:05','','','','e308b7ed9830f3606baad345d4cce6b3.jpg',0.000,0.000,0.000,20),(23,7,NULL,1,'','EXTRALIFE. Очищающий шампунь - 265 мл','extralife-ochishchayushchiy-shampun-265-ml',1250.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-08-27 13:39:44','2015-08-27 14:08:05','','','','6c13688f68e5795df821976fe03050fe.jpg',0.000,0.000,0.000,21),(24,6,NULL,1,'','EXTRALIFE. Очищающий несмываемый лосьон для кожи головы - 165 мл','extralife-ochishchayushchiy-nesmyvaemyy-loson-dlya-kozhi-golovy-165-ml',1200.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-08-27 13:41:28','2015-08-27 14:08:05','','','','0b1e53f8dc06c57c0726d9e1b7adbb79.jpg',0.000,0.000,0.000,22),(25,4,NULL,1,'','FINISH. Крем для придания текстуры - 150 мл','finish-krem-dlya-pridaniya-tekstury-150-ml',1300.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-08-27 13:45:28','2015-08-27 14:08:05','','','','a1a317c30d7244569bbfb056150a54de.jpg',0.000,0.000,0.000,23),(26,NULL,NULL,1,'','FINISH. Гель экстрасильной фиксации - 200 мл','finish-gel-ekstrasilnoy-fiksacii-200-ml',1050.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-08-27 13:46:29','2015-08-27 14:08:05','','','','cc1cf0116d2768756ae7348556e839ab.jpg',0.000,0.000,0.000,24),(27,NULL,NULL,1,'','FINISH. Матирующая паста - 100 мл','finish-matiruyushchaya-pasta-100-ml',1300.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-08-27 13:50:45','2015-08-27 14:08:05','','','','25244cc5e8e38d9984caeb2892d70db6.jpg',0.000,0.000,0.000,25),(28,NULL,NULL,1,'','FINISH. Спрей для блеска волос - 100 мл','finish-sprey-dlya-bleska-volos-100-ml',2000.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-08-27 13:52:10','2015-08-27 14:08:05','','','','a87c6396c0dcad32f39c9e88f18a3b07.jpg',0.000,0.000,0.000,26),(29,NULL,NULL,1,'','FINISH. Паста для придания формы - 100 мл','finish-pasta-dlya-pridaniya-formy-100-ml',1500.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-08-27 13:56:01','2015-08-27 14:08:05','','','','9b621ff9aef557458919616959e47824.jpg',0.000,0.000,0.000,27),(30,NULL,NULL,1,'','FINISH. Матирующий воск - 100 мл','finish-matiruyushchiy-vosk-100-ml',1700.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-08-27 14:02:53','2015-08-27 14:08:05','','','','863b25858b50581c8c2057c87a110749.jpg',0.000,0.000,0.000,28),(31,NULL,NULL,1,'','FINISH. Гель-воск - 150 мл','finish-gel-vosk-150-ml',1150.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-08-27 14:05:07','2015-08-27 14:08:05','','','','8a209fc8978deae471b06596784e23ec.jpg',0.000,0.000,0.000,29),(32,NULL,NULL,1,'','FINISH. Средство для придания глянцевого блеска - 250 мл','finish-sredstvo-dlya-pridaniya-glyancevogo-bleska-250-ml',1000.000,NULL,NULL,'','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2015-08-27 14:06:49','2015-08-27 14:09:16','','','','aa29aa38bab8e9c945b129da9015bf0f.jpg',0.000,0.000,0.000,30);
/*!40000 ALTER TABLE `yupe_store_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_product_attribute_eav`
--

DROP TABLE IF EXISTS `yupe_store_product_attribute_eav`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_product_attribute_eav` (
  `product_id` int(11) NOT NULL,
  `attribute` varchar(250) NOT NULL,
  `value` text,
  KEY `ix_yupe_store_product_attribute_eav_product_id` (`product_id`),
  KEY `ix_yupe_store_product_attribute_eav_attribute` (`attribute`),
  CONSTRAINT `fk_yupe_store_product_attribute_eav_attribute` FOREIGN KEY (`attribute`) REFERENCES `yupe_store_attribute` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_product_attribute_eav_product` FOREIGN KEY (`product_id`) REFERENCES `yupe_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_product_attribute_eav`
--

LOCK TABLES `yupe_store_product_attribute_eav` WRITE;
/*!40000 ALTER TABLE `yupe_store_product_attribute_eav` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_store_product_attribute_eav` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_product_category`
--

DROP TABLE IF EXISTS `yupe_store_product_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_product_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_yupe_store_product_category_product_id` (`product_id`),
  KEY `ix_yupe_store_product_category_category_id` (`category_id`),
  CONSTRAINT `fk_yupe_store_product_category_category` FOREIGN KEY (`category_id`) REFERENCES `yupe_store_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_product_category_product` FOREIGN KEY (`product_id`) REFERENCES `yupe_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_product_category`
--

LOCK TABLES `yupe_store_product_category` WRITE;
/*!40000 ALTER TABLE `yupe_store_product_category` DISABLE KEYS */;
INSERT INTO `yupe_store_product_category` VALUES (5,3,8),(6,3,8),(7,4,8),(8,4,8),(9,6,8),(10,6,8),(11,5,8),(12,5,8),(13,7,8),(14,7,8),(15,8,6),(16,8,6),(17,9,6),(18,9,6),(19,10,9),(20,10,9),(21,11,9),(22,11,9),(23,12,4),(24,12,4),(25,13,4),(26,13,4),(27,14,4),(28,14,4),(29,15,3),(30,15,3),(31,16,3),(32,16,3),(33,17,3),(34,17,3),(35,18,5),(36,18,5),(37,19,5),(38,19,5),(39,20,5),(40,20,5),(41,21,7),(42,21,7),(43,22,7),(44,22,7),(45,23,7),(46,23,7),(47,24,7),(48,24,7),(49,25,10),(50,25,10),(51,26,10),(52,26,10),(53,27,10),(54,27,10),(55,28,10),(56,28,10),(57,29,10),(58,29,10),(59,30,10),(60,30,10),(61,31,10),(62,31,10),(63,32,10),(64,32,10);
/*!40000 ALTER TABLE `yupe_store_product_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_product_image`
--

DROP TABLE IF EXISTS `yupe_store_product_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_product_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_yupe_store_product_image_product` (`product_id`),
  CONSTRAINT `fk_yupe_store_product_image_product` FOREIGN KEY (`product_id`) REFERENCES `yupe_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_product_image`
--

LOCK TABLES `yupe_store_product_image` WRITE;
/*!40000 ALTER TABLE `yupe_store_product_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_store_product_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_product_link`
--

DROP TABLE IF EXISTS `yupe_store_product_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_product_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `linked_product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_store_product_link_product` (`product_id`,`linked_product_id`),
  KEY `fk_yupe_store_product_link_linked_product` (`linked_product_id`),
  KEY `fk_yupe_store_product_link_type` (`type_id`),
  CONSTRAINT `fk_yupe_store_product_link_linked_product` FOREIGN KEY (`linked_product_id`) REFERENCES `yupe_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_product_link_product` FOREIGN KEY (`product_id`) REFERENCES `yupe_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_product_link_type` FOREIGN KEY (`type_id`) REFERENCES `yupe_store_product_link_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_product_link`
--

LOCK TABLES `yupe_store_product_link` WRITE;
/*!40000 ALTER TABLE `yupe_store_product_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_store_product_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_product_link_type`
--

DROP TABLE IF EXISTS `yupe_store_product_link_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_product_link_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_store_product_link_type_code` (`code`),
  UNIQUE KEY `ux_yupe_store_product_link_type_title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_product_link_type`
--

LOCK TABLES `yupe_store_product_link_type` WRITE;
/*!40000 ALTER TABLE `yupe_store_product_link_type` DISABLE KEYS */;
INSERT INTO `yupe_store_product_link_type` VALUES (1,'similar','Похожие'),(2,'related','Сопутствующие');
/*!40000 ALTER TABLE `yupe_store_product_link_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_product_variant`
--

DROP TABLE IF EXISTS `yupe_store_product_variant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_product_variant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `attribute_value` varchar(255) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `type` tinyint(4) NOT NULL,
  `sku` varchar(50) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_yupe_store_product_variant_product` (`product_id`),
  KEY `idx_yupe_store_product_variant_attribute` (`attribute_id`),
  KEY `idx_yupe_store_product_variant_value` (`attribute_value`),
  CONSTRAINT `fk_yupe_store_product_variant_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `yupe_store_attribute` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_product_variant_product` FOREIGN KEY (`product_id`) REFERENCES `yupe_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_product_variant`
--

LOCK TABLES `yupe_store_product_variant` WRITE;
/*!40000 ALTER TABLE `yupe_store_product_variant` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_store_product_variant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_type`
--

DROP TABLE IF EXISTS `yupe_store_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `main_category_id` int(11) DEFAULT NULL,
  `categories` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_store_type_name` (`name`),
  KEY `fk_yupe_store_type_main_category` (`main_category_id`),
  CONSTRAINT `fk_yupe_store_type_main_category` FOREIGN KEY (`main_category_id`) REFERENCES `yupe_store_category` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_type`
--

LOCK TABLES `yupe_store_type` WRITE;
/*!40000 ALTER TABLE `yupe_store_type` DISABLE KEYS */;
INSERT INTO `yupe_store_type` VALUES (4,'Крем',NULL,'N;'),(5,'Эмульсия',NULL,'N;'),(6,'Лосьон',NULL,'N;'),(7,'Шампунь',NULL,'N;'),(8,'Масло',NULL,'N;'),(9,'Кондиционер',NULL,'N;'),(10,'Бальзам',NULL,'N;'),(11,'Маска',NULL,'N;'),(12,'Сыворотка',NULL,'N;');
/*!40000 ALTER TABLE `yupe_store_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_type_attribute`
--

DROP TABLE IF EXISTS `yupe_store_type_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_type_attribute` (
  `type_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  PRIMARY KEY (`type_id`,`attribute_id`),
  CONSTRAINT `fk_yupe_store_type_attribute_type` FOREIGN KEY (`type_id`) REFERENCES `yupe_store_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_type_attribute`
--

LOCK TABLES `yupe_store_type_attribute` WRITE;
/*!40000 ALTER TABLE `yupe_store_type_attribute` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_store_type_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_user_tokens`
--

DROP TABLE IF EXISTS `yupe_user_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_user_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `expire_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_yupe_user_tokens_user_id` (`user_id`),
  CONSTRAINT `fk_yupe_user_tokens_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_user_tokens`
--

LOCK TABLES `yupe_user_tokens` WRITE;
/*!40000 ALTER TABLE `yupe_user_tokens` DISABLE KEYS */;
INSERT INTO `yupe_user_tokens` VALUES (1,1,'OVt8wYPP~xTXYectXGaVR7A33oTI0~P1',4,0,'2015-07-11 13:49:09','2015-07-11 13:49:09','192.168.56.1','2015-07-18 13:49:09'),(3,2,'WFmPKFeugQQSEUeUh3kRPIUwAeUKDmja',2,0,'2015-09-14 18:28:04','2015-09-14 18:28:04','195.209.119.138','2015-09-15 18:28:04');
/*!40000 ALTER TABLE `yupe_user_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_user_user`
--

DROP TABLE IF EXISTS `yupe_user_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_user_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `update_time` datetime NOT NULL,
  `first_name` varchar(250) DEFAULT NULL,
  `middle_name` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  `nick_name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '0',
  `birth_date` date DEFAULT NULL,
  `site` varchar(250) NOT NULL DEFAULT '',
  `about` varchar(250) NOT NULL DEFAULT '',
  `location` varchar(250) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '2',
  `access_level` int(11) NOT NULL DEFAULT '0',
  `visit_time` datetime DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `avatar` varchar(150) DEFAULT NULL,
  `hash` varchar(255) NOT NULL DEFAULT 'c34f9bbb73e987e82f5ac97da54f44500.24857200 1436399072',
  `email_confirm` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_user_user_nick_name` (`nick_name`),
  UNIQUE KEY `ux_yupe_user_user_email` (`email`),
  KEY `ix_yupe_user_user_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_user_user`
--

LOCK TABLES `yupe_user_user` WRITE;
/*!40000 ALTER TABLE `yupe_user_user` DISABLE KEYS */;
INSERT INTO `yupe_user_user` VALUES (1,'2015-07-09 02:49:23','','','','admin','info@previarussia.ru',0,NULL,'','','',1,1,'2015-09-18 05:11:39','2015-07-09 02:49:23',NULL,'$2y$13$0eSIwmWL2aMpQg7LA829B.wPIC.tYHmJ2qlErU6Rk2ZbGXruuhJZG',1),(2,'2015-08-27 11:08:25','Сергей','','Барчугов','sergey','bs7@pisem.net',1,NULL,'','','',1,1,'2015-09-14 18:28:57','2015-07-09 02:55:42',NULL,'$2y$13$WTKUEIa7ev8s8q7.8rmP7.Sj8j74An5Pvc.Fz7fRdGye/gsFtq8c6',1);
/*!40000 ALTER TABLE `yupe_user_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_user_user_auth_assignment`
--

DROP TABLE IF EXISTS `yupe_user_user_auth_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_user_user_auth_assignment` (
  `itemname` char(64) NOT NULL,
  `userid` int(11) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`),
  KEY `fk_yupe_user_user_auth_assignment_user` (`userid`),
  CONSTRAINT `fk_yupe_user_user_auth_assignment_item` FOREIGN KEY (`itemname`) REFERENCES `yupe_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_user_user_auth_assignment_user` FOREIGN KEY (`userid`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_user_user_auth_assignment`
--

LOCK TABLES `yupe_user_user_auth_assignment` WRITE;
/*!40000 ALTER TABLE `yupe_user_user_auth_assignment` DISABLE KEYS */;
INSERT INTO `yupe_user_user_auth_assignment` VALUES ('admin',1,NULL,NULL),('admin',2,NULL,NULL);
/*!40000 ALTER TABLE `yupe_user_user_auth_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_user_user_auth_item`
--

DROP TABLE IF EXISTS `yupe_user_user_auth_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_user_user_auth_item` (
  `name` char(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`),
  KEY `ix_yupe_user_user_auth_item_type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_user_user_auth_item`
--

LOCK TABLES `yupe_user_user_auth_item` WRITE;
/*!40000 ALTER TABLE `yupe_user_user_auth_item` DISABLE KEYS */;
INSERT INTO `yupe_user_user_auth_item` VALUES ('admin',2,'Администратор',NULL,NULL);
/*!40000 ALTER TABLE `yupe_user_user_auth_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_user_user_auth_item_child`
--

DROP TABLE IF EXISTS `yupe_user_user_auth_item_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_user_user_auth_item_child` (
  `parent` char(64) NOT NULL,
  `child` char(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `fk_yupe_user_user_auth_item_child_child` (`child`),
  CONSTRAINT `fk_yupe_user_user_auth_itemchild_parent` FOREIGN KEY (`parent`) REFERENCES `yupe_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_user_user_auth_item_child_child` FOREIGN KEY (`child`) REFERENCES `yupe_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_user_user_auth_item_child`
--

LOCK TABLES `yupe_user_user_auth_item_child` WRITE;
/*!40000 ALTER TABLE `yupe_user_user_auth_item_child` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_user_user_auth_item_child` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_yandex_market_export`
--

DROP TABLE IF EXISTS `yupe_yandex_market_export`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_yandex_market_export` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `settings` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_yandex_market_export`
--

LOCK TABLES `yupe_yandex_market_export` WRITE;
/*!40000 ALTER TABLE `yupe_yandex_market_export` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_yandex_market_export` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_yupe_settings`
--

DROP TABLE IF EXISTS `yupe_yupe_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_yupe_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` varchar(100) NOT NULL,
  `param_name` varchar(100) NOT NULL,
  `param_value` varchar(255) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_yupe_settings_module_id_param_name_user_id` (`module_id`,`param_name`,`user_id`),
  KEY `ix_yupe_yupe_settings_module_id` (`module_id`),
  KEY `ix_yupe_yupe_settings_param_name` (`param_name`),
  KEY `fk_yupe_yupe_settings_user_id` (`user_id`),
  CONSTRAINT `fk_yupe_yupe_settings_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_yupe_settings`
--

LOCK TABLES `yupe_yupe_settings` WRITE;
/*!40000 ALTER TABLE `yupe_yupe_settings` DISABLE KEYS */;
INSERT INTO `yupe_yupe_settings` VALUES (1,'yupe','siteDescription','Previa Russia','2015-07-09 02:50:11','2015-07-09 02:50:11',1,1),(2,'yupe','siteName','Previa Russia','2015-07-09 02:50:11','2015-07-09 02:50:11',1,1),(3,'yupe','siteKeyWords','Previa Russia','2015-07-09 02:50:11','2015-07-09 02:50:11',1,1),(4,'yupe','email','info@previarussia.ru','2015-07-09 02:50:11','2015-07-09 02:50:11',1,1),(5,'yupe','theme','unify','2015-07-09 02:50:11','2015-07-16 06:31:11',1,1),(6,'yupe','backendTheme','','2015-07-09 02:50:11','2015-07-09 02:50:11',1,1),(7,'menuitem','pageSize','100','2015-07-09 05:06:02','2015-07-09 05:06:02',1,2),(8,'homepage','mode','2','2015-07-09 05:37:21','2015-09-18 05:18:51',1,1),(9,'homepage','target','3','2015-07-09 05:37:21','2015-09-18 05:19:00',1,1),(10,'homepage','limit','','2015-07-09 05:37:21','2015-07-09 05:37:21',1,1),(11,'order','notifyEmailFrom','no-reply@previarussia.ru','2015-07-09 07:48:23','2015-07-09 07:48:23',1,1),(12,'order','notifyEmailsTo','info@previarussia.ru, maxim.avramenko@gmail.com, bs7@pisem.net ','2015-07-09 07:48:23','2015-07-09 08:39:50',1,1),(13,'order','showOrder','1','2015-07-09 07:48:23','2015-07-09 07:48:23',1,1),(14,'order','enableCheck','1','2015-07-09 07:48:23','2015-07-09 07:48:23',1,1),(15,'order','defaultStatus','1','2015-07-09 07:48:23','2015-07-09 07:48:23',1,1),(16,'order','enableComments','1','2015-07-09 07:48:23','2015-07-09 07:48:23',1,1),(17,'yupe','coreCacheTime','3600','2015-07-09 07:49:39','2015-07-09 07:49:39',1,1),(18,'yupe','uploadPath','uploads','2015-07-09 07:49:40','2015-07-09 07:49:40',1,1),(19,'yupe','editor','redactor','2015-07-09 07:49:40','2015-07-09 07:49:40',1,1),(20,'yupe','availableLanguages','ru,en,zh_cn','2015-07-09 07:49:40','2015-07-09 07:49:40',1,1),(21,'yupe','defaultLanguage','ru','2015-07-09 07:49:40','2015-07-09 07:49:40',1,1),(22,'yupe','defaultBackendLanguage','ru','2015-07-09 07:49:40','2015-07-09 07:49:40',1,1),(23,'yupe','allowedIp','','2015-07-09 07:49:40','2015-07-09 07:49:40',1,1),(24,'yupe','hidePanelUrls','1','2015-07-09 07:49:40','2015-07-09 07:49:40',1,1),(25,'yupe','logo','images/logo.png','2015-07-09 07:49:40','2015-07-09 07:49:40',1,1),(26,'yupe','allowedExtensions','gif, jpeg, png, jpg, zip, rar','2015-07-09 07:49:40','2015-07-09 07:49:40',1,1),(27,'yupe','mimeTypes','image/gif, image/jpeg, image/png, application/zip, application/rar','2015-07-09 07:49:40','2015-07-09 07:49:40',1,1),(28,'yupe','maxSize','5242880','2015-07-09 07:49:40','2015-07-09 07:49:40',1,1),(29,'attribute','pageSize','50','2015-07-14 14:18:56','2015-07-14 14:18:56',1,2),(30,'catalog','mainCategory','1','2015-07-16 05:11:33','2015-07-16 05:11:33',1,1),(31,'catalog','uploadPath','catalog','2015-07-16 05:11:33','2015-07-16 05:11:33',1,1),(32,'catalog','editor','redactor','2015-07-16 05:11:33','2015-07-16 05:11:33',1,1),(33,'catalog','allowedExtensions','jpg,jpeg,png,gif','2015-07-16 05:11:33','2015-07-16 05:11:33',1,1),(34,'catalog','minSize','0','2015-07-16 05:11:33','2015-07-16 05:11:33',1,1),(35,'catalog','maxSize','5242880','2015-07-16 05:11:33','2015-07-16 05:11:33',1,1),(36,'comment','allowGuestComment','0','2015-07-16 06:15:37','2015-07-16 06:15:37',1,1),(37,'comment','defaultCommentStatus','0','2015-07-16 06:15:37','2015-07-16 06:15:37',1,1),(38,'comment','autoApprove','0','2015-07-16 06:15:37','2015-07-16 06:15:37',1,1),(39,'comment','notify','1','2015-07-16 06:15:37','2015-07-16 06:15:37',1,1),(40,'comment','email','info@previarussia.ru','2015-07-16 06:15:37','2015-07-16 06:15:37',1,1),(41,'comment','showCaptcha','1','2015-07-16 06:15:37','2015-07-16 06:15:37',1,1),(42,'comment','minCaptchaLength','3','2015-07-16 06:15:37','2015-07-16 06:15:37',1,1),(43,'comment','maxCaptchaLength','6','2015-07-16 06:15:37','2015-07-16 06:15:37',1,1),(44,'comment','rssCount','10','2015-07-16 06:15:37','2015-07-16 06:15:37',1,1),(45,'comment','allowedTags','p,br,strong,img[src|style],a[href]','2015-07-16 06:15:37','2015-07-16 06:15:37',1,1),(46,'comment','antiSpamInterval','3','2015-07-16 06:15:37','2015-07-16 06:15:37',1,1),(47,'comment','stripTags','1','2015-07-16 06:15:37','2015-07-16 06:15:37',1,1),(48,'comment','editor','textarea','2015-07-16 06:15:37','2015-07-16 06:15:37',1,1),(49,'comment','modelsAvailableForRss','','2015-07-16 06:15:37','2015-07-16 06:15:37',1,1),(50,'product','pageSize','50','2015-08-27 13:27:59','2015-08-27 13:27:59',2,2);
/*!40000 ALTER TABLE `yupe_yupe_settings` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-18  6:42:07
